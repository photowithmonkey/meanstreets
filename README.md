# MEAN STREETS


### Running in Development Mode

To run backend server:

    cd {project_dir}/crime-predictor
    mvn spring-boot:run
    
 Open browser to: http://localhost:8085


To run front end:

    npm install /* if running for the first time */
    
    cd to {project_dir}/dashboard
    npm start
    
 Open browser to: http://localhost:3000
 
 
### Running as a SpringBoot Jar

Build the project

    cd {project_dir}
    mvn package
    cd crime-predictor/target
    java -jar crime-predictor-0.0.1-SNAPSHOT.jar
  

### Running as a Docker Container

    docker pull ddolgachev/mean-streets
    docker run -p 8085:8085 ddolgachev/mean-streets
     
Open browser to http://localhost:8085     
