
## Mean Streets

###### Installation instructions </p>

1. Setup database

    The application is designed to use a MySQL database for production (which is no longer available), and H2
 for development and testing.

     To setup development version of H2 copy the data directory to the user directory.

    ```
    cd crim-webapp-archive
    mv data ~
    ```

2. Download JavaScript dependencies.

    From the dashboard directory run npm install<p>

    ```
    cd crim-webapp/dashboard
    npm install
    ```

3. To run the backend

    ```
    cd ../crime-predictor
    mvn spring-boot:run
    ```

4. To run the front end
    ```
    cd ../dashboard
    npm start
    ```

