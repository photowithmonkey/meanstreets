-- drop table event;
CREATE TABLE event ( event_id int(11) NOT NULL AUTO_INCREMENT,
						title varchar(255) NOT NULL,
                        all_day varchar(10) NOT NULL,
                        start_year int(11) NOT NULL,
                        start_month int(11) NOT NULL,
                        start_day int(11) NOT NULL,
                        start_hour int(11) DEFAULT NULL,
                        start_minute int(11) DEFAULT NULL,
                        end_year int(11) NOT NULL,
                        end_month int(11) NOT NULL,
                        end_day int(11) NOT NULL,
                        end_hour int(11) DEFAULT NULL,
                        end_minute int(11) DEFAULT NULL,
                        color varchar(255) NOT NULL,
                        PRIMARY KEY (event_id));

INSERT INTO event (title, all_day, start_year, start_month, start_day, start_hour, start_minute, end_year, end_month, end_day, end_hour, end_minute, color) VALUES ('Scrum 6th', 'true', 2019, 11, 6, 8, 0, 2019, 11, 6, 10, 30, 'default');
INSERT INTO event (title, all_day, start_year, start_month, start_day, start_hour, start_minute, end_year, end_month, end_day, end_hour, end_minute, color) VALUES ('Scrum 9th', 'false', 2019, 11, 9, 8, 0, 2019, 11, 9, 10, 30, 'default');
INSERT INTO event (title, all_day, start_year, start_month, start_day, start_hour, start_minute, end_year, end_month, end_day, end_hour, end_minute, color) VALUES ('Scrum 13th', 'true', 2019, 11, 13, 8, 0, 2019, 11, 13, 10, 30, 'default');
INSERT INTO event (title, all_day, start_year, start_month, start_day, start_hour, start_minute, end_year, end_month, end_day, end_hour, end_minute, color) VALUES ('Scrum 16th', 'false', 2019, 11, 16, 8, 0, 2019, 11, 16, 10, 30, 'default');
INSERT INTO event (title, all_day, start_year, start_month, start_day, start_hour, start_minute, end_year, end_month, end_day, end_hour, end_minute, color) VALUES ('Scrum 20th', 'false', 2019, 11, 20, 8, 0, 2019, 11, 20, 10, 30, 'default');
INSERT INTO event (title, all_day, start_year, start_month, start_day, start_hour, start_minute, end_year, end_month, end_day, end_hour, end_minute, color) VALUES ('Scrum 23rd', 'true', 2019, 11, 23, 8, 0, 2019, 11, 23, 10, 30, 'yellow');
INSERT INTO event (title, all_day, start_year, start_month, start_day, start_hour, start_minute, end_year, end_month, end_day, end_hour, end_minute, color) VALUES ('Scrum 27th', 'false', 2019, 11, 27, 8, 0, 2019, 11, 27, 10, 30, 'blue');
INSERT INTO event (title, all_day, start_year, start_month, start_day, start_hour, start_minute, end_year, end_month, end_day, end_hour, end_minute, color) VALUES ('Scrum 30th', 'false', 2019, 11, 30, 8, 0, 2019, 11, 30, 10, 30, 'purple');
INSERT INTO event (title, all_day, start_year, start_month, start_day, start_hour, start_minute, end_year, end_month, end_day, end_hour, end_minute, color) VALUES ('Graduation Day', 'true', 2019, 12, 21, 8, 0, 2019, 12, 21, 10, 30, 'default');
INSERT INTO event (title, all_day, start_year, start_month, start_day, start_hour, start_minute, end_year, end_month, end_day, end_hour, end_minute, color) VALUES ('Christmas Eve', 'true', 2019, 12, 24, 8, 0, 2019, 12, 24, 10, 30, 'green');
INSERT INTO event (title, all_day, start_year, start_month, start_day, start_hour, start_minute, end_year, end_month, end_day, end_hour, end_minute, color) VALUES ('Christmas Day', 'true', 2019, 12, 25, 8, 0, 2019, 12, 25, 10, 30, 'red');
INSERT INTO event (title, all_day, start_year, start_month, start_day, start_hour, start_minute, end_year, end_month, end_day, end_hour, end_minute, color) VALUES ('Thanksgiving Day', 'true', 2019, 11, 28, 8, 0, 2019, 11, 28, 10, 30, 'orange');