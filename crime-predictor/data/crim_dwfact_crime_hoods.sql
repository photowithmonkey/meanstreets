create table crime_hoods
(
    id          int auto_increment
        primary key,
    hood        varchar(255) null,
    total_crime int          not null,
    year        int          null
);

INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East Harlem', 12393, 2006);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East New York', 15053, 2006);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Midwood', 14704, 2006);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Washington Heights', 10577, 2006);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Woodlawn', 8746, 2006);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East Harlem', 13744, 2007);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East New York', 15741, 2007);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Midwood', 13962, 2007);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Washington Heights', 10908, 2007);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Woodlawn', 9175, 2007);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East Harlem', 13685, 2008);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East New York', 15687, 2008);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Midwood', 13822, 2008);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Washington Heights', 10620, 2008);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Woodlawn', 9306, 2008);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East Harlem', 13659, 2009);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East New York', 16894, 2009);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Midwood', 13191, 2009);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Washington Heights', 10211, 2009);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Woodlawn', 9208, 2009);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East Harlem', 13784, 2010);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East New York', 16749, 2010);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Midwood', 12799, 2010);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Washington Heights', 9621, 2010);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Woodlawn', 9235, 2010);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East Harlem', 12666, 2011);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East New York', 16978, 2011);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Midwood', 12700, 2011);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Washington Heights', 9209, 2011);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Woodlawn', 8664, 2011);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East Harlem', 13177, 2012);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East New York', 17256, 2012);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Midwood', 12838, 2012);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Washington Heights', 9327, 2012);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Woodlawn', 9171, 2012);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East Harlem', 13294, 2013);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East New York', 17038, 2013);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Midwood', 12572, 2013);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Washington Heights', 9516, 2013);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Woodlawn', 8521, 2013);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East Harlem', 13695, 2014);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East New York', 17532, 2014);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Midwood', 12171, 2014);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Washington Heights', 9187, 2014);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Woodlawn', 8878, 2014);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East Harlem', 13639, 2015);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East New York', 16138, 2015);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Midwood', 12241, 2015);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Washington Heights', 9376, 2015);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Woodlawn', 9175, 2015);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East Harlem', 13059, 2016);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East New York', 15067, 2016);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Midwood', 11785, 2016);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Washington Heights', 9681, 2016);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Woodlawn', 10355, 2016);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East Harlem', 12812, 2017);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East New York', 15055, 2017);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Midwood', 11341, 2017);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Washington Heights', 9949, 2017);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Woodlawn', 10198, 2017);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East Harlem', 12331, 2018);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East New York', 14791, 2018);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Midwood', 10956, 2018);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Washington Heights', 9432, 2018);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Woodlawn', 9651, 2018);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East Harlem', 398, 2019);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East New York', 481, 2019);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Midwood', 311, 2019);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Washington Heights', 304, 2019);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Woodlawn', 304, 2019);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East Harlem', 271, 2020);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('East New York', 329, 2020);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Midwood', 214, 2020);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Washington Heights', 202, 2020);
INSERT INTO crim_dwfact.crime_hoods (hood, total_crime, year) VALUES ('Woodlawn', 202, 2020);