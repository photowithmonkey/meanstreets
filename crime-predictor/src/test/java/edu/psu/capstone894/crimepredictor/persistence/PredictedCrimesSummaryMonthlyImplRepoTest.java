package edu.psu.capstone894.crimepredictor.persistence;

import edu.psu.capstone894.crimepredictor.model.PredictedCrimesSummaryMonthly;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class PredictedCrimesSummaryMonthlyImplRepoTest {

    @Autowired
    PredictedCrimesSummaryMonthlyRepo predictedCrimesSummaryMonthlyRepo;

    @Test
    void findAllByReportedDateBetween() {
        Integer year = 2018;
        List<PredictedCrimesSummaryMonthly> result = predictedCrimesSummaryMonthlyRepo.findAllByYear(year);
        Assert.assertEquals(36, result.size());
    }
}