package edu.psu.capstone894.crimepredictor.controllers;

import edu.psu.capstone894.crimepredictor.api.DataAbstractServices;
import edu.psu.capstone894.crimepredictor.api.PredictedCrimesSummaryMonthlySrv;
import edu.psu.capstone894.crimepredictor.api.PredictedCrimesSummarySrv;
import edu.psu.capstone894.crimepredictor.model.*;
import edu.psu.capstone894.crimepredictor.model.types.PropertyCrimeValues;
import edu.psu.capstone894.crimepredictor.util.JsonMatcher;
import edu.psu.capstone894.crimepredictor.util.ResultSizeMatcher;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static edu.psu.capstone894.crimepredictor.util.TestDataGenerators.getTestList;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class DataAbstractControllerTest {

    private static final String BASE_URL = "http://localhost:8085/api";

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @MockBean
    private PredictedCrimesSummarySrv predictedCrimesSummarySrv;

    @MockBean
    private DataAbstractServices dataAbstractServices;

    @MockBean
    private PredictedCrimesSummaryMonthlySrv predictedCrimesSummaryMonthlySrv;

    @BeforeEach
    void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac)
                .build();
    }

    @Test
    void getPredictedPropertyAverageValues() throws Exception {

        List<PropertyCrimeValues> testData = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            testData.add(PropertyCrimeValuesImpl.builder()
                    .clusterDate(LocalDate.now())
                    .totalCrime(45.0)
                    .price(500000.0)
                    .build());
        }

        given(dataAbstractServices.getPredictedPropertyAverageValues())
                .willReturn(testData);


        ResultSizeMatcher resultSizeMatcher = new ResultSizeMatcher("Timeline query", 10);

        this.mockMvc.perform(get(BASE_URL + "/abstracts/properties")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(resultSizeMatcher));
    }

    @Test
    void getPredictedPropertyValuesByZip() throws Exception {

        List<PredictedCrimePropertyValues> testData = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            testData.add(PredictedCrimePropertyValues.builder()
                    .clusterDate(LocalDate.now())
                    .zipcode("48103")
                    .price(500000.0)
                    .build());
        }

        given(dataAbstractServices.getPredictedPropertyValues("48103"))
                .willReturn(testData);


        ResultSizeMatcher resultSizeMatcher = new ResultSizeMatcher("Timeline query", 10);

        this.mockMvc.perform(get(BASE_URL + "/abstracts/properties/48103")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(resultSizeMatcher));
    }

    @Test
    void getPredictedCrimesSummary() throws Exception {

        JSONArray testData = getTestJsonArray(20);
        given(predictedCrimesSummarySrv.getPredictedCrimesSummary()).willReturn(testData);

        this.mockMvc.perform(get(BASE_URL + "/abstracts/annual")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(new JsonMatcher(testData)));
    }

    @Test
    void getPredictedCrimeTypesForYear() throws Exception {

        List<PredictedCrimesSummary> testData = getTestList(PredictedCrimesSummary.class, 20);
        given(predictedCrimesSummarySrv.getCrimeBreakdownForYear(2008))
                .willReturn(testData);

        this.mockMvc.perform(get(BASE_URL + "/abstracts/annual_by_type/2008")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(new ResultSizeMatcher("CrimesTypesForYear", 20)));
    }

    @Test
    void getPredictedCrimesSummaryMonthly() throws Exception {

        JSONArray testData = getTestJsonArray(20);
        given(predictedCrimesSummaryMonthlySrv.getPredictedCrimesSummaryMonthly(2008))
                .willReturn(testData);

        this.mockMvc.perform(get(BASE_URL + "/abstracts/monthly_by_type/2008")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(new ResultSizeMatcher("CrimeSummaryMonthly", 20)));
    }


    @Test
    void getCrimesByWeek() throws Exception {
        List<IncidentsByWeek> testData = getTestList(IncidentsByWeek.class, 20);
        given(dataAbstractServices.getCrimesByWeek()).willReturn(testData);

        this.mockMvc.perform(get(BASE_URL + "/abstracts/weekly")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(new ResultSizeMatcher("CrimesByWeek", 20)));
    }

    @Test
    void getAllZipCodes() throws Exception {

        List<String> testData = getTestList(String.class, 20);
        given(dataAbstractServices.getAllZipCodes()).willReturn(testData);

        this.mockMvc.perform(get(BASE_URL + "/abstracts/properties/zips")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(new ResultSizeMatcher("AllZipCodes", 20)));
    }

    @Test
    void getCrimeByNeighborhood() throws Exception {
        List<CrimeHoods> testData = getTestList(CrimeHoods.class, 20);
        given(dataAbstractServices.getCrimeHoodsForYear(2008)).willReturn(testData);

        this.mockMvc.perform(get(BASE_URL + "/abstracts/crimehood/2008")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(new ResultSizeMatcher("CrimeByNeighborhood", 20)));
    }

    private JSONArray getTestJsonArray(int num) {
        JSONArray testArray = new JSONArray();
        for (int i = 0; i < num; i++) {
            JSONObject obj = new JSONObject();
            obj.put("idx", i);
            testArray.add(obj);
        }

        return testArray;
    }
}