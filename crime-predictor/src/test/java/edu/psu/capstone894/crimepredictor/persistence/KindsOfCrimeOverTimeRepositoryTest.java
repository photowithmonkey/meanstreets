package edu.psu.capstone894.crimepredictor.persistence;

import edu.psu.capstone894.crimepredictor.model.KindsOfCrimeOverTime;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class KindsOfCrimeOverTimeRepositoryTest {

    @Autowired
    private KindsOfCrimeOverTimeRepository kindsOfCrimeOverTimeRepository;

    @Test
    void findAllByCrimeType() {
        List<KindsOfCrimeOverTime> felonies = kindsOfCrimeOverTimeRepository
                .findAllByCrimeTypeAndMonthIdx("FELONY",10);
        assertEquals(10004, felonies.size());
    }
}