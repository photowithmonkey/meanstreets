package edu.psu.capstone894.crimepredictor.persistence;

import edu.psu.capstone894.crimepredictor.model.CrimeEvent;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class EventsRepositoryTest {

    @Autowired
    EventRepository eventRepository;

    @BeforeEach
    public void startUp() {
        CrimeEvent event1 = CrimeEvent.builder()
                .startYear(2019)
                .startMonth(11)
                .startDay(5)
                .build();

        CrimeEvent event2 = CrimeEvent.builder()
                .startYear(2019)
                .startMonth(2)
                .startDay(27)
                .build();

        eventRepository.deleteAll();
        eventRepository.save(event1);
        eventRepository.save(event2);
    }

    private List<CrimeEvent> getFromRepository() {
        List<CrimeEvent> list = eventRepository.findAll();
        return list;
    }

    @Test
    public void findAllEvents() {
        List<CrimeEvent> events = getFromRepository();
        Assert.assertEquals(2, events.size());
    }
}
