package edu.psu.capstone894.crimepredictor.services;

import edu.psu.capstone894.crimepredictor.api.DataAbstractServices;
import edu.psu.capstone894.crimepredictor.model.CrimeHoods;
import edu.psu.capstone894.crimepredictor.model.IncidentsByWeek;
import edu.psu.capstone894.crimepredictor.model.PredictedCrimePropertyValues;
import edu.psu.capstone894.crimepredictor.model.PropertyCrimeValuesImpl;
import edu.psu.capstone894.crimepredictor.model.types.PropertyCrimeValues;
import edu.psu.capstone894.crimepredictor.persistence.CrimeByDayOfMonthRepository;
import edu.psu.capstone894.crimepredictor.persistence.CrimeHoodRepository;
import edu.psu.capstone894.crimepredictor.persistence.PredictedCrimePropertyValuesRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static edu.psu.capstone894.crimepredictor.util.TestDataGenerators.getTestList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;

@SpringBootTest
class DataAbstractServicesImplTest {

    @Autowired
    private DataAbstractServices dataAbstractServices;

    @MockBean
    private PredictedCrimePropertyValuesRepository predictedCrimePropertyValuesRepository;

    @MockBean
    private CrimeHoodRepository crimeHoodRepository;

    @MockBean
    private CrimeByDayOfMonthRepository crimeByDayOfMonthRepository;

    @Test
    void getPredictedPropertyValues() {

        List<PredictedCrimePropertyValues> testData = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            testData.add(PredictedCrimePropertyValues
                    .builder()
                    .idx(i)
                    .clusterDate(LocalDate.now())
                    .totalCrime(100.0)
                    .price(500000.00)
                    .zipcode("48103")
                    .build());
        }

        given(predictedCrimePropertyValuesRepository.findAllByZipcode("48103"))
                .willReturn(testData);

        List<PredictedCrimePropertyValues> allByZipcode
                = dataAbstractServices.getPredictedPropertyValues("48103");

        assertEquals(10, allByZipcode.size());
    }

    @Test
    void getPredictedPropertyAverageValues() {

        List<PropertyCrimeValues> testData = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            testData.add(PropertyCrimeValuesImpl
                    .builder()
                    .clusterDate(LocalDate.now())
                    .totalCrime(100.0)
                    .price(500000.00)
                    .build());
        }

        given(predictedCrimePropertyValuesRepository.findAverageForAllZipCodes())
                .willReturn(testData);

        List<PropertyCrimeValues> averageForAllZipCodes
                = dataAbstractServices.getPredictedPropertyAverageValues();

        assertEquals(10, averageForAllZipCodes.size());
    }


    @Test
    void getAllZipCodes() throws InstantiationException, IllegalAccessException {

        List<String> testData = getTestList(String.class, 20);
        given(predictedCrimePropertyValuesRepository.getAllZipCodes()).willReturn(testData);

        List<String> allZipCodes = dataAbstractServices.getAllZipCodes();
        assertEquals(20, allZipCodes.size());
    }

    @Test
    void getCrimesByWeek() throws InstantiationException, IllegalAccessException {

        List<IncidentsByWeek> testData = getTestList(IncidentsByWeek.class, 20);
        given(crimeByDayOfMonthRepository.findAll()).willReturn(testData);

        List<IncidentsByWeek> crimesByWeek = dataAbstractServices.getCrimesByWeek();
        assertEquals(20, crimesByWeek.size());
    }

    @Test
    void getCrimeHoodsForYear() throws InstantiationException, IllegalAccessException {

        List<CrimeHoods> testData = getTestList(CrimeHoods.class, 20);

        given(crimeHoodRepository.findAllByYear(2008)).willReturn(testData);

        List<CrimeHoods> crimeHoodsForYear = dataAbstractServices.getCrimeHoodsForYear(2008);
        assertEquals(20, crimeHoodsForYear.size());
    }

}