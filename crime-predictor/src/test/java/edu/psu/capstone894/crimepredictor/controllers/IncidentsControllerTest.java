package edu.psu.capstone894.crimepredictor.controllers;

import edu.psu.capstone894.crimepredictor.api.IncidentsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.LinkedList;

import static java.time.LocalDate.now;
import static java.time.temporal.TemporalAdjusters.firstDayOfYear;
import static java.time.temporal.TemporalAdjusters.lastDayOfYear;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class IncidentsControllerTest {

    private MockMvc mockMvc;

    private static final String BASE_URL = "http://localhost:8085/api";

    @Autowired
    private WebApplicationContext wac;

    @BeforeEach
    void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac)
                .build();
    }

    @MockBean
    IncidentsService incidentsService;

    @Autowired
    IncidentsController incidentsController;

    @Test
    void findIncidents() throws Exception {
        this.mockMvc.perform(get(BASE_URL + "/incidents")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    private static LinkedList<String> link(String... values) {
        return new LinkedList<>(Arrays.asList(values));
    }

    @Test
    void findIncidents2() throws Exception {

        this.mockMvc.perform(get(BASE_URL + "/incidents?weapon=hand&description=larceny&city=Baltimore")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.put("city", link("Baltimore"));
        map.put("weapon", link("hand"));
        map.put("description", link("larceny"));

        Pageable pageable = PageRequest.of(0, 20);

        LocalDate dateFrom = now().with(firstDayOfYear());
        LocalDate dateTo = now().with(lastDayOfYear());

        class ListMatcher implements ArgumentMatcher<MultiValueMap<String, String>> {

            private final MultiValueMap<String, String> target;

            public ListMatcher(MultiValueMap<String, String> target) {
                this.target = target;
            }

            @Override
            public boolean matches(MultiValueMap<String, String> map) {
                return target.equals(map);
            }
        }

        verify(incidentsService).queryIncidents(argThat(new ListMatcher(map)), eq(dateFrom), eq(dateTo), eq(pageable));
//        verify(incidentsService).queryIncidents(argThat(new MapMatcher(map)), eq(dateFrom), eq(dateTo), eq(pageable));
    }
}

