package edu.psu.capstone894.crimepredictor.util;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.hamcrest.CustomMatcher;

public class JsonMatcher extends CustomMatcher<String> {

    private String jsonString;

    public JsonMatcher(JSONArray jsonArray) {
        this(jsonArray.toJSONString());
    }

    public JsonMatcher(JSONObject jsonObject) {
        this(jsonObject.toJSONString());
    }

    public JsonMatcher(String jsonString) {
        super(jsonString);
        this.jsonString = jsonString;
    }

    @Override
    public boolean matches(Object o) {
        String testJson = ((String) o);
        return jsonString.equals(testJson);
    }
}
