package edu.psu.capstone894.crimepredictor.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

@SpringBootTest
class RedirectToIndexFilterTest {

    @Autowired
    RedirectToIndexFilter redirectToIndexFilter;

    @MockBean
    HttpServletRequest request;

    @MockBean
    ServletResponse response;

    @MockBean
    FilterChain filterChain;

    @MockBean
    RequestDispatcher requestDispatcher;


    @Test
    void doFilter() throws IOException, ServletException {
        given(request.getRequestURI()).willReturn("/api/testpath");
        redirectToIndexFilter.doFilter(request, response, filterChain);
        then(filterChain).should().doFilter(request, response);
    }


    @Test
    void doFilter2() throws IOException, ServletException {
        given(request.getRequestURI()).willReturn("/take/me/somewhere");
        given(request.getRequestDispatcher("/")).willReturn(requestDispatcher);

        redirectToIndexFilter.doFilter(request, response, filterChain);
        then(requestDispatcher).should().forward(request, response);
    }

}