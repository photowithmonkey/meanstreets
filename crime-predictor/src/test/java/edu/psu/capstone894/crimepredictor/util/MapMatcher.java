package edu.psu.capstone894.crimepredictor.util;

import org.mockito.ArgumentMatcher;

import java.util.Map;

// Mockito cannot match two HashMaps using plain Map.equals, so this custom Matcher
// is used to compare them

public class MapMatcher implements ArgumentMatcher<Map<String, String[]>> {

    private Map<String, String[]> expectedMap;

    public MapMatcher(Map<String, String[]> expectedMap) {
        this.expectedMap = expectedMap;
    }

    public String toString() {
        return "Map<String,String[]>";
    }

    @Override
    public boolean matches(Map<String, String[]> map) {

        if (map.size() != expectedMap.size())
            return false;

        for (String value : expectedMap.keySet()) {
            String[] expectedValue = expectedMap.get(value);
            String[] actualValue = map.get(value);
            if (expectedValue.length != actualValue.length)
                return false;

            for (int i = 0; i < expectedValue.length; i++) {
                if (!expectedValue[i].equals(actualValue[i]))
                    return false;
            }
        }

        return true;
    }
}
