package edu.psu.capstone894.crimepredictor.services;

import edu.psu.capstone894.crimepredictor.model.CrimeEvent;
import edu.psu.capstone894.crimepredictor.persistence.EventRepository;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;

@SpringBootTest
public class EventsServiceImplTest {

    @MockBean
    EventRepository eventRepository;

    @Autowired
    private EventsServiceImpl eventsService;

    @Test
    void getAllEventsTest() {
        // A unit test should only test the specific unit of code and not its dependents
        // therefore, you want to mock the repository layer instead of using it

        // crate some test data that I would expect the repository to return
        CrimeEvent event1 = new CrimeEvent();
        CrimeEvent event2 = new CrimeEvent();
        List<CrimeEvent> testData = new ArrayList<>();
        testData.add(event1);
        testData.add(event2);

        // when called with the following parameters
        Sort sort =  Sort.by(Sort.DEFAULT_DIRECTION, "title");
        given(eventRepository.findAll(sort)).willReturn(testData);

        List<CrimeEvent> result = eventsService.getAllEvents();
        Assert.assertEquals(2, result.size());
    }
}
