package edu.psu.capstone894.crimepredictor.util;

import com.jayway.jsonpath.JsonPath;
import org.hamcrest.CustomMatcher;

import java.util.List;

public class ResultSizeMatcher extends CustomMatcher {

    private long size;

    public ResultSizeMatcher(String description, long size) {
        super(description);
        this.size = size;
    }

    @Override public boolean matches(Object o) {
        String json = (String) o;
        List<String> items = JsonPath.read(json, "$");
        return items.size() == this.size;
    }
}
