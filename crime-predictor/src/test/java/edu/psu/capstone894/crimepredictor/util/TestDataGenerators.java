package edu.psu.capstone894.crimepredictor.util;

import java.util.ArrayList;
import java.util.List;

public class TestDataGenerators {

    // generate a dummy list of specified size and type - records are just empty objects
    // TODO member objects are either Mocks or simulated data classes
    public static <T> List<T> getTestList(Class<T> tClass, int size) throws IllegalAccessException, InstantiationException {
        List<T> list = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            list.add(tClass.newInstance());
        }
        return list;
    }
}
