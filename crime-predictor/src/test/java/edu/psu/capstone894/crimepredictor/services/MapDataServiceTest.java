package edu.psu.capstone894.crimepredictor.services;

import com.jayway.jsonpath.JsonPath;
import edu.psu.capstone894.crimepredictor.api.MapDataService;
import edu.psu.capstone894.crimepredictor.model.types.IncidentsByLocation;
import edu.psu.capstone894.crimepredictor.model.KindsOfCrimeOverTime;
import edu.psu.capstone894.crimepredictor.persistence.KindsOfCrimeOverTimeRepository;
import edu.psu.capstone894.crimepredictor.persistence.PredictedCrimePropertyValuesRepository;
import edu.psu.capstone894.crimepredictor.persistence.PredictedTypeLocResultsRepository;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;

@SpringBootTest
class MapDataServiceTest {

    @MockBean
    private PredictedTypeLocResultsRepository predictedTypeLocRepository;

    @MockBean
    private KindsOfCrimeOverTimeRepository kindsOfCrimeOverTimeRepository;

    @MockBean
    private PredictedCrimePropertyValuesRepository predictedCrimePropertyValuesRepository;

    @Autowired
    private MapDataService mapDataService;

    @Test
    void getIncidentsByLocation() {

        List<IncidentsByLocation> testData = new ArrayList<>();
        testData.add(new TestIncident("FELONIOUS", "Jaywalking", 5));
        testData.add(new TestIncident("FELONIOUS", "Nose Picking", 3));
        testData.add(new TestIncident("FELONIOUS", "Gum Chewing", 7));
        testData.add(new TestIncident("FELONIOUS", "Book Reading", 3));
        testData.add(new TestIncident("LASCIVIOUS", "French Kissing", 5));
        testData.add(new TestIncident("LASCIVIOUS", "Strong Hugging", 5));
        testData.add(new TestIncident("LASCIVIOUS", "Hand Holding", 5));
        testData.add(new TestIncident("OBNOXIOUS", "Starring", 5));
        testData.add(new TestIncident("OBNOXIOUS", "Spitting", 5));
        testData.add(new TestIncident("OBNOXIOUS", "Frowning", 5));
        testData.add(new TestIncident("OBNOXIOUS", "Yawning", 5));

        given(predictedTypeLocRepository.getIncidentsForLocation(40.64336, -73.95777, 5))
                .willReturn(testData);

        JSONObject incidentsByLocation
                = mapDataService.getIncidentsByLocation(40.64336, -73.95777, 5);

        String result = JsonPath.parse(incidentsByLocation).read("$.children[2].children[3].name");
        assertEquals("Yawning", result);
    }

    @Test
    void KindsOfCrimeOverTimeTest() {

        List<KindsOfCrimeOverTime> testData = new ArrayList<>(5);
        for (int i = 0; i < 5; i++) {
            KindsOfCrimeOverTime kindsOfCrimeOverTime = KindsOfCrimeOverTime
                    .builder()
                    .crimeType("FELONY")
                    .lat(45.0)
                    .lng(-74.0)
                    .month(1)
                    .monthIdx(i)
                    .year(2017)
                    .build();

            testData.add(kindsOfCrimeOverTime);
        }


        given(kindsOfCrimeOverTimeRepository.findAllByCrimeTypeAndMonthIdx("FELONY", 10))
                .willReturn(testData);

        JSONArray felony = mapDataService.getKindsOfCrimeOverTime("FELONY", 10);
        assertEquals(5, felony.size());
    }


}

@Getter
@AllArgsConstructor
class TestIncident implements IncidentsByLocation {

    private String crime_Description;
    private String crime_Type;
    private Integer incidents;
}