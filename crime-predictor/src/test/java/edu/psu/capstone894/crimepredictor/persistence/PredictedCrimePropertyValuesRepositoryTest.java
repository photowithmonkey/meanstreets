package edu.psu.capstone894.crimepredictor.persistence;

import edu.psu.capstone894.crimepredictor.model.PredictedCrimePropertyValues;
import edu.psu.capstone894.crimepredictor.model.types.PropertyCrimeValues;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class PredictedCrimePropertyValuesRepositoryTest {

    @Autowired
    private PredictedCrimePropertyValuesRepository predictedCrimePropertyValuesRepository;

    @Test
    void findAllByZipcode() {
        List<PredictedCrimePropertyValues> result = predictedCrimePropertyValuesRepository.findAllByZipcode("11208");
        Assert.assertEquals(131, result.size());
        Assert.assertNotNull(result.get(0).getClusterDate());
        Assert.assertNotNull(result.get(0).getTotalCrime());
        Assert.assertNotNull(result.get(0).getPrice());
        Assert.assertNotNull(result.get(130).getClusterDate());
        Assert.assertNotNull(result.get(130).getTotalCrime());
        Assert.assertNotNull(result.get(130).getPrice());
    }

    @Test
    void findAverageForAllZipCodes() {
        List<PropertyCrimeValues> result = predictedCrimePropertyValuesRepository.findAverageForAllZipCodes();
        Assert.assertEquals(131, result.size());

        Assert.assertNotNull(result.get(123).getClusterDate());
        Assert.assertNotNull(result.get(123).getTotalCrime());
        Assert.assertNotNull(result.get(123).getPrice());
    }
}