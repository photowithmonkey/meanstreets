package edu.psu.capstone894.crimepredictor.controllers;

import edu.psu.capstone894.crimepredictor.api.AttributesService;
import edu.psu.capstone894.crimepredictor.model.Attribute;
import edu.psu.capstone894.crimepredictor.util.JsonMatcher;
import edu.psu.capstone894.crimepredictor.util.ResultSizeMatcher;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class AttributeControllerTest {

    private MockMvc mockMvc;

    private static final String BASE_URL = "http://localhost:8085/api";

    @Autowired
    private WebApplicationContext wac;

    @BeforeEach
    void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac)
                .build();
    }

    @MockBean
    AttributesService attributesService;

    @Autowired
    AttributeController attributeController;

    @Test
    void findAttributesByType() throws Exception {

        List<Attribute> testData = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            testData.add(new Attribute("weapon", "Weapon" + i, "Baltimore"));
        }

        given(this.attributesService.findAttributeByType("weapon")).willReturn(testData);

        ResultSizeMatcher resultSizeMatcher = new ResultSizeMatcher("Attributes for weapon", 10);

        this.mockMvc.perform(get(BASE_URL + "/attributes/type/weapon")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(resultSizeMatcher));
    }

    @Test
    void getAllAttributeTest() throws Exception {

        JSONObject jsonObject = new JSONObject();
        JSONArray weapons = new JSONArray();
        JSONArray crimeType = new JSONArray();
        JSONArray city = new JSONArray();

        for (int i = 0; i < 10; i++) {
            JSONObject o = new JSONObject();
            o.put("type", "weapon");
            o.put("label", "Weapon" + i);
            weapons.add(o);
        }
        jsonObject.put("weapon", weapons);

        for (int i = 0; i < 3; i++) {
            JSONObject o = new JSONObject();
            o.put("type", "city");
            o.put("label", "City" + i);
            city.add(o);
        }
        jsonObject.put("city", city);

        for (int i = 0; i < 7; i++) {
            JSONObject o = new JSONObject();
            o.put("type", "type");
            o.put("label", "CrimeType" + i);
            crimeType.add(o);
        }
        jsonObject.put("crimeType", crimeType);

        given(this.attributesService.getAllAttributes()).willReturn(jsonObject);
        this.mockMvc.perform(get(BASE_URL + "/attributes")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(new JsonMatcher(jsonObject)));
    }
}