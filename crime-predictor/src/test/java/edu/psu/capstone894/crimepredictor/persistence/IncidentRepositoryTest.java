package edu.psu.capstone894.crimepredictor.persistence;

import edu.psu.capstone894.crimepredictor.model.types.MaxMinDate;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

@SpringBootTest
class IncidentRepositoryTest {

    @Autowired
    IncidentRepository incidentRepository;

    @Test
    void findMaxAndMinDates() {
        MaxMinDate maxAndMinDates = incidentRepository.findMaxAndMinDates("Baltimore");

        LocalDate mindate = LocalDate.of(1963, 10, 30);
        LocalDate maxdate = LocalDate.of(2019, 9, 7);
        Assert.assertEquals(mindate, maxAndMinDates.getMinDate());
        Assert.assertEquals(maxdate, maxAndMinDates.getMaxDate());
    }
}