package edu.psu.capstone894.crimepredictor.controllers;

import com.jayway.jsonpath.JsonPath;
import edu.psu.capstone894.crimepredictor.api.EventsService;
import edu.psu.capstone894.crimepredictor.model.CrimeEvent;
import edu.psu.capstone894.crimepredictor.util.ResultSizeMatcher;
import org.hamcrest.CustomMatcher;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class EventsControllerTest {

    private MockMvc mockMvc;

    private static final String BASE_URL = "http://localhost:8085/api";

    @Autowired
    private WebApplicationContext wac;

    @BeforeEach
    void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac)
                .build();
    }

    @MockBean
    EventsService eventsService;

    @Autowired
    EventsController eventsController;

    @Order(4)
    @Test
    void getAllEventsTest() throws Exception {
        given(this.eventsService.getAllEvents()).willReturn(createTestData());

//        JSONObject jsonObject = new JSONObject();
//        JSONArray jsonArray = buildEventsArray();
//        jsonObject.put("events", jsonArray);

        ResultSizeMatcher resultSizeMatcher = new ResultSizeMatcher("Number of events", 2);
        JsonContentMatcher jsonContentMatcher = new JsonContentMatcher("Second year is not 2017");

        this.mockMvc.perform(get(BASE_URL + "/events")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(resultSizeMatcher)) // test to make sure all 2 records returned
                .andExpect(content().string(jsonContentMatcher)); // test if the JSON matches the test data objects
    }

    private List<CrimeEvent> createTestData() {

        CrimeEvent event1 = CrimeEvent.builder()
                .startYear(2019)
                .startMonth(11)
                .startDay(5)
                .build();

        CrimeEvent event2 = CrimeEvent.builder()
                .startYear(2017)
                .startMonth(2)
                .startDay(27)
                .build();

        List<CrimeEvent> testData = new ArrayList<>();
        testData.add(event1);
        testData.add(event2);

        return testData;
    }

/*
    private JSONArray buildEventsArray() {

        JSONArray arr = new JSONArray();
        JSONObject json = new JSONObject();
        json.put("title", "First Event");
        json.put("allDay", "false");
        json.put("startYear", "2019");
        json.put("startMonth", "10");
        json.put("startDay", "5");
        json.put("startHour", "15");
        json.put("startMinute", "30");
        json.put("endYear", "2019");
        json.put("endMonth", "10");
        json.put("endDay", "5");
        json.put("endHour", "16");
        json.put("endMinute", "30");
        arr.add(json);

        json.put("title", "Second Event");
        json.put("allDay", "true");
        json.put("startYear", "2019");
        json.put("startMonth", "10");
        json.put("startDay", "6");
        json.put("startHour", "15");
        json.put("startMinute", "30");
        json.put("endYear", "2019");
        json.put("endMonth", "10");
        json.put("endDay", "6");
        json.put("endHour", "16");
        json.put("endMinute", "30");
        arr.add(json);

        return arr;
    }
*/

/*
    static class JsonMatcher extends CustomMatcher<String> {

        private JSONObject jsonObject;

        JsonMatcher(JSONObject jsonObject) {
            super("Match returned Events Json");
            this.jsonObject = jsonObject;
        }

        @Override
        public boolean matches(Object o) {
            String testJson = ((String) o);
            return jsonObject.toJSONString().equals(testJson);
        }
    }
*/
}


class JsonContentMatcher extends CustomMatcher {


    public JsonContentMatcher(String description) {
        super(description);
    }

    @Override
    public boolean matches(Object o) {
        String json = (String) o;
        int year = JsonPath.read(json, "$[1].startYear");
        int month = JsonPath.read(json, "$[0].startMonth");

        return year == 2017 && month == 11;
    }
}
