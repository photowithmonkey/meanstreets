package edu.psu.capstone894.crimepredictor.services;

import edu.psu.capstone894.crimepredictor.model.Attribute;
import edu.psu.capstone894.crimepredictor.model.types.MaxMinDate;
import edu.psu.capstone894.crimepredictor.persistence.AttributeRepository;
import edu.psu.capstone894.crimepredictor.persistence.IncidentRepository;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;

@SpringBootTest
class AttributesServiceImplTest {

    @MockBean
    AttributeRepository attributeRepository;

    @MockBean
    IncidentRepository incidentRepository;

    @Autowired
    private AttributesServiceImpl attributesService;

    @Test
    void findAttributeByType() {

        List<Attribute> testData = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            testData.add(new Attribute("weapon", "Weapon" + i, "New York"));
        }

        given(attributeRepository.findAllByType("weapon", Sort.by("label"))).willReturn(testData);

        List<Attribute> result = attributesService.findAttributeByType("weapon");
        Assert.assertEquals(testData, result);
    }


    @Test
    void findAttributeByCity() {

        List<Attribute> testData = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            testData.add(new Attribute("TestAttribute", "Some test attribute " + i, "New York"));
        }

        given(attributeRepository.findAllByCity("New York", Sort.by("type", "label"))).willReturn(testData);

        LocalDate testDate = LocalDate.of(1917, 10, 1);
        MaxMinDate maxMinDate = new TestMaxMinDate(testDate, testDate);
        given(incidentRepository.findMaxAndMinDates("New York")).willReturn(maxMinDate);

        JSONObject result = attributesService.findAttributesByCity("New York");
        JSONObject attributes = ((JSONObject) result.get("attributes"));
        Assert.assertEquals(10, ((JSONArray) attributes.get("TestAttribute")).size());
        Assert.assertEquals(testDate, result.get("mindate"));
        Assert.assertEquals(testDate, result.get("maxdate"));
    }

    @Test
    void getAllAttributes() {
        List<Attribute> testData = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            testData.add(new Attribute("weapon", "Weapon" + i, "Baltimore"));
        }
        for (int i = 0; i < 7; i++) {
            testData.add(new Attribute("description", "Description" + i, "Baltimore"));
        }
        for (int i = 0; i < 3; i++) {
            testData.add(new Attribute("type", "CrimeType" + i, "Baltimore"));
        }

        Sort sort = Sort.by(Sort.DEFAULT_DIRECTION, "type", "label");
        given(attributeRepository.findAll(sort)).willReturn(testData);

        JSONObject allAttributes = attributesService.getAllAttributes();
        Assert.assertEquals(10, ((JSONArray) allAttributes.get("weapon")).size());
        Assert.assertEquals(7, ((JSONArray) allAttributes.get("description")).size());
        Assert.assertEquals(3, ((JSONArray) allAttributes.get("type")).size());
    }
}


@AllArgsConstructor
@Getter
class TestMaxMinDate implements MaxMinDate {
    private LocalDate minDate;
    private LocalDate maxDate;
}