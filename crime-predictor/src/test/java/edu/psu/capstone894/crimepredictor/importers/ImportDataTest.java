package edu.psu.capstone894.crimepredictor.importers;


import edu.psu.capstone894.crimepredictor.persistence.AttributeRepository;
import edu.psu.capstone894.crimepredictor.persistence.EventRepository;
import edu.psu.capstone894.crimepredictor.persistence.IncidentRepository;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.rules.ExpectedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ImportDataTest {

    private Logger logger = LoggerFactory.getLogger(ImportDataTest.class);

    private final static int NUM_LINES = 274188;
    private final static int NUM_LINES_NY = 216226;

    private final static int NUM_ATTRIBUTES = 310;
    private final static int NUM_ATTRIBUTES_NY = 310;

    @Autowired
    private IncidentRepository incidentRepository;

    @Autowired
    private AttributeRepository attributeRepository;

    @Autowired
    private EventRepository eventRepository;

    public final static String fileName = "data/BPD_Part_1_Victim_Based_Crime_Data.txt";


    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    @Order(1)
    void importBaltimoreData() {
        ImportOpenBaltimore importOpenBaltimore = new ImportOpenBaltimore(incidentRepository, attributeRepository);
        attributeRepository.deleteAll();
        incidentRepository.deleteAll();
        long total_processed = importOpenBaltimore.importData();
        long total_added = incidentRepository.count();
        long attributes_added = attributeRepository.count();

        Assert.assertEquals("Total processed", NUM_LINES, total_processed);
        Assert.assertEquals("Total added", NUM_LINES - 1, total_added);
        Assert.assertEquals("Attributes:", NUM_ATTRIBUTES, attributes_added);
    }

    @Test
    @Order(3)
    void importEvents() {
        ImportEvent importEvent = new ImportEvent(eventRepository);
        long totalProcessed = importEvent.importData();
        long expectedProcessed = 2;
        Assert.assertEquals("Total Processed: ", expectedProcessed, totalProcessed);
    }
}
