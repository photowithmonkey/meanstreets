package edu.psu.capstone894.crimepredictor.persistence;

import edu.psu.capstone894.crimepredictor.model.Attribute;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;

import java.util.List;

@SpringBootTest
public class AttributesRepositoryTest {

    @Autowired
    AttributeRepository attributeRepository;


    private List<Attribute> getFromRepository(String fieldName) {
        return attributeRepository.findAllByCityAndType("Baltimore",fieldName, Sort.by("label"));
    }

    @Test
    public void findAllAttributesByType() {
        List<Attribute> weapons = getFromRepository("weapon");
        Assert.assertEquals(6, weapons.size());
    }

    @Test
    public void findAllAttributesByTypeDesc() {
        List<Attribute> descriptions = getFromRepository("description");
        Assert.assertEquals(14, descriptions.size());
    }

    @Test
    public void findAllAttributesByTypeDistrict() {
        List<Attribute> districts = getFromRepository("district");
        Assert.assertEquals(10, districts.size());
    }

    @Test
    public void findAllAttributesByTypeNeighborhood() {
        List<Attribute> neighborhoods = getFromRepository("neighborhood");
        Assert.assertEquals(279, neighborhoods.size());
    }

    @Test
    public void findAllAttributesByCity() {
        List<Attribute> allByCity = attributeRepository.findAllByCity("Baltimore", Sort.by("type", "label"));
        Assert.assertEquals(310, allByCity.size());
    }

}
