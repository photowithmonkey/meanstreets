package edu.psu.capstone894.crimepredictor.persistence;

import edu.psu.capstone894.crimepredictor.model.CrimeHoods;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class CrimeHoodRepositoryTest {

    @Autowired
    private CrimeHoodRepository crimeHoodRepository;

    @Test
    void findAll() {
        List<CrimeHoods> allByYear = crimeHoodRepository.findAll();
        assertEquals(75, allByYear.size());
    }

    @Test
    void findAllByYear() {
        List<CrimeHoods> allByYear = crimeHoodRepository.findAllByYear(2020);
        assertEquals(5, allByYear.size());
    }
}