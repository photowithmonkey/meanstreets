package edu.psu.capstone894.crimepredictor.persistence;

import edu.psu.capstone894.crimepredictor.model.types.GroupedLocations;
import edu.psu.capstone894.crimepredictor.model.types.IncidentsByLocation;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class PredictedTypeLocRepositoryTest {

    @Autowired
    private PredictedTypeLocResultsRepository predictedTypeLocRepository;

    @Test
    void getGroupedLocations() {
        List<GroupedLocations> groupedLocations
                = predictedTypeLocRepository.getGroupedLocations(4);

        assertEquals(2306, groupedLocations.size());
    }

    @Test
    void getIncidentsByLocation() {
        List<IncidentsByLocation> incidentsForLocation
                = predictedTypeLocRepository.getIncidentsForLocation(40.679, -73.896, 3);

        assertEquals(3, incidentsForLocation.size());
    }
}