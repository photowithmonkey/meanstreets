package edu.psu.capstone894.crimepredictor.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.psu.capstone894.crimepredictor.api.MapDataService;
import edu.psu.capstone894.crimepredictor.model.KindsOfCrimeOverTimeSimple;
import edu.psu.capstone894.crimepredictor.util.JsonMatcher;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class MapDataControllerTest {

    private static final String BASE_URL = "http://localhost:8085/api";

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @BeforeEach
    void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac)
                .build();
    }

    @MockBean
    private MapDataService mapDataService;

    @Test
    void getIncidentsByLoc() throws Exception {

        JSONObject testObject = new JSONObject();
        testObject.put("name", "Jupiter");

        given(mapDataService.getIncidentsByLocation(40.64336, -73.95777, 5))
                .willReturn(testObject);

        this.mockMvc.perform(get(BASE_URL + "/map/incidents?lat=40.64336&lng=-73.95777&level=5")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(testObject.toJSONString()));
    }

    @Test
    void getKindsOfCrimeOverTime() throws Exception {

        List<KindsOfCrimeOverTimeSimple> crimes = new ArrayList<>(5);
        for (int i = 0; i < 5; i++) {
            crimes.add(KindsOfCrimeOverTimeSimple.builder()
                    .lat(45.0)
                    .lng(-74.0)
                    .qty(4)
                    .build());
        }

        JSONArray testData = new JSONArray();
        for (int i = 0; i < 5; i++) {
            JSONArray e = new JSONArray();
            e.add(45);
            e.add(-74);
            e.add(4);
            testData.add(e);
        }

        given(mapDataService.getKindsOfCrimeOverTime("felony", 10))
                .willReturn(testData);

        ObjectMapper objectMapper = new ObjectMapper();
        String testJson = objectMapper.writeValueAsString(testData);


        JsonMatcher jsonMatcher = new JsonMatcher(testJson);

        this.mockMvc.perform(get(BASE_URL + "/map/timeline/felony/10")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(jsonMatcher));
    }

}