package edu.psu.capstone894.crimepredictor.services;

import edu.psu.capstone894.crimepredictor.api.IncidentsService;
import edu.psu.capstone894.crimepredictor.model.Incident;
import edu.psu.capstone894.crimepredictor.persistence.IncidentRepository;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.time.LocalDate;
import java.time.Month;
import java.util.*;

import static java.time.LocalDate.now;
import static java.time.temporal.TemporalAdjusters.firstDayOfYear;
import static org.mockito.BDDMockito.given;

@SpringBootTest
class IncidentsServiceImplTest {


    @MockBean
    IncidentRepository incidentRepository;

    @Autowired
    private IncidentsService incidentsService;


    private static LinkedList<String> link(String... values) {
        return new LinkedList<>(Arrays.asList(values));
    }


    @Test
    void queryIncidents() {

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.put("city", link("Baltimore"));
        map.put("weapon", link("HANDS", "KNIFE"));
        map.put("type", link("AGG. ASSAULT"));


        List<Incident> testIncidents = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            Incident incident = new Incident();
            incident.setDescription("Incident_" + i);
            testIncidents.add(incident);
        }

        Pageable pageable = PageRequest.of(0, 20);
        PageImpl<Incident> page = new PageImpl<>(testIncidents);

        LocalDate dateFrom = now().with(firstDayOfYear());
        LocalDate dateTo = now();
        given(incidentRepository.queryBuilderPageable(map, dateFrom, dateTo, pageable)).willReturn(page);

        Page<Incident> result = incidentsService.queryIncidents(map, dateFrom, dateTo, pageable);
        Assert.assertEquals(page, result);
    }


    @Test
    void queryIncidentsShootings() {

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.put("city", link("Baltimore"));
        map.put("weapon", link("FIRE", "FIREARM"));
        map.put("type", link("SHOOTING"));


        List<Incident> testIncidents = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            Incident incident = new Incident();
            incident.setDescription("Incident_" + i);
            testIncidents.add(incident);
        }

        Pageable pageable = PageRequest.of(0, 20);
        PageImpl<Incident> page = new PageImpl<>(testIncidents);

        LocalDate dateFrom = LocalDate.of(2014, Month.DECEMBER, 31);
        LocalDate dateTo = LocalDate.of(2015, Month.DECEMBER, 30);
        given(incidentRepository.queryBuilderPageable(map, dateFrom, dateTo, pageable)).willReturn(page);

        Page<Incident> result = incidentsService.queryIncidents(map, dateFrom, dateTo, pageable);
        Assert.assertEquals(page, result);
    }
}
