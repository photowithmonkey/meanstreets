package edu.psu.capstone894.crimepredictor.services;

import edu.psu.capstone894.crimepredictor.api.DataAbstractServices;
import edu.psu.capstone894.crimepredictor.model.CrimeHoods;
import edu.psu.capstone894.crimepredictor.model.IncidentsByWeek;
import edu.psu.capstone894.crimepredictor.model.PredictedCrimePropertyValues;
import edu.psu.capstone894.crimepredictor.model.PropertyCrimeValuesImpl;
import edu.psu.capstone894.crimepredictor.model.types.PropertyCrimeValues;
import edu.psu.capstone894.crimepredictor.persistence.CrimeByDayOfMonthRepository;
import edu.psu.capstone894.crimepredictor.persistence.CrimeHoodRepository;
import edu.psu.capstone894.crimepredictor.persistence.PredictedCrimePropertyValuesRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class DataAbstractServicesImpl implements DataAbstractServices {

    private PredictedCrimePropertyValuesRepository predictedCrimePropertyValuesRepository;
    private CrimeByDayOfMonthRepository crimeByDayOfMonthRepository;
    private CrimeHoodRepository crimeHoodRepository;


    @Override
    public List<String> getAllZipCodes() {
        return predictedCrimePropertyValuesRepository.getAllZipCodes();
    }

    @Override
    public List<IncidentsByWeek> getCrimesByWeek() {
        return crimeByDayOfMonthRepository.findAll();
    }

    @Override
    public List<PredictedCrimePropertyValues> getPredictedPropertyValues(String zipCode) {
        return adjustScale(predictedCrimePropertyValuesRepository.findAllByZipcode(zipCode));
    }

    @Override
    public List<PropertyCrimeValues> getPredictedPropertyAverageValues() {
        List<PropertyCrimeValues> averageForAllZipCodes = predictedCrimePropertyValuesRepository.findAverageForAllZipCodes();
        return adjustScaleRatio(averageForAllZipCodes);
    }

    @Override
    public List<CrimeHoods> getCrimeHoodsForYear(int year){
         return crimeHoodRepository.findAllByYear(year);
    }


    private List<PredictedCrimePropertyValues> adjustScale(List<PredictedCrimePropertyValues> data) {


        Optional<PredictedCrimePropertyValues> maxPrice = data.stream()
                .max(Comparator.comparing(PredictedCrimePropertyValues::getPrice));

        Optional<PredictedCrimePropertyValues> maxCrimeRate = data.stream()
                .max(Comparator.comparing(PredictedCrimePropertyValues::getTotalCrime));


        double mp = maxPrice.get().getPrice(); //max price
        double mc = maxCrimeRate.get().getTotalCrime(); //max crime
        double adj = mp / mc; // multiplier

        return data.stream()
                .peek(d -> d.setTotalCrime(Math.floor(d.getTotalCrime() * adj)))
                .collect(Collectors.toList());
    }

    private List<PropertyCrimeValues> adjustScaleRatio(List<PropertyCrimeValues> data) {
        Optional<PropertyCrimeValues> maxPrice = data.stream()
                .max(Comparator.comparing(PropertyCrimeValues::getPrice));

        Optional<PropertyCrimeValues> maxCrimeRate = data.stream()
                .max(Comparator.comparing(PropertyCrimeValues::getTotalCrime));

        double mp = maxPrice.get().getPrice(); //max price
        double mc = maxCrimeRate.get().getTotalCrime(); //max crime
        double adj = mp / mc; // multiplier

        List<PropertyCrimeValues> collect = data.stream()
                .map(d -> (PropertyCrimeValues) PropertyCrimeValuesImpl.builder()
                        .clusterDate(d.getClusterDate())
                        .price(d.getPrice())
                        .totalCrime(Math.floor(d.getTotalCrime() * adj))
                        .build()
                ).collect(Collectors.toList());


        return collect;
    }
}
