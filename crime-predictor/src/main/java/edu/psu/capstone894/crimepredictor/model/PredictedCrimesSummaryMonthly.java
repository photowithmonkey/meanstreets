package edu.psu.capstone894.crimepredictor.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * This class represents certain descriptors for PredictedCrimesSummary
 *
 * <p>
 * This is class is described by the @Entity, @Getter, and @NoArgsConstructor annotations. These annotations allows
 * boilerplate code to be generated through either persistence or the lombok library.
 * <p>
 * The @Entity annotation marks the POJO as a JPA entity. Allows class name to be used in queries
 * The @Getter annotation gets the lombok library to generate getter methods automatically.
 * The @NoArgsConstructor annotation will generate a constructor with no arguments.
 * </p>
 */
@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PredictedCrimesSummaryMonthly {

    /**
     * This variable is the primary key for attribute entities
     *
     * <p>
     * The @Id annotation specifies the primary key for this entity.
     * The @GeneratedValue annotation specifies how the primary keys are created.
     * </p>
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Integer year;
    private Integer month;
    private String crimeDescription;
    private Integer totalCrimes;
    private String city;

    /**
     * This constructor creates the object using the lombok @Builder annotation
     *
     * <p>
     * The @Builder annotation will automatically produce the code required to instantiate the object.
     * </p>
     *
     * @param date
     * @param lowerFelonyTotal
     * @param upperFelonyTotal
     * @param lowerMisdemeanorTotal
     * @param upperMisdemeanorTotal
     * @param city
     */
}
