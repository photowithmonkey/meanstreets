package edu.psu.capstone894.crimepredictor.services;

import edu.psu.capstone894.crimepredictor.api.MapDataService;
import edu.psu.capstone894.crimepredictor.model.types.GroupedLocations;
import edu.psu.capstone894.crimepredictor.model.types.IncidentsByLocation;
import edu.psu.capstone894.crimepredictor.model.KindsOfCrimeOverTime;
import edu.psu.capstone894.crimepredictor.model.PredictedCrimeTypeLocPredictResults;
import edu.psu.capstone894.crimepredictor.persistence.KindsOfCrimeOverTimeRepository;
import edu.psu.capstone894.crimepredictor.persistence.PredictedTypeLocResultsRepository;
import lombok.AllArgsConstructor;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class MapDataServiceImpl implements MapDataService {

    private PredictedTypeLocResultsRepository predictedTypeLocResultsRepository;
    private KindsOfCrimeOverTimeRepository kindsOfCrimeOverTimeRepository;

    @Override
    public List<PredictedCrimeTypeLocPredictResults> getPredictedTypeLocResults() {
        return predictedTypeLocResultsRepository.findAll();
    }

    @Override
    public List<GroupedLocations> getGroupedLocation(int level) {
        return predictedTypeLocResultsRepository.getGroupedLocations(level);
    }

    @Override
    public JSONObject getIncidentsByLocation(double latitude, double longitude, int level) {
        List<IncidentsByLocation> incidentsForLocation
                = predictedTypeLocResultsRepository.getIncidentsForLocation(latitude, longitude, level);

        JSONObject root = new JSONObject();
        JSONArray children = new JSONArray();
        root.put("name", "Incidents");
        root.put("children", children);


        String crime;
        String lastCrime = "";
        JSONArray children2 = null;
        for (IncidentsByLocation inc : incidentsForLocation) {
            crime = inc.getCrime_Description();
            if (!crime.equals(lastCrime)) {
                JSONObject child = new JSONObject();
                children2 = new JSONArray();
                child.put("name", crime);
                child.put("children", children2);
                children.add(child);
            }

            JSONObject c2 = new JSONObject();
            c2.put("name", inc.getCrime_Type());
            c2.put("incidents", inc.getIncidents());
            children2.add(c2);

            lastCrime = crime;
        }

        return root;
    }

    @Override
    public JSONArray getKindsOfCrimeOverTime(String type, Integer monthIdx) {
        if (type != null && !type.isEmpty()) {

            List<KindsOfCrimeOverTime> allByCrimeType = kindsOfCrimeOverTimeRepository
                    .findAllByCrimeTypeAndMonthIdx(type.toUpperCase(), monthIdx);

            JSONArray arr = new JSONArray();
            for (KindsOfCrimeOverTime item : allByCrimeType) {
                JSONArray e = new JSONArray();
                e.add(item.getLat());
                e.add(item.getLng());
                e.add(item.getQty());
                arr.add(e);
            }

            return arr;

        }

        return new JSONArray();
    }

}
