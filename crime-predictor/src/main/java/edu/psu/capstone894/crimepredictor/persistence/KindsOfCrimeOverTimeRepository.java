package edu.psu.capstone894.crimepredictor.persistence;

import edu.psu.capstone894.crimepredictor.model.KindsOfCrimeOverTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KindsOfCrimeOverTimeRepository
        extends JpaRepository<KindsOfCrimeOverTime, Integer> {

    List<KindsOfCrimeOverTime> findAllByCrimeTypeAndMonthIdx(String crimeType, Integer monthIdx);
}
