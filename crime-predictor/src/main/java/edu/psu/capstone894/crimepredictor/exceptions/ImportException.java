package edu.psu.capstone894.crimepredictor.exceptions;

public class ImportException extends Exception {

    public ImportException(String message){
       super(message);
    }
}
