package edu.psu.capstone894.crimepredictor.controllers;

import edu.psu.capstone894.crimepredictor.api.IncidentsService;
import edu.psu.capstone894.crimepredictor.model.Incident;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import static java.time.temporal.TemporalAdjusters.firstDayOfYear;
import static java.time.temporal.TemporalAdjusters.lastDayOfYear;

/**
 * This class is responsible for handlling requests for incidents
 *
 * <p>
 * This class handles the mappings to all incidents requests
 * by the users.
 * The @RestController signifies that this class is a request handler.
 * The @RestController annotation combines both @Controller and
 * the @ResponseBody annotations. So, while it does handle requests,
 * it can also respond with a response body, hence the @ResponseBody annotation.
 * Once the request is made the functions will return the objects to the site.
 * </p>
 */
@RestController
@RequestMapping("/api/incidents")
public class IncidentsController {

    /**
     * Variable is responsible for querying the incidents
     * repository.
     */
    private IncidentsService incidentsService;

    public IncidentsController(IncidentsService incidentsService) {
        this.incidentsService = incidentsService;
    }

    /**
     * This function is mapped to return all incidents in the repository with parameter constraints.
     *
     * <p>
     * This function is a GET mapping. It handles GET requests for incidents.
     * Any parameters passed in the request will put constraints on the types of incidents that will be returned.
     * The parameters that can restrict the incidents are listed below.
     * </p>
     *
     * @param 'city'        the city that the incident took place
     * @param 'weapon'      the weapon used in an incident
     * @param 'description' the description of the crime
     * @param 'dateFrom'    the date of the incident
     * @param 'dateTo'      the end date of the incident
     * @param 'pageable'    the pageable object that will hold records
     * @return the page for X number of incidents
     */
    @GetMapping
    public Page<Incident> getAllIncidents(@RequestParam(required = false) String dateFrom,
                                          @RequestParam(required = false) String dateTo,
                                          @RequestParam MultiValueMap<String, String> params,
                                          Pageable pageable) {

        LocalDate df = dateFrom != null ? LocalDate.parse(dateFrom, DateTimeFormatter.ISO_LOCAL_DATE)
                : LocalDate.now().with(firstDayOfYear());
        LocalDate dt = dateTo != null ? LocalDate.parse(dateTo, DateTimeFormatter.ISO_LOCAL_DATE)
                : LocalDate.now().with(lastDayOfYear());


        params.remove("dateFrom");
        params.remove("dateTo");
        params.remove("page");
        params.remove("size");

        return incidentsService.queryIncidents(params, df, dt, pageable);
    }



    /**
     * This function puts parameters into a map object
     *
     * <p>
     * This function will take in strings that are parameters and match them with
     * their key.
     * </p>
     *
     * @param key   the key that parameter strings are matched to
     * @param map   the map object that holds the key and their parameters
     * @param value the parameter strings
     */
    private static void addParameters(String key, Map<String, String[]> map, List<String> value) {
        if (value != null)
            map.put(key, value.toArray(new String[0]));
    }

}
