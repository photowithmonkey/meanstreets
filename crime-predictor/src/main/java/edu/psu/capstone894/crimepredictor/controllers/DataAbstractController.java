package edu.psu.capstone894.crimepredictor.controllers;

import edu.psu.capstone894.crimepredictor.api.DataAbstractServices;
import edu.psu.capstone894.crimepredictor.api.PredictedCrimesSummaryMonthlySrv;
import edu.psu.capstone894.crimepredictor.api.PredictedCrimesSummarySrv;
import edu.psu.capstone894.crimepredictor.model.CrimeHoods;
import edu.psu.capstone894.crimepredictor.model.IncidentsByWeek;
import edu.psu.capstone894.crimepredictor.model.PredictedCrimePropertyValues;
import edu.psu.capstone894.crimepredictor.model.PredictedCrimesSummary;
import edu.psu.capstone894.crimepredictor.model.types.PropertyCrimeValues;
import lombok.AllArgsConstructor;
import net.minidev.json.JSONArray;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/api/abstracts")
@AllArgsConstructor
public class DataAbstractController {

    private final DataAbstractServices dataAbstractServices;
    private PredictedCrimesSummarySrv predictedCrimesSummarySrv;
    private PredictedCrimesSummaryMonthlySrv predictedCrimesSummaryMonthlySrv;

    @RequestMapping("/annual")
    public JSONArray getPredictedCrimesSummary() {
        return predictedCrimesSummarySrv.getPredictedCrimesSummary();
    }

    @RequestMapping("/annual_by_type/{year}")
    public List<PredictedCrimesSummary> getPredictedCrimeTypesForYear(@PathVariable int year) {
        return predictedCrimesSummarySrv.getCrimeBreakdownForYear(year);
    }

    @RequestMapping("/monthly_by_type/{year}")
    public JSONArray getPredictedCrimesSummaryMonthly(@PathVariable int year) {
        return predictedCrimesSummaryMonthlySrv.getPredictedCrimesSummaryMonthly(year);
    }

    @RequestMapping("/weekly")
    public List<IncidentsByWeek> getCrimesByWeek() {
        return dataAbstractServices.getCrimesByWeek();
    }

    @RequestMapping("/properties")
    public List<PropertyCrimeValues> getPredictedPropertyAverageValues() {
        return dataAbstractServices.getPredictedPropertyAverageValues();
    }

    @RequestMapping("/properties/{zipCode}")
    public List<PredictedCrimePropertyValues> getPredictedPropertyValuesByZip(@PathVariable String zipCode) {
        return dataAbstractServices.getPredictedPropertyValues(zipCode);
    }

    @RequestMapping("/properties/zips")
    public List<String> getAllZipCodes() {
        return dataAbstractServices.getAllZipCodes();
    }

    @RequestMapping("/crimehood/{year}")
    public List<CrimeHoods> getCrimeByNeighborhood(@PathVariable int year) {
        return dataAbstractServices.getCrimeHoodsForYear(year);
    }
}
