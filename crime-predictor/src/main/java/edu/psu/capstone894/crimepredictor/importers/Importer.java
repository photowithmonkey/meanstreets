package edu.psu.capstone894.crimepredictor.importers;

import com.google.common.collect.HashMultimap;
import edu.psu.capstone894.crimepredictor.exceptions.ImportException;
import edu.psu.capstone894.crimepredictor.model.Attribute;
import edu.psu.capstone894.crimepredictor.model.Incident;
import edu.psu.capstone894.crimepredictor.persistence.AttributeRepository;
import edu.psu.capstone894.crimepredictor.persistence.IncidentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Properties;
import java.util.stream.Stream;

/**
 * This abstract class is responsible for giving importers baseline functions
 *
 * <p>
 *     This is the parent class of all importers. It introduces functions that all importers
 *     should use to import incident and attribute data.
 * </p>
 */
abstract class Importer {

    /**
     * Logger object for Importer class
     * @see Logger
     */
    private static Logger logger = LoggerFactory.getLogger(Importer.class);

    /**
     * Repository that holds the incidents
     * @see Incident
     * @see IncidentRepository
     */
    private IncidentRepository incidentRepository;

    /**
     * Repository that holds the incident attributes
     * @see Attribute
     * @see AttributeRepository
     */
    private AttributeRepository attributeRepository;

    /**
     * Map of the attributes pulled from incidents
     * @see HashMultimap
     */
    private HashMultimap<String, String> attributes;

    /**
     * The filename of the import file
     */
    private String imporFileName;

    /**
     * The name of the city
     */
    private String cityName;

    public Importer(IncidentRepository incidentRepository,
                    AttributeRepository attributeRepository,
                    String cityName,
                    String importFileName) {
        this.incidentRepository = incidentRepository;
        this.attributeRepository = attributeRepository;
        this.attributes = HashMultimap.create();
        this.attributes.put("city", cityName);
        this.imporFileName = importFileName;
        this.cityName = cityName;
    }

    /**
     * This function will parse the data to save the incident and attributes to their respective repositories.
     *
     * <p>
     *     This function is created to be passed on to the importer subclass. It is responsible for importing incidents
     *     line by line.
     * </p>
     *
     * @see IncidentRepository
     * @see Incident
     * @param line the line of data that is being imported
     * @param incidentRepository the repository that will save the line of data
     * @param attributes the map object of attributes
     * @return
     */
    abstract Incident parseIncident(String line,
                                    IncidentRepository incidentRepository,
                                    HashMultimap<String, String> attributes
    );

    /**
     * This function sets the path to the import file
     *
     * @see Properties
     * @see Path
     * @return number of lines imported from the import file
     */
    public long importData() {
        Properties properties = System.getProperties();
        String userDir = properties.getProperty("user.dir");
        Path path = Paths.get(userDir + "//" + imporFileName);
        long i = importData(path);

        if (i > 0) {
            SaveAttributes();
        }

        return i;
    }


    /**
     * This function handles parsing the file and importing data from the file
     *
     * <p>
     *     This function will take the path to our file and parse it. The subclasses will the parse the incident
     *     their way.
     * </p>
     * @param path path to the file being imported
     * @return number of lines of data imported
     */
    long importData(Path path) {
        try (Stream<String> stream = Files.lines(path)) {
            System.out.println(stream.toString());
            return stream.map((String line) -> parseIncident(line, incidentRepository, attributes))
                    .count();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return 0;
    }

    /**
     * Function will save the attributes to the attribute repository.
     *
     * @see Attribute
     * @see AttributeRepository
     */
    private void SaveAttributes() {
        //save to in existing attributes
        attributes.forEach((type, label) -> {
            attributeRepository.save(new Attribute(type, label, cityName));
        });
    }


    /**
     * This function will format the date strings into a LocalDate structure
     *
     * @see LocalDate
     * @param dateStr date string
     * @param timeStr time string
     * @return the date in a LocalDate structure
     * @throws ImportException
     */
    protected static LocalDate formatDate(String dateStr, String timeStr) throws ImportException {
        final String DATE_FORMAT = "M/d/yyyy";
        final String TIME_FORMAT = "H:mm:ss";

        if (dateStr == null || dateStr.isEmpty())
            throw new ImportException("Could not import row without a date");

        if (timeStr == null || timeStr.isEmpty()) {
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
            return LocalDate.parse(dateStr, dateFormatter);
        } else {
            try {
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_FORMAT + " " + TIME_FORMAT);
                return LocalDate.parse(dateStr + " " + timeStr, dateTimeFormatter);
            } catch (DateTimeParseException e) {
                logger.error("Could not parse date: " + dateStr + " " + timeStr);
            }
        }

        return null;
    }

    /**
     * Parse string and convert to Integer
     *
     * @see Integer
     * @param str the string that will be converted to an Integer
     * @return the converted Integer
     */
    protected static Integer formatInt(String str) {
        if (str != null && !str.isEmpty())
            try {
                return Integer.parseInt(str);
            } catch (Exception e) {
                logger.error("Could not parse integer with value of: " + str);
            }
        return null;
    }

    /**
     * Parse string and convert to Double
     *
     * @see Double
     * @param str the string that will be converted to a Double
     * @return the converted Double
     */
    protected static Double formatDouble(String str) {
        if (str != null && !str.isEmpty())
            return Double.parseDouble(str);

        return Double.NaN;
    }
}
