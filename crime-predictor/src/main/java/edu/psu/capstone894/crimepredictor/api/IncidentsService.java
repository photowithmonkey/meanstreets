package edu.psu.capstone894.crimepredictor.api;

import edu.psu.capstone894.crimepredictor.model.Incident;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import java.time.LocalDate;
import java.util.List;

public interface IncidentsService {

    Page<Incident> queryIncidents(MultiValueMap<String, String> params, LocalDate dateFrom, LocalDate dateTo, Pageable pageable);

    List<Incident> downloadIncidents(MultiValueMap<String, String> params, LocalDate df, LocalDate dt);
}
