package edu.psu.capstone894.crimepredictor.persistence;

import edu.psu.capstone894.crimepredictor.model.PredictedCrimesSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * This class is responsible for storing PredictedCrimesSummary objects
 *
 * <p>
 *     The @Repository is a Spring annotation the indicates that this class is a repository. It
 * </p>
 */
@Repository
public interface PredictedCrimesSummaryRepository
        extends JpaRepository<PredictedCrimesSummary, Long> {

        List<PredictedCrimesSummary>  findAllByYear(Integer year);
}
