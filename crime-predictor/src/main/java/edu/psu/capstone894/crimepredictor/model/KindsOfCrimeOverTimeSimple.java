package edu.psu.capstone894.crimepredictor.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor
public class KindsOfCrimeOverTimeSimple {
    Double lat;
    Double lng;
    Integer qty;
}
