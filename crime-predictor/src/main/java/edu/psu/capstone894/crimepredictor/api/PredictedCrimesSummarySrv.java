package edu.psu.capstone894.crimepredictor.api;

import edu.psu.capstone894.crimepredictor.model.PredictedCrimesSummary;
import net.minidev.json.JSONArray;

import java.util.List;

public interface PredictedCrimesSummarySrv {

    List<PredictedCrimesSummary> getCrimeBreakdownForYear(Integer year);

    JSONArray getPredictedCrimesSummary();
}
