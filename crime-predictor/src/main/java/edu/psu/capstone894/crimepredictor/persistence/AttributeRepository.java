package edu.psu.capstone894.crimepredictor.persistence;

import edu.psu.capstone894.crimepredictor.model.Attribute;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * This class is responsible for storing Attribute objects
 *
 * <p>
 * The @Repository is a Spring annotation the indicates that this class is a repository. It
 * </p>
 */
@Repository
public interface AttributeRepository extends JpaRepository<Attribute, Long> {

    /**
     * This function will find all attributes by a type
     *
     * @param type the descriptor the user is looking for
     * @param sort the sort options for the results
     * @return list of Attribute objects
     */
    List<Attribute> findAllByType(String type, Sort sort);

    /**
     * This function will find all Attributes by city
     *
     * @param city the city descriptor the user is looking for
     * @param by   the sort options for the results
     * @return list of Attribute objects
     */
    List<Attribute> findAllByCity(String city, Sort by);


    List<Attribute> findAllByCityAndType(String city, String type, Sort by);
}
