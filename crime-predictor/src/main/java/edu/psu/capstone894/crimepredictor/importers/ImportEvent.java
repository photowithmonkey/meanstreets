package edu.psu.capstone894.crimepredictor.importers;

import edu.psu.capstone894.crimepredictor.model.CrimeEvent;
import edu.psu.capstone894.crimepredictor.persistence.EventRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.stream.Stream;

public class ImportEvent {

    /**
     * Logger object for ImportEvent
     * @see org.slf4j.Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(ImportEvent.class);

    /**
     * The filename of the import file
     */
    private String importFilename = "data/EventsTest.txt";

    /**
     * Repository that holds the events
     */
    private EventRepository eventRepository;

    public ImportEvent(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public ImportEvent(EventRepository eventRepository,
                       String importFilename) {
        this.eventRepository = eventRepository;
        this.importFilename = importFilename;
    }

    public long importData() {
        Properties props = System.getProperties();
        String userDir = props.getProperty("user.dir");
        Path path = Paths.get(userDir+ "//" +importFilename);
        long i = importData(path);

        return i;
    }

    public long importData(Path path) {
        long retVal = 0;
        try(Stream<String> stream = Files.lines(path)) {
            retVal = stream.map((String line) ->
                    parseEvent(line, eventRepository))
                    .count();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return retVal;
    }

    /**
     * This function will parse the data to save the event to its repository.
     *
     * <p>
     *     This function parses a line of data from the import file.
     * </p>
     * @param line the line of data that is being imported
     * @param eventRepository the repository that will save the line of data
     * @return
     */
    public CrimeEvent parseEvent(String line, EventRepository eventRepository) {
        String[] values = line.split("\t");
        CrimeEvent event = null;
        CrimeEvent ev = null;
        try {
            event = CrimeEvent.builder()
                    .title(values[0])
                    .allDay(formatBoolean(values[1]))
                    .startYear(formatInt(values[2]))
                    .startMonth(formatInt(values[3]))
                    .startDay(formatInt(values[4]))
                    .startHour(formatInt(values[5]))
                    .startMinute(formatInt(values[6]))
                    .endYear(formatInt(values[7]))
                    .endMonth(formatInt(values[8]))
                    .endDay(formatInt(values[9]))
                    .endHour(formatInt(values[10]))
                    .endMinute(formatInt(values[11]))
                    .color(values[12])
                    .build();

            ev = eventRepository.save(event);
        } catch (Exception e) {
            logger.error("Failed to import row: " +line);
        }
        return event;
    }

    private Boolean formatBoolean(String str) {
        Boolean retVal = Boolean.FALSE;
        if (str != null && !str.isEmpty()) {

        } else {
            if(str.equalsIgnoreCase("true")) {
                retVal = Boolean.TRUE;
            }
        }
        return retVal;
    }

    /**
     * Parse string and convert to Integer
     *
     * @see Integer
     * @param str the string that will be converted to an Integer
     * @return the converted Integer
     */
    private Integer formatInt(String str) {
        Integer retVal = null;
        if (str != null && !str.isEmpty())
            try {
                retVal = Integer.parseInt(str);
            } catch (Exception e) {
                logger.error("Could not parse integer with value of: " + str);
            }
        return retVal;
    }
}
