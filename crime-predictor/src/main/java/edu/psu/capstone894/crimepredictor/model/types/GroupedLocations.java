package edu.psu.capstone894.crimepredictor.model.types;


public interface GroupedLocations {
    Double getLatitude();

    Double getLongitude();

    Integer getCnt();
}
