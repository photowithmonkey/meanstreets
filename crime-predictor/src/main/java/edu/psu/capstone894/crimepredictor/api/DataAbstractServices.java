package edu.psu.capstone894.crimepredictor.api;

import edu.psu.capstone894.crimepredictor.model.CrimeHoods;
import edu.psu.capstone894.crimepredictor.model.IncidentsByWeek;
import edu.psu.capstone894.crimepredictor.model.PredictedCrimePropertyValues;
import edu.psu.capstone894.crimepredictor.model.types.PropertyCrimeValues;

import java.util.List;

public interface DataAbstractServices {

    List<PredictedCrimePropertyValues> getPredictedPropertyValues(String zipCode);

    List<PropertyCrimeValues> getPredictedPropertyAverageValues();

    List<String> getAllZipCodes();

    List<IncidentsByWeek> getCrimesByWeek();

    List<CrimeHoods> getCrimeHoodsForYear(int year);
}
