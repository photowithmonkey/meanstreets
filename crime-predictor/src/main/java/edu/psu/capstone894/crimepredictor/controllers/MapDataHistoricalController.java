package edu.psu.capstone894.crimepredictor.controllers;

import edu.psu.capstone894.crimepredictor.model.HistoricalCrimeTypeLocSummary;
import edu.psu.capstone894.crimepredictor.services.MapDataHistoricalService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/map")
public class MapDataHistoricalController {

    private MapDataHistoricalService mapDataHistoricalService;

    public MapDataHistoricalController(MapDataHistoricalService mapDataHistoricalService) {
        this.mapDataHistoricalService = mapDataHistoricalService;
    }

//    @RequestMapping("/locationsHistorical")
//    public List<GroupedLocations> getGroupedLoc(@RequestParam int level) {
//        return mapDataHistoricalService.getGroupedLocation(level);
//    }

//    @RequestMapping("/incidentsHistorical")
//    public String getIncidentsByLoc(@RequestParam Double lat,
//                                    @RequestParam Double lng,
//                                    @RequestParam Integer level) {
//        return mapDataHistoricalService.getIncidentsByLocation(lat, lng, level).toJSONString();
//    }

    @RequestMapping("/typeLocResults")
    public List<HistoricalCrimeTypeLocSummary> getHistoricalTypeLocResultsData() {
        return mapDataHistoricalService.getHistoricalTypeLocSummary();
    }
}
