package edu.psu.capstone894.crimepredictor.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class KindsOfCrimeOverTimeResult {
    private List<KindsOfCrimeOverTimeSimple> crime;
    private Double centerLat;
    private Double centerLng;
    private Integer maxMonthIdx;
}
