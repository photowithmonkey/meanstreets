package edu.psu.capstone894.crimepredictor.services;

import edu.psu.capstone894.crimepredictor.api.IncidentsService;
import edu.psu.capstone894.crimepredictor.model.Incident;
import edu.psu.capstone894.crimepredictor.persistence.IncidentRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import java.time.LocalDate;
import java.util.List;

/**
 * The class acts as a service for retrieving Incident objects from the repository
 */
@Service
public class IncidentsServiceImpl implements IncidentsService {

    private IncidentRepository incidentRepository;

    public IncidentsServiceImpl(IncidentRepository incidentRepository){
        this.incidentRepository = incidentRepository;
    }

    /**
     * This function queries the repository using the parameters passed
     *
     * @param params a Map object holding the attribute-parameter pairing for the query
     * @param dateFrom beginning date
     * @param dateTo end date
     * @param pageable pagination object
     * @return a page of Incidents
     */
    @Override
    public Page<Incident> queryIncidents(MultiValueMap<String, String> params, LocalDate dateFrom, LocalDate dateTo, Pageable pageable){
        return incidentRepository.queryBuilderPageable(params,dateFrom,dateTo,pageable);
    }

    @Override
    public List<Incident> downloadIncidents(MultiValueMap<String, String> params, LocalDate dateFrom, LocalDate dateTo) {
        return incidentRepository.queryBuilder(params,dateFrom,dateTo);
    }
}
