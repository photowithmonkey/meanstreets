package edu.psu.capstone894.crimepredictor.model;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * This class represents certain descriptors for Incidents
 *
 * <p>
 * This is class is described by the @Entity, @Getter, @Setter, @NoArgsConstructor,
 * and @Table annotations. These annotations allows boilerplate code to be generated through either
 * persistence or the lombok library.
 * <p>
 * The @Entity annotation marks the POJO as a JPA enitity. Allows class name to be used in queries
 * The @Getter and @Setter annotations gets the lombok library to generate getter and setter methods automatically.
 * The @Table annotation specifies the primary table for this entity. The @Index annotation creates an index
 * for the table. It allows for better query and look-up performance for a set of columns in the table.
 * The @NoArgsConstructor annotation will generate a constructor with no arguments.
 * </p>
 */
@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(
        // Indexes improve query and look-up performance for a set of columns
        name = "f_crime_test",
        indexes = {
                @Index(name = "city_index", columnList = "city"),
                @Index(name = "weapon_index", columnList = "weapon"),
                @Index(name = "description_index", columnList = "ofns_desc"),
                @Index(name = "district_index", columnList = "boro_nm"),
                @Index(name = "offense_index", columnList = "law_cat_cd")
        }
)
public class Incident {

    /**
     * This variable is the primary key for attribute entities
     *
     * <p>
     * The @Id annotation specifies the primary key for this entity.
     * The @GeneratedValue annotation specifies how the primary keys are created.
     * </p>
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long incidentId;
    private long cmplntNum;
    private String city;
    @Column(name = "CmplntFrDt")
    private LocalDate crimeDate;            // BAL(CrimeDate & CrimeTime)   NYC(CMPLNT_FR_DT & CMPLNT_FR_TM)
    private LocalDate cmplntFrTm;

    @Column(name = "PD_CD")
    private String crimeCode;               // BAL(CrimeCode)               NYC(PC_CD)

    private String location;                // BAL(Location)                NYC(PATROL_BORO) minus "PATROL BORO"

    @Column(name = "OFNS_DESC")
    private String offense;             // BAL(Description)             NYC(PD_DESC)

    @Column(name = "PD_DESC")
    private String description;

    @Column(name = "LOC_OF_OCCUR_DESC")
    private String place;                    // BAL(Inside/Outside)          NYC(LOC_OF_OCCUR_DESC)
    private String weapon;                  // BAL(Weapon)
    private Integer post;                   // BAL(Post)

    private String district;                // BAL(District)                NYC(BORO_NM)

    @Column(name = "BORO_NM")
    private String boro;                // BAL(District)                NYC(BORO_NM)

    private String neighborhood;            // BAL(Neighborhood)            NYC(HADEVELOPT)
    private Double longitude;               // BAL(Longitude)               NYC(Longitude)
    private Double latitude;                // BAL(Latitude)                NYC(Latitude)

    @Column(name = "PREM_TYP_DESC")
    private String premises;                 // BAL(Premise)                 NYC(PREM_TYPE_DESC)

    @Column(name = "LAW_CAT_CD")
    private String category;

    @Column(name = "JURIS_DESC")
    private String jurisdiction;
    private Integer kyCd;

    /**
     * This constructor creates the object using the lombok @Builder annotation
     *
     * <p>
     * The @Builder annotation will automatically produce the code required to instantiate the object.
     * </p>
     *
     * @param cmplntNum
     * @param city
     * @param crimeDate
     * @param cmplntFrTm
     * @param crimeCode
     * @param location
     * @param description
     * @param description
     * @param place
     * @param weapon
     * @param post
     * @param district
     * @param neighborhood
     * @param longitude
     * @param latitude
     * @param premises
     * @param offense
     * @param jurisdiction
     * @param kyCd
     */
    @Builder
    public Incident(long cmplntNum, String city, LocalDate crimeDate, LocalDate cmplntFrTm,
                    String crimeCode, String location, String offense, String description,
                    String insideOutside, String weapon, Integer post, String district, String boro,
                    String neighborhood, Double longitude, Double latitude, String premises,
                    String jurisdiction, Integer kyCd) {
        this.cmplntNum = cmplntNum;
        this.city = city;
        this.crimeDate = crimeDate;
        this.cmplntFrTm = cmplntFrTm;
        this.crimeCode = crimeCode;
        this.location = location;
        this.offense = offense;
        this.description = description;
        this.place = insideOutside;
        this.weapon = weapon;
        this.post = post;
        this.district = district;
        this.boro = boro;
        this.neighborhood = neighborhood;
        this.longitude = longitude;
        this.latitude = latitude;
        this.premises = premises;
        this.jurisdiction = jurisdiction;
        this.kyCd = kyCd;
    }

    public static String[] getHeaders() {

        List<String> list = new ArrayList<>();
        for (Field declaredField : Incident.class.getDeclaredFields()) {
            list.add(declaredField.getName());
        }

        return list.toArray(new String[0]);
    }
}
