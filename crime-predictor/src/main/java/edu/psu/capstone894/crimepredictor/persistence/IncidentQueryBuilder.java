package edu.psu.capstone894.crimepredictor.persistence;

import edu.psu.capstone894.crimepredictor.model.Incident;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import java.time.LocalDate;
import java.util.List;

/**
 * This is an interface for a Incident Query class
 */
public interface IncidentQueryBuilder {

    /**
     * This function will build a query based on the parameters passed
     *
     * @param params a Map object holding the attribute-parameter pairing for the query
     * @param dateFrom beginning date
     * @param dateTo end date
     * @param pageable pagination object
     * @return a page of Incidents
     */
    Page<Incident> queryBuilderPageable(MultiValueMap<String, String> params, LocalDate dateFrom, LocalDate dateTo, Pageable pageable);

    List<Incident> queryBuilder(MultiValueMap<String, String> params, LocalDate dateFrom, LocalDate dateTo);
}

