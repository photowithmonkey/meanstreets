package edu.psu.capstone894.crimepredictor.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PredictedCrimePropertyValues {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idx;

    private LocalDate clusterDate;
    private String zipcode;
    private Double totalCrime;
    private Double price;

}
