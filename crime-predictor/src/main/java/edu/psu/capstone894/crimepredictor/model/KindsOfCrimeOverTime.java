package edu.psu.capstone894.crimepredictor.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class KindsOfCrimeOverTime {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idx;
    private Integer monthIdx;

    private Integer year;
    private Integer month;
    private Double lat;
    private Double lng;

    private String crimeType;
    private Integer qty;

}

