package edu.psu.capstone894.crimepredictor.persistence;

import edu.psu.capstone894.crimepredictor.model.CrimeHoods;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CrimeHoodRepository extends JpaRepository<CrimeHoods, Integer> {

    List<CrimeHoods> findAllByYear(int year);
}
