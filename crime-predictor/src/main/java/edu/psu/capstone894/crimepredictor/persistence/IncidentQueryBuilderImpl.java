package edu.psu.capstone894.crimepredictor.persistence;

import edu.psu.capstone894.crimepredictor.model.Incident;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.MultiValueMap;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * This function is a implementation of the Incident Query Builder Interface
 *
 * <p>
 * The @Repository annotation specifies that this class stores objects.
 * The @Qualifier annotation specifies which bean needs to be injected in specific instances. Spring will pass an
 * exception when there are multiple beans of the same type. The @Qualifier annotation mitigate that issue.
 * </p>
 */
@Repository
@Qualifier("IncidentQueryBuilderImpl")
public class IncidentQueryBuilderImpl implements IncidentQueryBuilder {

    private final EntityManager em;

    public IncidentQueryBuilderImpl(EntityManager em) {
        this.em = em;
    }


/*
    private long buildCountQuery(MultiValueMap<String, String> params, LocalDate dateFrom, LocalDate dateTo) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Incident> incident = cq.from(Incident.class);
        cq.select(cb.count(incident));
        Predicate predicate = buildPredicate(params, dateFrom, dateTo, cb, incident);
        cq.where(predicate);
        TypedQuery<Long> query = em.createQuery(cq);
        Long singleResult = query.getSingleResult();
        return singleResult;
    }
*/

    /**
     * This function will build a query based on the parameters passed
     *
     * @param params   a Map object holding the attribute-parameter pairing for the query
     * @param dateFrom beginning date
     * @param dateTo   end date
     * @param pageable pagination object
     * @return
     */

    @Override
    public Page<Incident> queryBuilderPageable(MultiValueMap<String, String> params, LocalDate dateFrom,
                                               LocalDate dateTo, Pageable pageable) {

        List<Incident> resultList;
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Incident> cq = cb.createQuery(Incident.class);
        cq.where(buildPredicate(params, dateFrom, dateTo, cb, cq.from(Incident.class)));
        TypedQuery<Incident> query = em.createQuery(cq);

        //this clause is for unit tests only, because at runtime we don't know what total number of records will be
        if (pageable == null) {
            resultList = query.getResultList();
            Pageable p = PageRequest.of(0, 20);
            return new PageImpl<>(resultList.subList(0, 20), p, resultList.size());
        }

        resultList = query.setMaxResults(pageable.getPageSize())
                .setFirstResult(new Long(pageable.getOffset()).intValue())
                .getResultList();
        return new PageImpl<>(resultList, pageable, resultList.size());
    }


    @Override
    public List<Incident> queryBuilder(MultiValueMap<String, String> params, LocalDate dateFrom, LocalDate dateTo) {

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Incident> cq = cb.createQuery(Incident.class);
        cq.where(buildPredicate(params, dateFrom, dateTo, cb, cq.from(Incident.class)));
        TypedQuery<Incident> query = em.createQuery(cq);

        return query.getResultList();
    }

    private Predicate buildPredicate(MultiValueMap<String, String> params, LocalDate dateFrom,
                                     LocalDate dateTo, CriteriaBuilder cb,
                                     Root<Incident> incident) {
        List<Predicate> predicates = new ArrayList<>();
        if (params.size() > 0) {
            for (String key : params.keySet()) {
                List<String> value = params.get(key);
                if (value != null && value.size() > 0) {
                    if (value.size() > 1) {
                        Predicate[] clause = new Predicate[value.size()];
                        for (int i = 0; i < value.size(); i++) {
                            Predicate p = cb.equal(incident.get(key), value.get(i));
                            clause[i] = p;
                        }
                        Predicate clausePredicate = cb.or(clause);
                        predicates.add(clausePredicate);
                    } else {
                        Predicate p = cb.equal(incident.get(key), value.get(0));
                        predicates.add(p);
                    }
                }
            }
        }

        // set date parameters, if none provided in query default to current year
        Predicate td = cb.lessThanOrEqualTo(incident.get("crimeDate"), dateTo);
        Predicate fd = cb.greaterThanOrEqualTo(incident.get("crimeDate"), dateFrom);
        predicates.add(cb.and(fd, td));

        return cb.and(predicates.toArray(new Predicate[0]));
    }
}
