package edu.psu.capstone894.crimepredictor.services;

import edu.psu.capstone894.crimepredictor.api.PredictedCrimesSummaryMonthlySrv;
import edu.psu.capstone894.crimepredictor.model.PredictedCrimesSummaryMonthly;
import edu.psu.capstone894.crimepredictor.persistence.PredictedCrimesSummaryMonthlyRepo;
import lombok.AllArgsConstructor;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@AllArgsConstructor
public class PredictedCrimesSummaryMonthlyImpl implements PredictedCrimesSummaryMonthlySrv {

    private PredictedCrimesSummaryMonthlyRepo monthlyRepo;

    @Override
    public JSONArray getPredictedCrimesSummaryMonthly(int year) {
        List<PredictedCrimesSummaryMonthly> data = monthlyRepo.findAllByYear(year);

        SortedMap<Integer, HashMap<String, Integer>> pivoted = new TreeMap<>();
        data.stream()
                .sorted(Comparator.comparing(PredictedCrimesSummaryMonthly::getMonth))
                .forEach((record) -> {
                    if (!pivoted.containsKey(record.getMonth()))
                        pivoted.put(record.getMonth(), new HashMap<>());

                    Map<String, Integer> map = pivoted.get(record.getMonth());
                    map.put(record.getCrimeDescription(), record.getTotalCrimes());
                });

        JSONArray result = new JSONArray();
        pivoted.forEach((key, map) -> {
            JSONObject obj = new JSONObject();
            obj.put("month", key);
            map.forEach((k, v) -> {
                obj.put(k, v);
            });
            result.add(obj);
        });

        return result;

    }
}
