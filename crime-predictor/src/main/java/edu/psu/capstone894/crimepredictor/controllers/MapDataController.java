package edu.psu.capstone894.crimepredictor.controllers;

import edu.psu.capstone894.crimepredictor.api.MapDataService;
import edu.psu.capstone894.crimepredictor.model.types.GroupedLocations;
import edu.psu.capstone894.crimepredictor.model.PredictedCrimeTypeLocPredictResults;
import net.minidev.json.JSONArray;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/map")
public class MapDataController {

    private MapDataService mapDataService;

    public MapDataController(MapDataService mapDataService) {
        this.mapDataService = mapDataService;
    }

    @RequestMapping("/locations")
    public List<GroupedLocations> getGroupedLoc(@RequestParam int level) {
        return mapDataService.getGroupedLocation(level);
    }

    @RequestMapping("/incidents")
    public String getIncidentsByLoc(@RequestParam Double lat,
                                    @RequestParam Double lng,
                                    @RequestParam Integer level) {
        return mapDataService.getIncidentsByLocation(lat, lng, level).toJSONString();
    }

    @RequestMapping("/typeLoc")
    public List<PredictedCrimeTypeLocPredictResults> getPredictedTypeLocResultsData() {
        return mapDataService.getPredictedTypeLocResults();
    }

    @RequestMapping("/timeline/{type}/{monthIdx}")
    public JSONArray getKindsOfCrimeOverTime(@PathVariable String type,
                                             @PathVariable Integer monthIdx) {
        return mapDataService.getKindsOfCrimeOverTime(type, monthIdx);
    }


}
