package edu.psu.capstone894.crimepredictor.model;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Getter
@Setter
@Entity
@NoArgsConstructor
public class IncidentsByWeek {
    @Id
    private Integer id;
    private Integer week;
    private Integer day;
    private String dayName;
    private Integer countIncidents;

    @Builder
    public IncidentsByWeek( Integer week, Integer day, String dayName, Integer countIncidents) {
        this.week = week;
        this.day = day;
        this.dayName = dayName;
        this.countIncidents = countIncidents;
    }
}
