package edu.psu.capstone894.crimepredictor.persistence;

import edu.psu.capstone894.crimepredictor.model.HistoricalCrimeTypeLocSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * This class is responsible for storing PredictedCrimeTypeLocPredictResults objects
 *
 * <p>
 * The @Repository is a Spring annotation the indicates that this class is a repository. It
 * </p>
 */
@Repository
public interface HistoricalTypeLocSummaryRepository
        extends JpaRepository<HistoricalCrimeTypeLocSummary, Long> {

    String query = "select g1.latitude, g1.longitude, count(*) cnt " +
            "from (select round(latitude, ?1) latitude, round(longitude, ?1) longitude " +
            "from historical_crime_type_loc_summary) g1 " +
            "group by g1.latitude, g1.longitude";

    // list of locations grouped by rounding for which data is available
//    @Query(value = query, nativeQuery = true)
//    List<GroupedLocations> getGroupedLocations(int level);

    String query2 = "select crime_type, count(*) incidents " +
            "from historical_crime_type_loc_summary " +
            "where round(latitude, ?3) = ?1 and round(longitude, ?3) = ?2 " +
            "group by crime_type";

    // list of crime types for a given location
//    @Query(value = query2, nativeQuery = true)
//    List<IncidentsByLocation> getIncidentsForLocation(double latitude, double longitude, int level);
}

