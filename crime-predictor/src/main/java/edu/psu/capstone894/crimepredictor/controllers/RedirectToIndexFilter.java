package edu.psu.capstone894.crimepredictor.controllers;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * This class is responsible for redirecting users back to homepage of site
 */
@Component
public class RedirectToIndexFilter implements Filter  {

    // TODO: Write the documentation for RedirectToIndexFilter
    @Override
    public void doFilter(ServletRequest request,
                         ServletResponse response,
                         FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest req = ((HttpServletRequest) request);
        String requestUri = req.getRequestURI();

        if (requestUri.startsWith("/api") || requestUri.startsWith("/browser")
                || requestUri.startsWith("/static")
        ) {
            filterChain.doFilter(request, response);
            return;
        }

        request.getRequestDispatcher("/").forward(request, response);
    }
}
