package edu.psu.capstone894.crimepredictor.services;

import edu.psu.capstone894.crimepredictor.api.AttributesService;
import edu.psu.capstone894.crimepredictor.model.Attribute;
import edu.psu.capstone894.crimepredictor.model.types.MaxMinDate;
import edu.psu.capstone894.crimepredictor.persistence.AttributeRepository;
import edu.psu.capstone894.crimepredictor.persistence.IncidentRepository;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * The class acts as a service for retrieving Attribute objects from the repository
 */
@Service
public class AttributesServiceImpl implements AttributesService {

    private AttributeRepository attributeRepository;
    private IncidentRepository incidentRepository;

    public AttributesServiceImpl(AttributeRepository attributeRepository, IncidentRepository incidentRepository) {
        this.attributeRepository = attributeRepository;
        this.incidentRepository = incidentRepository;
    }

    /**
     * Retrieves all Attributes from the Attribute Repository
     *
     * @return all Attributes in the form of a JSON Object
     */
    @Override
    public JSONObject getAllAttributes() {
        Sort sort = Sort.by(Sort.DEFAULT_DIRECTION, "type", "label");
        List<Attribute> all = this.attributeRepository.findAll(sort);
        return createAttributeJson(all);
    }

    /**
     * Retrieves Attributes by the type passed as a parameter
     *
     * @param type the Attribute descriptor the user is looking for
     * @return list of Attribute objects
     */
    @Override
    public List<Attribute> findAttributeByType(String type) {
        return attributeRepository.findAllByType(type, Sort.by("label"));
    }

    /**
     * Retrieves Attributes by the specified city
     *
     * @param city the city descriptor the user is looking for
     * @return all Attributes that meet the criteria in the form of a JSON Object
     */
    @Override
    public JSONObject findAttributesByCity(String city) {
        List<Attribute> list = attributeRepository.findAllByCity(city, Sort.by("type", "label"));
        JSONObject result = new JSONObject();
        JSONObject jo = createAttributeJson(list);

        //we don't want the city attribute, since its implied
        jo.remove("city");
        result.put("attributes", jo);

        MaxMinDate maxAndMinDates = incidentRepository.findMaxAndMinDates(city);
        result.put("mindate", maxAndMinDates.getMinDate());
        result.put("maxdate", maxAndMinDates.getMaxDate());

        return result;
    }


    private JSONObject createAttributeJson(List<Attribute> all) {
        String type;
        String lastType = null;
        JSONObject json = new JSONObject();
        JSONArray ja = null;
        for (Attribute attribute : all) {
            type = attribute.getType();
            if (!type.equals(lastType)) {
                ja = new JSONArray();
                json.put(type, ja);
            }
            ja.add(attribute.getLabel());
            lastType = type;
        }
        return json;
    }
}
