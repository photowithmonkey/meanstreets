package edu.psu.capstone894.crimepredictor.model.types;

import java.time.LocalDate;

public interface MaxMinDate {
    LocalDate getMaxDate();

    LocalDate getMinDate();
}
