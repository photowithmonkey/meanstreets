package edu.psu.capstone894.crimepredictor.persistence;

import edu.psu.capstone894.crimepredictor.model.Incident;
import edu.psu.capstone894.crimepredictor.model.types.MaxMinDate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * This class is responsible for storing Incident objects
 *
 * <p>
 *     The @Repository is a Spring annotation the indicates that this class is a repository. It
 * </p>
 */
@Repository
public interface IncidentRepository extends JpaRepository<Incident, Long>, IncidentQueryBuilder {

    /**
     * This function executes the query based on the dates and city
     *
     * @param city the location the Incident took place
     * @return
     */
    @Query("SELECT MIN(crimeDate) AS minDate, MAX(crimeDate) AS maxDate FROM Incident where city = ?1 ")
    MaxMinDate findMaxAndMinDates(String city);

    List<Incident> findAllByCity(String city);
}
