package edu.psu.capstone894.crimepredictor.services;

//import edu.psu.capstone894.crimepredictor.model.PredictedCrimeTypeLocPredictResults;
//import edu.psu.capstone894.crimepredictor.persistence.PredictedTypeLocResultsRepository;

import edu.psu.capstone894.crimepredictor.model.HistoricalCrimeTypeLocSummary;
import edu.psu.capstone894.crimepredictor.persistence.HistoricalTypeLocSummaryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MapDataHistoricalService {

    private HistoricalTypeLocSummaryRepository historicalTypeLocSummaryRepository;

    public MapDataHistoricalService(HistoricalTypeLocSummaryRepository historicalTypeLocSummaryRepository) {
        this.historicalTypeLocSummaryRepository = historicalTypeLocSummaryRepository;
    }

    public List<HistoricalCrimeTypeLocSummary> getHistoricalTypeLocSummary() {
        return historicalTypeLocSummaryRepository.findAll();
    }

/*
    public List<GroupedLocations> getGroupedLocation(int level) {
        return historicalTypeLocSummaryRepository.getGroupedLocations(level);
    }

    public JSONObject getIncidentsByLocation(double latitude, double longitude, int level) {
        List<IncidentsByLocation> incidentsForLocation
                = historicalTypeLocSummaryRepository.getIncidentsForLocation(latitude, longitude, level);

        JSONObject root = new JSONObject();
        JSONArray children = new JSONArray();
        root.put("name", "Incidents");
        root.put("children", children);


        String crime;
        String lastCrime = "";
        JSONArray children2 = null;
        for (IncidentsByLocation inc : incidentsForLocation) {
            crime = inc.getCrime_Description();
            if (!crime.equals(lastCrime)) {
                JSONObject child = new JSONObject();
                children2 = new JSONArray();
                child.put("name", crime);
                child.put("children", children2);
                children.add(child);
            }

            JSONObject c2 = new JSONObject();
            c2.put("name", inc.getCrime_Type());
            c2.put("incidents", inc.getIncidents());
            children2.add(c2);

            lastCrime = crime;
        }

        return root;
    }
*/
}
