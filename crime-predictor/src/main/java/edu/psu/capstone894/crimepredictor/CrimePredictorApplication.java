package edu.psu.capstone894.crimepredictor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrimePredictorApplication {

    public static void main(String[] args) {

        try{
            SpringApplication.run(CrimePredictorApplication.class, args);
        }catch(Exception e){
            System.out.println("Exception running the CrimePredictorApplication " + e);
        }
    }

}
