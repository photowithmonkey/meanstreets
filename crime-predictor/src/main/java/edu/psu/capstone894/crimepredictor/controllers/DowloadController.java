package edu.psu.capstone894.crimepredictor.controllers;

import edu.psu.capstone894.crimepredictor.api.IncidentsService;
import edu.psu.capstone894.crimepredictor.model.Incident;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static java.time.temporal.TemporalAdjusters.firstDayOfYear;
import static java.time.temporal.TemporalAdjusters.lastDayOfYear;

@Controller
@RequestMapping("/api/download")
public class DowloadController {

    private IncidentsService incidentsService;

    public DowloadController(IncidentsService incidentsService){
        this.incidentsService = incidentsService;
    }


    @RequestMapping("/incidents")
    public void downloadAllIncidents(@RequestParam(required = false) String dateFrom,
                                     @RequestParam(required = false) String dateTo,
                                     @RequestParam MultiValueMap<String, String> params,
                                     HttpServletResponse response) throws IOException {

        LocalDate df = dateFrom != null ? LocalDate.parse(dateFrom, DateTimeFormatter.ISO_LOCAL_DATE)
                : LocalDate.now().with(firstDayOfYear());
        LocalDate dt = dateTo != null ? LocalDate.parse(dateTo, DateTimeFormatter.ISO_LOCAL_DATE)
                : LocalDate.now().with(lastDayOfYear());


        params.remove("dateFrom");
        params.remove("dateTo");
        params.remove("page");
        params.remove("size");

        List<Incident> incidents = incidentsService.downloadIncidents(params, df, dt);

        String csvFileName = "incidents.csv";
        response.setContentType("text/csv");

        // creates mock data
        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"",
                csvFileName);
        response.setHeader(headerKey, headerValue);


        // uses the Super CSV API to generate CSV data from the model data
        ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(),
                CsvPreference.STANDARD_PREFERENCE);

        String[] header = Incident.getHeaders();

        csvWriter.writeHeader(header);

        for (Incident incident : incidents) {
            csvWriter.write(incident, header);
        }

        csvWriter.close();
    }


}
