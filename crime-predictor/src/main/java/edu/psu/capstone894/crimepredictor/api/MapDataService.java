package edu.psu.capstone894.crimepredictor.api;

import edu.psu.capstone894.crimepredictor.model.types.GroupedLocations;
import edu.psu.capstone894.crimepredictor.model.PredictedCrimeTypeLocPredictResults;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

import java.util.List;

public interface MapDataService {
    List<PredictedCrimeTypeLocPredictResults> getPredictedTypeLocResults();

    List<GroupedLocations> getGroupedLocation(int level);

    JSONObject getIncidentsByLocation(double latitude, double longitude, int level);

    JSONArray getKindsOfCrimeOverTime(String type, Integer monthIdx);
}
