package edu.psu.capstone894.crimepredictor.controllers;

import edu.psu.capstone894.crimepredictor.api.EventsService;
import edu.psu.capstone894.crimepredictor.model.CrimeEvent;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * This class is responsible for handling requests for events
 *
 * <p>
 * This class handles the mappings to all event requests by the user.
 * The @RestController signifies that this class is a request handler.
 * The @RestController annotation combines both @Controller and @ResponseBody
 * annotations. So, while it does handle requests, it can also respond with a
 * response body, hence the @ResponseBody annotation. Once the request is made the
 * functions will return the objects to the site.
 * </p>
 */
@RestController
@RequestMapping("/api/events")
public class EventsController {

    /**
     * Variable is responsible for querying the events repository
     */
    private EventsService eventsService;

    public EventsController(EventsService eventsService) {
        this.eventsService = eventsService;
    }

    /**
     * This function returns all events in the event repository
     *
     * <p>
     * This is the function that will respond to GET requests from the user.
     * It will return all events from the event repository.
     * The function will return the events in the form of a JSON object.
     * </p>
     *
     * @return a JSON object holding all events.
     */
    @GetMapping
    public List<CrimeEvent> getAllEvents() {
        return eventsService.getAllEvents();
    }

    /**
     * This function sends an Event object to the event service to post
     *
     * <p>
     * This is the function that will respond to POST requests from the user.
     * It will send the event to the event repository.
     * </p>
     *
     * @param event the Event object that will be posted to the database
     * @return Event object information
     */
    @PostMapping
    public CrimeEvent createEvent(@RequestBody CrimeEvent event) {
        return eventsService.newEvent(event);
    }

}
