package edu.psu.capstone894.crimepredictor.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * This class represents certain descriptors for Attributes (type, label, and city)
 *
 * <p>
 *     This is class is described by the @Entity, @Getter, @Setter, and @Table annotations. These annotations allows
 *     boilerplate code to be generated through either persistence or the lombok library.
 *
 *     The @Entity annotation marks the POJO as a JPA enitity. Allows class name to be used in queries
 *     The @Getter and @Setter annotations gets the lombok library to generate getter and setter methods automatically.
 *     The @Table annotation specifies the primary table for this entity. The @Index annotation creates an index
 *     for the table. It allows for better query and look-up performance for a set of columns in the table.
 * </p>
 */
@Entity
@Table(
        indexes = {
                @Index(name = "attribute_type", columnList = "type"),
                @Index(name = "attribute_city", columnList = "city"),
        }
)
@Getter
@Setter
@NoArgsConstructor
public class Attribute {

    /**
     * This variable is the primary key for attribute entities
     *
     * <p>
     *     The @Id annotation specifies the primary key for this entity.
     *     The @GeneratedValue annotation specifies how the primary keys are created.
     * </p>
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long attributeId;

    private String type;
    private String label;
    private String city;

    public Attribute(String type, String label, String city) {
        this.type = type;
        this.label = label;
        this.city = city;
    }

}
