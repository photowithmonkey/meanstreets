package edu.psu.capstone894.crimepredictor.model;

import edu.psu.capstone894.crimepredictor.model.types.PropertyCrimeValues;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class PropertyCrimeValuesImpl implements PropertyCrimeValues {
    private LocalDate clusterDate;
    private Double totalCrime;
    private Double price;
}
