package edu.psu.capstone894.crimepredictor.model.types;

public interface IncidentsByLocation {
    String getCrime_Type();

    String getCrime_Description();

    Integer getIncidents();
}
