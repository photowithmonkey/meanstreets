package edu.psu.capstone894.crimepredictor.controllers;

import edu.psu.capstone894.crimepredictor.model.Attribute;
import edu.psu.capstone894.crimepredictor.api.AttributesService;
import net.minidev.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * This class is responsible for handling requests for attributes
 *
 * <p>
 *     This class handles the mappings to all attribute requests by the user.
 *     The @RestController signifies that this class is a request handler.
 *     The @Restcontroller annotation combines both @Controller and @ResponseBody
 *     annotations. So, while it does handle requests, it can also respond with a
 *     response body, hence the @ResponseBody annotation. Once the request is made the
 *     functions will return the objects to the site.
 * </p>
 */
@RestController
@RequestMapping("/api/attributes")
public class AttributeController {

    /**
     * Variable is responsible for querying the attributes repository.
     */
    private AttributesService attributesService;

    public AttributeController(AttributesService attributesService) {
        this.attributesService = attributesService;
    }

    /**
     * This function returns all attributes in the attribute repository
     *
     * <p>
     *     This is function that will respond to GET requests from the user.
     *     It will return all attributes from the attribute repository.
     *     The function will return the attributes in the form of a JSON object.
     * </p>
     * @return a JSON object holding all attributes. This is the response
     */
    @GetMapping
    public JSONObject getAllAttributes() {
        return attributesService.getAllAttributes();
    }

    /**
     * This function will return all attributes that match the type in the request
     *
     * <p>
     *     This is a GET mapping that handles GET requests from the user. The user passes
     *     a type of attribute in the request and the attribute service will query the
     *     attribute repository based on the type passed.
     * </p>
     * @param type the attribute type that the user is looking for
     * @return list of attributes that match the type specified by the type parameter
     */
    @GetMapping("/type/{type}")
    public List<Attribute> findAttributesByType(@PathVariable("type") String type) {
        return attributesService.findAttributeByType(type);
    }

    @GetMapping("/city/{city}")
    public JSONObject findAttributesByCity(@PathVariable("city") String city) {
        return attributesService.findAttributesByCity(city);
    }

}
