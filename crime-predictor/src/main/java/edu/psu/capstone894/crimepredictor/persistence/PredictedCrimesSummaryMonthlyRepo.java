package edu.psu.capstone894.crimepredictor.persistence;


import edu.psu.capstone894.crimepredictor.model.PredictedCrimesSummaryMonthly;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * This class is responsible for storing CrimeDesGrowth objects
 *
 * <p>
 * The @Repository is a Spring annotation the indicates that this class is a repository. It
 * </p>
 */

@Repository
public interface PredictedCrimesSummaryMonthlyRepo extends JpaRepository<PredictedCrimesSummaryMonthly, Long> {
    List<PredictedCrimesSummaryMonthly> findAllByYear(Integer year);
}
