package edu.psu.capstone894.crimepredictor.importers;

import com.google.common.collect.HashMultimap;
import edu.psu.capstone894.crimepredictor.model.Incident;
import edu.psu.capstone894.crimepredictor.persistence.AttributeRepository;
import edu.psu.capstone894.crimepredictor.persistence.IncidentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class imports Baltimore Open Data.
 *
 * <p>
 *     This class is responsible for importing Baltimore data from the Open Data site. It is a subclass of
 *     the Importer class
 * </p>
 * @see Importer
 */
public class ImportOpenBaltimore extends Importer {

    /**
     * Logger object for ImportOpenBaltimore
     * @see Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(ImportOpenBaltimore.class);

    /**
     * The filename of the import file
     */
    private final static String fileName = "data/BPD_Part_1_Victim_Based_Crime_Data.txt";

    public ImportOpenBaltimore(IncidentRepository incidentRepository, AttributeRepository attributeRepository) {
        super(incidentRepository, attributeRepository,"Baltimore", fileName);
    }

    /**
     * This function will parse the data to save the incident and attributes to their respective repositories.
     *
     * <p>
     *      This function parses a line of data from the import file.
     * </p>
     * @see Incident
     * @see IncidentRepository
     * @param line the line of data that is being imported
     * @param incidentRepository the repository that will save the line of data
     * @param attributes the map object of attributes
     * @return
     */
    protected Incident parseIncident(String line,
                                     IncidentRepository incidentRepository,
                                     HashMultimap<String, String> attributes) {

        String[] values = line.split("\t");
        Incident incident;
        try {
            incident = Incident.builder()
                    .crimeDate(formatDate(values[0], values[1]))
                    .crimeCode(values[2])
                    .location(values[3])
                    .description(values[4])
                    .insideOutside(values[5])
                    .weapon(values[6])
                    .post(formatInt(values[7]))
                    .district((values[8]))
                    .neighborhood(values[9])
                    .longitude(formatDouble(values[10]))
                    .latitude(formatDouble(values[11]))
                    .premises(values[13])
                    .city("Baltimore")
                    .build();

            incidentRepository.save(incident);

            attributes.put("weapon", values[6]);
            attributes.put("description", values[4]);
            attributes.put("district", values[8]);
            attributes.put("neighborhood", values[9]);

            return incident;
        } catch (Exception e) {
            logger.error("Failed to import row:" + line);
            return null;
        }
    }

}
