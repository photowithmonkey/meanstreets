package edu.psu.capstone894.crimepredictor.services;

import edu.psu.capstone894.crimepredictor.api.EventsService;
import edu.psu.capstone894.crimepredictor.model.CrimeEvent;
import edu.psu.capstone894.crimepredictor.persistence.EventRepository;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * The class acts as a service for retrieving Event objects from the repository
 */
@Service
public class EventsServiceImpl implements EventsService {

    private EventRepository eventRepository;

    public EventsServiceImpl(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    /**
     * Retrieves all Events from the Event Repository
     *
     * @return all Events in the form of a JSON Object
     */
    public List<CrimeEvent> getAllEvents() {
        Sort sort = Sort.by(Sort.DEFAULT_DIRECTION, "title");
        return this.eventRepository.findAll(sort);
    }

    /**
     * Posts Event object to Event repository
     *
     * @param newEvent object that will be posted
     * @return Event object information
     */
    public CrimeEvent newEvent(CrimeEvent newEvent) {
        return eventRepository.save(newEvent);
    }
}
