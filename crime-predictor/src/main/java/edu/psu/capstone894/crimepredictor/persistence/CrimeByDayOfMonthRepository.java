package edu.psu.capstone894.crimepredictor.persistence;

import edu.psu.capstone894.crimepredictor.model.IncidentsByWeek;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CrimeByDayOfMonthRepository extends JpaRepository<IncidentsByWeek, Long> {
}
