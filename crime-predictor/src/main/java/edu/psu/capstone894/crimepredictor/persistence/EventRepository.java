package edu.psu.capstone894.crimepredictor.persistence;

import edu.psu.capstone894.crimepredictor.model.CrimeEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * This class is responsible for storing Event objects
 *
 * <p>
 *     The @Repository is a Spring annotation that indicates that this class is a repository.
 * </p>
 */
@Repository
public interface EventRepository extends JpaRepository<CrimeEvent, Long> {

}
