package edu.psu.capstone894.crimepredictor.persistence;

import edu.psu.capstone894.crimepredictor.model.PredictedCrimePropertyValues;
import edu.psu.capstone894.crimepredictor.model.types.PropertyCrimeValues;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PredictedCrimePropertyValuesRepository extends JpaRepository<PredictedCrimePropertyValues,
        Integer> {

    List<PredictedCrimePropertyValues> findAllByZipcode(String zipCode);

    String query = "select cluster_date clusterDate, TRUNCATE(AVG(Total_Crime),0) totalCrime, TRUNCATE(AVG(Price),0) price" +
            " from ( select cluster_date cluster_date,  Total_Crime, Price from predicted_crime_property_values ) t1" +
            " group by cluster_date";

    @Query(value = query, nativeQuery = true)
    List<PropertyCrimeValues> findAverageForAllZipCodes();


    String query2 = "select distinct zipcode from predicted_crime_property_values";

    @Query(value = query2, nativeQuery = true)
    List<String> getAllZipCodes();
}
