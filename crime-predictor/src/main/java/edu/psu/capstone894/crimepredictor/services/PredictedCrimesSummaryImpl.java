package edu.psu.capstone894.crimepredictor.services;

import edu.psu.capstone894.crimepredictor.api.PredictedCrimesSummarySrv;
import edu.psu.capstone894.crimepredictor.model.PredictedCrimesSummary;
import edu.psu.capstone894.crimepredictor.persistence.PredictedCrimesSummaryRepository;
import lombok.AllArgsConstructor;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@AllArgsConstructor
public class PredictedCrimesSummaryImpl implements PredictedCrimesSummarySrv {

    private PredictedCrimesSummaryRepository predictedCrimesSummaryRepository;

    @Override
    public JSONArray getPredictedCrimesSummary() {
        List<PredictedCrimesSummary> records = predictedCrimesSummaryRepository
                .findAll();

        SortedMap<Integer, Map<String, Integer>> pivoted = new TreeMap<>();
        records.stream()
                .sorted(Comparator.comparing(PredictedCrimesSummary::getYear))
                .forEach((record) -> {
                    if (!pivoted.containsKey(record.getYear()))
                        pivoted.put(record.getYear(), new HashMap<>());

                    Map<String, Integer> map = pivoted.get(record.getYear());
                    map.put(record.getCrimeDescription(), record.getTotalCrimes());
                });

        JSONArray result = new JSONArray();
        pivoted.forEach((key, map) -> {
            JSONObject obj = new JSONObject();
            obj.put("year", key);
            map.forEach((k, v) -> {
                obj.put(k, v);
            });
            result.add(obj);
        });

        return result;
    }


    @Override
    public List<PredictedCrimesSummary> getCrimeBreakdownForYear(Integer year) {
        List<PredictedCrimesSummary> allByYear = predictedCrimesSummaryRepository.findAllByYear(year);
        return allByYear;
    }

}
