package edu.psu.capstone894.crimepredictor.api;

import edu.psu.capstone894.crimepredictor.model.Attribute;
import net.minidev.json.JSONObject;

import java.util.List;

/**
 * The interface is a superclass for services that retrieve Attribute objects from the repository
 */
public interface AttributesService {

    /**
     * Retrieves all Attributes from the Attribute Repository
     *
     * @return all Attributes in the form of a JSON Object
     */
    JSONObject getAllAttributes();

    /**
     * Retrieves Attributes by the type passed as a parameter
     *
     * @param type the Attribute descriptor the user is looking for
     * @return list of Attribute objects
     */
    List<Attribute> findAttributeByType(String type);

    /**
     * Retrieves Attributes by the specified city
     *
     * @param city the city descriptor the user is looking for
     * @return all Attributes that meet the criteria in the form of a JSON Object
     */
    JSONObject findAttributesByCity(String city);

}
