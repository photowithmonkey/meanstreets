package edu.psu.capstone894.crimepredictor.api;

import net.minidev.json.JSONArray;

public interface PredictedCrimesSummaryMonthlySrv {
    JSONArray getPredictedCrimesSummaryMonthly(int year);
}
