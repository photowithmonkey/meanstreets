package edu.psu.capstone894.crimepredictor.api;

import edu.psu.capstone894.crimepredictor.model.CrimeEvent;

import java.util.List;

/**
 * The interface is a superclass for services that retrieve Event objects from the repository
 */
public interface EventsService {

    /**
     * Retrieves all Events from the Event Repository
     *
     * @return all Events in the form of a JSON object
     */
    List<CrimeEvent> getAllEvents();

    /**
     * Posts Event object to Event repository
     *
     * @param event object that will be posted
     * @return Event object information
     */
    CrimeEvent newEvent(CrimeEvent event);
}
