package edu.psu.capstone894.crimepredictor.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class CrimeHoods {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private Integer year;
    private String hood;
    private int totalCrime;
}
