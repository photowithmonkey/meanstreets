package edu.psu.capstone894.crimepredictor.model;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * This class represents calendar events for the crime predictor application
 *
 * <p>
 *     This class is described by the @Entity, @Getter, @Setter, and @NoArgsConstructor. These annotations
 *     allow boilerplate code to be generated through either persistence or the lombok library.
 *
 *     The @Entity annotation marks the POJO as a JPA enitity. Allows class name to be used in queries
 *     The @Getter and @Setter annotations gets the lombok library to generate getter and setter methods automatically.
 *     The @NoArgsConstructor annotation will generate a constructor with no arguments.
 * </p>
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class CrimeEvent {

    /**
     * This variable is the primary key for attribute entities.
     *
     * <p>
     *     The @Id annotation specifies the primary key for this entity.
     *      The @GeneratedValue annotation specifies how the primary keys are created.
     * </p>
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long eventId;

    private String title;
    private Boolean allDay;
    private Integer startYear;
    private Integer startMonth;
    private Integer startDay;
    private Integer startHour;
    private Integer startMinute;
    private Integer endYear;
    private Integer endMonth;
    private Integer endDay;
    private Integer endHour;
    private Integer endMinute;
    private String color;

    /**
     * This constuctor creates the object using the lombok @Builder annotation
     *
     * <p>
     *     The @Builder annotation will automatically produce the code required to instantiate the object.
     * </p>
     *
     * @param title
     * @param allDay
     * @param startYear
     * @param startMonth
     * @param startDay
     * @param startHour
     * @param startMinute
     * @param endYear
     * @param endMonth
     * @param endDay
     * @param endHour
     * @param endMinute
     * @param color
     */
    @Builder
    public CrimeEvent(String title, Boolean allDay, Integer startYear, Integer startMonth,
                      Integer startDay, Integer startHour, Integer startMinute, Integer endYear,
                      Integer endMonth, Integer endDay, Integer endHour, Integer endMinute, String color) {
        this.title = title;
        this.allDay = allDay;
        this.startYear = startYear;
        this.startMonth = startMonth;
        this.startDay = startDay;
        this.startHour = startHour;
        this.startMinute = startMinute;
        this.endYear = endYear;
        this.endMonth = endMonth;
        this.endDay = endDay;
        this.endHour = endHour;
        this.endMinute = endMinute;
        this.color = color;
    }
}
