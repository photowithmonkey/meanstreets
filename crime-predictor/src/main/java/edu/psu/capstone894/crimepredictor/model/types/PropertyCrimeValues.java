package edu.psu.capstone894.crimepredictor.model.types;

import java.time.LocalDate;

public interface PropertyCrimeValues {

    LocalDate getClusterDate();

    Double getTotalCrime();

    Double getPrice();
}
