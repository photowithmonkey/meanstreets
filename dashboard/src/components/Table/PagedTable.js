import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import cx from "classnames";
import {makeStyles} from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import {KeyboardArrowLeft, KeyboardArrowRight} from "@material-ui/icons";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import LastPageIcon from "@material-ui/icons/LastPage";
import {CircularProgress} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";

const useStyles = makeStyles(theme => ({
    root: {
        flexShrink: 0,
        marginLeft: theme.spacing(2.5),
    },
    loadingIndicatorContainer: {
        height: 400
    },
    loadingIndicator: {}
}));


function mapTableColumns(data, columns) {
    return (iterator) => {
        return data.map((row, key) => {
            const orderedRow = columns.map((col) => {
                return row[col.accessor];
            });
            return iterator(orderedRow, key);
        });
    }
}


function TablePaginationActions(props) {
    const classes = useStyles();
    const {count, page, rowsPerPage, onChangePage} = props;

    const handleFirstPageButtonClick = event => {
        onChangePage(event, 0);
    };

    const handleBackButtonClick = event => {
        onChangePage(event, page - 1);
    };

    const handleNextButtonClick = event => {
        onChangePage(event, page + 1);
    };

    const handleLastPageButtonClick = event => {
        onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
    };

    return (
        <div className={classes.root}>
            <IconButton
                onClick={handleFirstPageButtonClick}
                disabled={page === 0}
                aria-label="first page"
            >
                <FirstPageIcon/>
            </IconButton>
            <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
                <KeyboardArrowLeft/>
            </IconButton>
            <IconButton
                onClick={handleNextButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="next page"
            >
                <KeyboardArrowRight/>
            </IconButton>
            <IconButton
                onClick={handleLastPageButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="last page"
            >
                <LastPageIcon/>
            </IconButton>
        </div>
    );
}


TablePaginationActions.propTypes = {
    count: PropTypes.number.isRequired,
    onChangePage: PropTypes.func.isRequired,
    page: PropTypes.number.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
};

export default function PagedTable(props) {
    const classes = useStyles();
    const {
        tableHead,
        tableData,
        tableHeaderColor,
        hover,
        colorsColls,
        coloredColls,
        customCellClasses,
        customClassesForCells,
        striped,
        tableShopping,
        customHeadCellClasses,
        customHeadClassesForCells,
        sliceRequest
    } = props;


    const initialState = {
        page: 0,
        rowsPerPage: 50,
        startIdx: 0
    };

    const [state, setState] = React.useState(initialState);

    const handleChangePage = (event, newPage) => {
        setState({...state, page: newPage});
    };

    const handleChangeRowsPerPage = event => {
        setState({...state, rowsPerPage: parseInt(event.target.value, 10), page: 0})
    };

    function formatDisplayedRows({from, to, count}) {
        const startIdx = state.startIdx;
        return `${startIdx + from}-${startIdx + to} of ${startIdx + count}${count < 300 ? "" : "+"}`
    }

    function showLoading() {
        return <Grid container justify="center" alignItems="center" className={classes.loadingIndicatorContainer}>
            <Grid item className={classes.loadingIndicator}>
                <div style={{textAlign:"center"}}>
                    <CircularProgress/>
                    <div><h4>LOADING</h4></div>
                </div>
            </Grid>
        </Grid>
    }


    function getPageOfData(page) {
        const {rowsPerPage} = state;
        const count = tableData.length;

        if (count === 0) {
            if (state.page !== 0) {
                setState({...state, page: 0})
            }
            return [];
        }

        const isLastPage = page >= Math.ceil(count / rowsPerPage) - 1;
        const isFirstPage = page === 0;

        if (isFirstPage || isLastPage) {
            sliceRequest(page > 0 ? "forward" : "back")
        }

        const startIdx = page * rowsPerPage;
        return tableData.slice(startIdx, Math.min(count - 1, startIdx + rowsPerPage - 1));
    }

    return (
        (!!tableData && tableData.length > 0) ?
            <div className={classes.tableResponsive}>
                <Table className={classes.table}>
                    {tableHead !== undefined ? (
                        <TableHead className={classes[tableHeaderColor]}>
                            <TableRow className={classes.tableRow + " " + classes.tableRowHead}>
                                {tableHead.map((prop, key) => {
                                    const tableCellClasses =
                                        classes.tableHeadCell +
                                        " " +
                                        classes.tableCell +
                                        " " +
                                        cx({
                                            [customHeadCellClasses[
                                                customHeadClassesForCells.indexOf(key)
                                                ]]: customHeadClassesForCells.indexOf(key) !== -1,
                                            [classes.tableShoppingHead]: tableShopping,
                                            [classes.tableHeadFontSize]: !tableShopping
                                        });
                                    return (
                                        <TableCell className={tableCellClasses} key={key}>
                                            {prop.header}
                                        </TableCell>
                                    );
                                })}
                            </TableRow>
                        </TableHead>
                    ) : null}
                    <TableBody>
                        {mapTableColumns(getPageOfData(state.page), tableHead)((prop, key) => {
                            let rowColor = "";
                            let rowColored = false;
                            if (prop.color !== undefined) {
                                rowColor = prop.color;
                                rowColored = true;
                                prop = prop.data;
                            }
                            const tableRowClasses = cx({
                                [classes.tableRowBody]: true,
                                [classes.tableRowHover]: hover,
                                [classes[rowColor + "Row"]]: rowColored,
                                [classes.tableStripedRow]: striped && key % 2 === 0
                            });

                            return (
                                <TableRow
                                    key={key}
                                    hover={hover}
                                    className={classes.tableRow + " " + tableRowClasses}
                                >
                                    {prop.map((prop, key) => {
                                        const tableCellClasses =
                                            classes.tableCell +
                                            " " +
                                            cx({
                                                [classes[colorsColls[coloredColls.indexOf(key)]]]:
                                                coloredColls.indexOf(key) !== -1,
                                                [customCellClasses[customClassesForCells.indexOf(key)]]:
                                                customClassesForCells.indexOf(key) !== -1
                                            });
                                        return (
                                            <TableCell className={tableCellClasses} key={key}>
                                                {prop}
                                            </TableCell>
                                        );
                                    })}
                                </TableRow>
                            );
                        })}
                    </TableBody>
                    <TableFooter>
                        <TableRow className={classes.tableRow}>
                            <TablePagination
                                rowsPerPageOptions={[50, 100, 200]}
                                colSpan={tableHead.length}
                                count={tableData.length}
                                rowsPerPage={state.rowsPerPage}
                                page={state.page} //current page
                                SelectProps={{
                                    inputProps: {'aria-label': 'rows per page'},
                                    native: true,
                                }}
                                labelDisplayedRows={formatDisplayedRows}
                                onChangePage={handleChangePage}
                                onChangeRowsPerPage={handleChangeRowsPerPage}
                                ActionsComponent={TablePaginationActions}
                            />
                        </TableRow>
                    </TableFooter>
                </Table>
            </div>
            : showLoading()
    )
}

PagedTable.defaultProps = {
    tableHeaderColor: "gray",
    hover: false,
    colorsColls: [],
    coloredColls: [],
    striped: false,
    customCellClasses: [],
    customClassesForCells: [],
    customHeadCellClasses: [],
    customHeadClassesForCells: []
};

PagedTable.propTypes = {
    tableHeaderColor: PropTypes.oneOf([
        "warning",
        "primary",
        "danger",
        "success",
        "info",
        "rose",
        "gray"
    ]),
    tableHead: PropTypes.array,
    // Of(PropTypes.arrayOf(PropTypes.node)) || Of(PropTypes.object),
    tableData: PropTypes.array,
    hover: PropTypes.bool,
    coloredColls: PropTypes.arrayOf(PropTypes.number),
    // Of(["warning","primary","danger","success","info","rose","gray"]) - colorsColls
    colorsColls: PropTypes.array,
    customCellClasses: PropTypes.arrayOf(PropTypes.string),
    customClassesForCells: PropTypes.arrayOf(PropTypes.number),
    customHeadCellClasses: PropTypes.arrayOf(PropTypes.string),
    customHeadClassesForCells: PropTypes.arrayOf(PropTypes.number),
    striped: PropTypes.bool,
    // this will cause some changes in font
    tableShopping: PropTypes.bool
};
