import Slider from '@material-ui/core/Slider';
import {makeStyles} from '@material-ui/core/styles';
import moment from "moment";
import React from 'react';
import {warningColor} from '../../assets/jss/material-dashboard-pro-react';
import chartsStyle from "../../assets/jss/material-dashboard-pro-react/views/chartsStyle.js";
import debounce from "lodash/debounce";

const useStyles = makeStyles(theme => ({
    margin: {
        height: theme.spacing(3),
    },
    slider: {
        color: warningColor[2]
    },
    legendTitle: chartsStyle.legendTitle
}));

const NUM_YEARS = 12;
const RES = 100 / (NUM_YEARS - 1);
const START_YEAR = 2008;

const marks = (new Array(NUM_YEARS))
    .fill(0)
    .map((v, c) => c * RES)
    .map((v, c) => ({value: v, label: moment(START_YEAR + c, 'YYYY').format('YY')}));


function valuetext(value) {
    const year = moment(START_YEAR + value / RES, 'YYYY');
    return year.format('YYYY');
}


export default function DiscreteSlider(props) {

    const classes = useStyles();

    const handleSliderChange = debounce(_handleSliderChange, 100);

    function _handleSliderChange(e, newValue) {
        props.handleSliderChange && props.handleSliderChange(START_YEAR + newValue / RES);
    }

    return (
        <div className={classes.root}>
            <h6 id="discrete-slider-restrict" className={classes.legendTitle}>Select Year</h6>
            <Slider
                className={classes.slider}
                defaultValue={(props.defaultYear - START_YEAR) * RES}
                valueLabelFormat={valuetext}
                getAriaValueText={valuetext}
                aria-labelledby="discrete-slider-restrict"
                step={null}
                valueLabelDisplay="auto"
                marks={marks}
                onChange={handleSliderChange}
            />
        </div>
    );
}