import React from "react";
import {makeStyles, Modal, Button} from "@material-ui/core";
import { useHistory } from "react-router-dom";

//For the modal
function rand() {
  return Math.round(Math.random() * 20) - 10;
}
//for the modal
function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();
  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}
//For the modal
const useStyles = makeStyles(theme => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    position: 'absolute',
    width: 450,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function SimpleModal() {
  const history = useHistory();
  const classes = useStyles();
  const [modalStyle] = React.useState(getModalStyle);
  //const [open, setOpen] = React.useState(false);
  const [open, setOpen] = React.useState(true);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
 //   if (success) {
 //     history.push("/auth/verify-page");
 //   }
  };

  //const message = useRef('hey');

  let message = '';
  let buttonText = '';

 // if (error) {
 //   message = error.message;
 //   buttonText = 'Fix it';
 // } else if (success) {
 //   message = "Congratulations, you have successfully signed up. Please check your email for a verification code.";
 //   buttonText = 'Continue';
 // }

  return (
    <div>
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={open}
        onClose={handleClose}
      >
        <div style={modalStyle} className={classes.paper}>
          <h2>ACHTUNG!</h2>
          <hr/>
          <h5>{this.state.error}</h5>
          <hr/>

          <Button variant="contained" color="primary" onClick={handleClose}>
            {buttonText}
          </Button>

        </div>
      </Modal>
    </div>
  );
}