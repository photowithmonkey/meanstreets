import React from "react";
import cx from "classnames";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";

// core components
import Button from "components/CustomButtons/Button";

// @material-ui components
import { makeStyles } from "@material-ui/core/styles";
import { List, ListItem, ListItemText, AppBar, Toolbar, Hidden } from "@material-ui/core";
import Menu from "@material-ui/icons/Menu";
import Drawer from "@material-ui/core/Drawer";

import styles from "assets/jss/material-dashboard-pro-react/components/authNavbarStyle.js";

const useStyles = makeStyles(styles);

const appBarStyle = {
    backgroundColor: "transparent",
    boxShadow: "none",
    borderBottom: "0",
    marginBottom: "0",
    position: "absolute",
    width: "100%",
    paddingTop: "10px",
    zIndex: "1029",
    color: "#555555",
    border: "0",
    borderRadius: "3px",
    padding: "10px 0",
    transition: "all 150ms ease 0s",
    minHeight: "50px",
    display: "block"
};

const listItemTextStyle = {
    padding:"0"
};

const toolbarStyle =  {
    backgroundColor: "transparent"
}

export default function AboutNavbar(props) {
    const [open, setOpen] = React.useState(false);
    const handleDrawerToggle = () => {
        setOpen(!open);
    }

    const activeRoute = routeName=> {
        return window.location.href.indexOf(routeName) > -1 ? true : false;
    };

    const classes = useStyles();
    const { color, pageName } = props;
    const appBarClasses = cx({
        [" " + classes[color]]: color
    });

    var list = (
        <List className={classes.list}>
            <ListItem className={classes.listItem}>
                <NavLink 
                    to={"/about/meanstreets"} 
                    className={cx(classes.NavLink, {
                        [classes.navLinkActive]: activeRoute("/about/meanstreets")
                })}>
                    <ListItemText
                        primary={"Mean Streets"}
                        disableTypography={true}
                        // style={classes.ListItemText}
                    />                  
                </NavLink>
            </ListItem>

            <ListItem className={classes.listItem}>
                <NavLink 
                    to={"/about/aj"} 
                    className={cx(classes.NavLink, {
                        [classes.navLinkActive]: activeRoute("/about/aj")
                })}>
                    <ListItemText
                        primary={"AJ"}
                        disableTypography={true}
                        className={classes.ListItemText}
                    />                  
                </NavLink>
            </ListItem>

            <ListItem className={classes.listItem}>
                <NavLink 
                    to={"/about/denis"} 
                    className={cx(classes.NavLink, {
                        [classes.navLinkActive]: activeRoute("/about/denis")
                })}>
                    <ListItemText
                        primary={"Denis"}
                        disableTypography={true}
                        className={classes.ListItemText}
                    />                  
                </NavLink>
            </ListItem>

            <ListItem className={classes.listItem}>
                <NavLink 
                    to={"/about/emin"} 
                    className={cx(classes.NavLink, {
                        [classes.navLinkActive]: activeRoute("/about/emin")
                })}>
                    <ListItemText
                        primary={"Emin"}
                        disableTypography={true}
                        className={classes.ListItemText}
                    />                  
                </NavLink>
            </ListItem>

            <ListItem className={classes.listItem}>
                <NavLink 
                    to={"/about/justin"} 
                    className={cx(classes.NavLink, {
                        [classes.navLinkActive]: activeRoute("/about/justin")
                })}>
                    <ListItemText
                        primary={"Justin"}
                        disableTypography={true}
                        className={classes.ListItemText}
                    />                  
                </NavLink>
            </ListItem>

            <ListItem className={classes.listItem}>
                <NavLink 
                    to={"/about/paul"} 
                    className={cx(classes.NavLink, {
                        [classes.navLinkActive]: activeRoute("/about/paul")
                })}>
                    <ListItemText
                        primary={"Paul"}
                        disableTypography={true}
                        className={classes.ListItemText}
                    />                  
                </NavLink>
            </ListItem>
        </List>
    );

    return (
        <AppBar position="static" style={appBarStyle}>
            <Toolbar className={classes.container}>
                <Hidden smDown>
                    <div className={classes.flex}>
                        <Button href="#" className={classes.title} color="transparent"
                                fontsize="md">
                            {pageName}
                        </Button>
                    </div>
                </Hidden>
                <Hidden mdUp>
                    <div className={classes.flex}>
                        <Button href="#" className={classes.title} color="transparent">
                            MD Pro React
                        </Button>
                    </div>
                </Hidden>
                <Hidden smDown>{list}</Hidden>
                <Hidden mdUp>
                    <Button
                        className={classes.sidebarButton}
                        color="transparent"
                        justIcon
                        aria-label="open drawer"
                        onClick={handleDrawerToggle}
                    >
                        <Menu />
                    </Button>
                </Hidden>
                <Hidden mdUp>
                    <Hidden mdUp>
                        <Drawer
                            variant="temporary"
                            anchor={"right"}
                            open={open}
                            classes={{
                                paper: classes.drawerPaper
                            }}
                            onClose={handleDrawerToggle}
                            ModalProps={{
                                keepMounted: true // Better open performance on mobile.
                            }}
                            >
                            {list}
                        </Drawer>
                    </Hidden>
                </Hidden>
            </Toolbar>
        </AppBar>
    );
}

AboutNavbar.propTypes = {
    color: PropTypes.oneOf(["primary", "info", "success", "warning", "danger", "transparent"]),
    pageName: PropTypes.string
};