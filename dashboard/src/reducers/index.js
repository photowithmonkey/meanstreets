import searchFiltersSlice from "./searchFiltersSlice";
import {combineReducers} from "redux";

export default combineReducers({
    searchFiltersSlice
})