import {createSlice} from "redux-starter-kit";

const searchFiltersSlice = createSlice({
    name: "filters",
    initialState: {selectedCity: null, cities: [], attributes: []},
    reducers: {
        selectCity(state, action) {
            state.selectedCity = action.payload.city;
        },
        saveAvailableCities(state, action) {
            state.cities = action.payload.cities;
        },
        saveAttributes(state, action) {
            state.attributes = action.payload.attributes;
        }
    }
});

export const {selectCity, saveAttributes, saveAvailableCities} = searchFiltersSlice.actions;

export default searchFiltersSlice.reducer;

