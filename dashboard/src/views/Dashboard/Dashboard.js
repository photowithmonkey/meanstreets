import Icon from "@material-ui/core/Icon";
// @material-ui/core components
import {makeStyles} from "@material-ui/core/styles";
import DateRange from "@material-ui/icons/DateRange";
import Language from "@material-ui/icons/Language";
import LocalOffer from "@material-ui/icons/LocalOffer";
// @material-ui/icons
// import ContentCopy from "@material-ui/icons/ContentCopy";
import Store from "@material-ui/icons/Store";
import Update from "@material-ui/icons/Update";
// import InfoOutline from "@material-ui/icons/InfoOutline";
import Warning from "@material-ui/icons/Warning";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import CardIcon from "components/Card/CardIcon.js";
// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Table from "components/Table/Table.js";
import Danger from "components/Typography/Danger.js";
import React from "react";
// react plugin for creating charts
// react plugin for creating vector maps
import {VectorMap} from "react-jvectormap";

import styles from "../../assets/jss/material-dashboard-pro-react/views/dashboardStyle.js";
import CrimeDistributionBars from "../../chartdata/CrimeDistributionBars";
import CrimeDistributionPie from "../../chartdata/CrimeDistributionPie";
import PredictedCrimes from "../../chartdata/PredictedCrimes";
import TopCrimeHoods from "../../chartdata/TopCrimeHoods";

const convertToMap = (crimeTable) => {
    let map = {};
    crimeTable.forEach((row) => map["US-" + row[1]] = (row[2]));
    return map;
};

const crimeTable = [
    ["Fairfield", "AL", 33.3888],
    ["Emeryville", "CA", 33.0527],
    ["Crowley", "LA", 27.2388],
    ["Mayfield", "KY", 22.75529],
    ["Magnolia", "AK", 22.3684],
    ["Cambridge", "MD", 20.7053],
    ["Lebanon", "MO", 17.5299],
    ["Riverdale", "GA", 16.6214],
    ["Zachary", "LA", 15.3129],
    ["Millington", "TN", 14.9492]
];


const useStyles = makeStyles(styles);

export default function Dashboard() {
    const classes = useStyles();
    return (
        <div>
            <TopCrimeHoods/>
            <GridContainer>
                <GridItem xs={12}>
                    <Card>
                        <CardHeader color="success" icon>
                            <CardIcon color="success">
                                <Language/>
                            </CardIcon>
                            <h4 className={classes.cardIconTitle}>
                                Top 10 Growing Crime Cities
                            </h4>
                        </CardHeader>
                        <CardBody>
                            <GridContainer justify="space-between">
                                <GridItem xs={12} sm={12} md={5}>
                                    <Table
                                        tableHead={["City", "State", "Ranking Change/1,000"]}
                                        tableData={crimeTable}
                                    />
                                </GridItem>
                                <GridItem xs={12} sm={12} md={6}>
                                    <VectorMap
                                        map={"us_aea"}
                                        backgroundColor="transparent"
                                        zoomOnScroll={false}
                                        containerStyle={{
                                            width: "100%",
                                            height: "280px"
                                        }}
                                        containerClassName="map"
                                        regionStyle={{
                                            initial: {
                                                fill: "#e4e4e4",
                                                "fill-opacity": 0.9,
                                                stroke: "none",
                                                "stroke-width": 0,
                                                "stroke-opacity": 0
                                            }
                                        }}
                                        series={{
                                            regions: [
                                                {
                                                    values: convertToMap(crimeTable),
                                                    scale: ["#AAAAAA", "#444444"],
                                                    normalizeFunction: "polynomial"
                                                }
                                            ]
                                        }}
                                    />
                                </GridItem>
                            </GridContainer>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
            <GridContainer>
                <GridItem xs={12} sm={12} md={4}>
                    <PredictedCrimes type="small"/>
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                    <CrimeDistributionBars type="small"/>
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                    <CrimeDistributionPie type="small"/>
                </GridItem>
            </GridContainer>
        </div>
    );
}
