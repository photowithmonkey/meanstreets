import GridContainer from "../../components/Grid/GridContainer";
import React from "react";
import GridItem from "../../components/Grid/GridItem";
import CardHeader from "../../components/Card/CardHeader";
import Card from "../../components/Card/Card";
import {makeStyles} from "@material-ui/core/styles";

import _ from 'lodash';
import styles from "assets/jss/material-dashboard-pro-react/views/incidentFormsStyle";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import CardBody from "../../components/Card/CardBody";
import CardText from "../../components/Card/CardText";
import axios from "axios";
import wNumb from "wnumb";
import Button from "../../components/CustomButtons/Button";
import * as queryString from "query-string";
import DateRangeSlider from "../../components/Input/DateRangeSlider";
import {useDispatch, useSelector} from "react-redux";
import {saveAttributes} from "../../reducers/searchFiltersSlice";
import moment from "moment";


const useStyles = makeStyles((theme) => ({styles}));


function formatFilterSelection(name, selection, values) {
    if (selection != null) {
        const sl = Array.isArray(selection) ? selection : [selection];
        if (!_.isEmpty(sl)) {
            const va = !!values ? sl.map((v) => values[v]) : selection;
            const o = {};
            o[name] = va;
            return queryString.stringify(o, 'comma');
        }
    }

    return null;
}

const dateConvert = {
    // to: (date) => date.format("MM/DD/YYYY"),
    to: (date) => date.format("YYYY-MM-DD"),
    from: (dateStr) => moment(dateStr)
};

const firstDayOfYear = moment().dayOfYear(1);
const lastDayOfYear = moment().dayOfYear(365);

// const Format = wNumb({decimals: 0});

const initState = {
    range: [firstDayOfYear, lastDayOfYear],
    minDate: firstDayOfYear,
    maxDate: lastDayOfYear,
    selected: {},
    attributes: {}
}

export default function IncidentQueryFormPred(props) {

    const [state, setState] = React.useState(initState);

    const selectedCity = useSelector(state => state.searchFiltersSlice.selectedCity);
    const dispatch = useDispatch();


    // get the available filters for from the database
    React.useEffect(() => {
        selectedCity && fetchFilters(selectedCity);
    }, [selectedCity]);


    // this is only called when the city has changed to redraw the filters
    // it also fires the Submit Query filter action to update the data
    function fetchFilters(selectedCity) {
        axios.get("/api/attributes/city/" + selectedCity).then((js) => {
            let attributes = js.data.attributes;
            const minDate = dateConvert.from(js.data.mindate);
            const maxDate = dateConvert.from(js.data.maxdate);

            setState({
                ...state,
                attributes: attributes,
                range: [minDate, maxDate],
                selected: {},
                minDate: minDate,
                maxDate: maxDate,
            });

            dispatch(saveAttributes({attributes: attributes}));
            handleSubmitClick({attributes,minDate,maxDate,selected});
        });
    }

    function handleFilterSelect(key, event) {
        const {selected} = state;
        selected[key] = event.target.value;
        setState({...state, selected: selected});
    }

    function handleDateRangeChange(minDate, maxDate) {
        setState({...state, minDate: minDate, maxDate: maxDate});
    }

    // constructs  a filter from the current settings
    // called by either the Submit Button on the form, or when the city has changed and redrawn the filters
    function handleSubmitClick({attributes, minDate, maxDate, selected}) {

        let params = {};
        // add selected city parameter
        params["city"] = formatFilterSelection("city", selectedCity);
        // add other non-date filters
        Object.keys(selected).forEach((key) => {
            params[key] = formatFilterSelection(key, selected[key], attributes[key]);
        });
        // add date filters
        params["dateFrom"] = formatFilterSelection("dateFrom", dateConvert.to(minDate));
        params["dateTo"] = formatFilterSelection("dateTo", dateConvert.to(maxDate));

        let paramStr = "";
        Object.keys(params).forEach(key => {
            if (params[key]) {
                paramStr += "&" + params[key]
            }
        });
        paramStr = paramStr.substr(1);
        //callback to set filters on table or map
        props.returnSelectedFilters(paramStr);
    }

    const classes = useStyles();
    const {attributes, selected, range} = state;

    return (<Card>
            <CardHeader color="rose" text>
                <CardText color="rose">
                    <h4 className={classes.cardTitle}>Filter Data:</h4>
                </CardText>
            </CardHeader>
            <CardBody>
                <GridContainer>
                    {
                        Object.keys(attributes).sort().map((filter, key) =>
                            <GridItem xs={12} sm={6} md={6} lg={6} key={key}>
                                <FormControl
                                    fullWidth
                                    className={classes.selectFormControl}
                                >
                                    <InputLabel
                                        htmlFor="multiple-select"
                                        className={classes.selectLabel}
                                    >
                                        Choose {filter}
                                    </InputLabel>
                                    <Select
                                        multiple
                                        value={selected[filter] || []}
                                        onChange={(e) => handleFilterSelect(filter, e)}
                                        MenuProps={{className: classes.selectMenu}}
                                        classes={{select: classes.select}}
                                        inputProps={{
                                            name: "multipleSelect",
                                            id: "multiple-select"
                                        }}
                                    >
                                        <MenuItem
                                            disabled
                                            classes={{
                                                root: classes.selectMenuItem
                                            }}
                                        >
                                            Choose {filter}
                                        </MenuItem>
                                        {
                                            attributes[filter] ? attributes[filter].map((label, key) =>
                                                <MenuItem key={key}
                                                          classes={{
                                                              root: classes.selectMenuItem,
                                                              selected: classes.selectMenuItemSelectedMultiple
                                                          }}
                                                          value={key}
                                                >
                                                    {label}
                                                </MenuItem>
                                            ) : null
                                        }
                                    </Select>
                                </FormControl>
                            </GridItem>
                        )
                    }
                    <GridItem xs={12} sm={12} md={12} lg={12}>
                        <div style={{paddingTop: "23px"}}></div>
                        <InputLabel
                            style={{paddingBottom: "6px"}}
                            htmlFor="simple-select"
                            className={classes.selectLabel}
                        >
                            Select Date
                        </InputLabel>
                        <DateRangeSlider range={range} handleDateRangeChange={handleDateRangeChange}/>
                    </GridItem>
                    <GridItem xs={12} sm={12} md={12} lg={12}>
                        <Button onClick={() => handleSubmitClick(state)}>Submit</Button>
                    </GridItem>
                </GridContainer>
            </CardBody>
        </Card>
    )
}

