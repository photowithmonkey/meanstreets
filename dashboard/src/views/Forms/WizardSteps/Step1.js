import React from "react";
import PropTypes from "prop-types";
// @material-ui/icons
import Face from "@material-ui/icons/Face";
import RecordVoiceOver from "@material-ui/icons/RecordVoiceOver";
import Email from "@material-ui/icons/Email";
import Lock from "@material-ui/icons/Lock"
import AccountCircle from "@material-ui/icons/AccountCircle"
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import InputAdornment from "@material-ui/core/InputAdornment";
// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import PictureUpload from "components/CustomUpload/PictureUpload.js";
import CustomInput from "components/CustomInput/CustomInput.js";

const style = {
    infoText: {
        fontWeight: "300",
        margin: "10px 0 30px",
        textAlign: "center"
    },
    inputAdornmentIcon: {
        color: "#555"
    },
    inputAdornment: {
        position: "relative"
    }
};

class Step1 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstname: "",
            firstnameState: "",
            lastname: "",
            lastnameState: "",
            email: "",
            emailState: "",
            username: "",
            usernameState: "",
            password: ""
        };
    }

    sendState() {
        return this.state;
    }

    // function that returns true if value is email, false otherwise
    verifyEmail(value) {
        var emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return emailRex.test(value);
    }

    // function that verifies if a string has a given length or not
    verifyLength(value, length) {
        return value.length >= length;
    }

    verifyPassword = (pa, length) => this.verifyLength(pa, length) && /[A-Z]/.test(pa) && /[0-9]/.test(pa) && /[\W]/.test(pa);

    change(event, stateName, type, stateNameEqualTo) {
        switch (type) {
            case "email":
                if (this.verifyEmail(event.target.value)) {
                    this.setState({[stateName + "State"]: "success"});
                } else {
                    this.setState({[stateName + "State"]: "error"});
                }
                break;
            case "length":
                if (this.verifyLength(event.target.value, stateNameEqualTo)) {
                    this.setState({[stateName + "State"]: "success"});
                } else {
                    this.setState({[stateName + "State"]: "error"});
                }
                break;
            case "password":
                this.setState({[stateName + "State"]: this.verifyPassword(event.target.value, stateNameEqualTo) ? "success" : "error"});
                break;
            default:
                break;
        }
        this.setState({[stateName]: event.target.value});
    }

    isValidated() {
        if (
            this.state.firstnameState === "success" &&
            this.state.lastnameState === "success" &&
            this.state.emailState === "success" &&
            this.state.usernameState === "success" &&
            this.state.passwordState === "success"
        ) {
            return true;
        } else {
            if (this.state.firstnameState !== "success") {
                this.setState({firstnameState: "error"});
            }
            if (this.state.lastnameState !== "success") {
                this.setState({lastnameState: "error"});
            }
            if (this.state.emailState !== "success") {
                this.setState({emailState: "error"});
            }
            if (this.state.usernameState !== "success") {
                this.setState({usernameState: "error"});
            }
            if (this.state.passwordState !== "success") {
                this.setState({passwordState: "error"});
            }
        }
        return false;
    }

    render() {
        const {classes} = this.props;
        return (
            <GridContainer justify="center">
                <GridItem xs={12} sm={12}>
                    <h4 className={classes.infoText}>
                        Let{"'"}s start with the basic information (with validation)
                    </h4>
                </GridItem>
                <GridItem xs={12} sm={4}>
                    <PictureUpload/>
                </GridItem>
                <GridItem xs={12} sm={6}>
                    <CustomInput
                        success={this.state.firstnameState === "success"}
                        error={this.state.firstnameState === "error"}
                        labelText={
                            <span>
                First Name <small>(required)</small>
              </span>
                        }
                        id="firstname"
                        formControlProps={{
                            fullWidth: true
                        }}
                        inputProps={{
                            onChange: event => this.change(event, "firstname", "length", 3),
                            endAdornment: (
                                <InputAdornment
                                    position="end"
                                    className={classes.inputAdornment}
                                >
                                    <Face className={classes.inputAdornmentIcon}/>
                                </InputAdornment>
                            )
                        }}
                    />
                    <CustomInput
                        success={this.state.lastnameState === "success"}
                        error={this.state.lastnameState === "error"}
                        labelText={
                            <span>
                Last Name <small>(required)</small>
              </span>
                        }
                        id="lastname"
                        formControlProps={{
                            fullWidth: true
                        }}
                        inputProps={{
                            onChange: event => this.change(event, "lastname", "length", 2),
                            endAdornment: (
                                <InputAdornment
                                    position="end"
                                    className={classes.inputAdornment}
                                >
                                    <RecordVoiceOver className={classes.inputAdornmentIcon}/>
                                </InputAdornment>
                            )
                        }}
                    />
                </GridItem>
                <GridItem xs={12} sm={12} md={12} lg={10}>
                    <CustomInput
                        success={this.state.emailState === "success"}
                        error={this.state.emailState === "error"}
                        labelText={
                            <span>
                Email <small>(required)</small>
              </span>
                        }
                        id="email"
                        formControlProps={{
                            fullWidth: true
                        }}
                        inputProps={{
                            onChange: event => this.change(event, "email", "email"),
                            startAdornment: (
                                <InputAdornment
                                    position="start"
                                    className={classes.inputAdornment}
                                >
                                    <Email className={classes.inputAdornmentIcon}/>
                                </InputAdornment>
                            )
                        }}
                    />
                </GridItem>
                <GridItem xs={12} sm={12} md={12} lg={10}>
                    <CustomInput
                        success={this.state.usernameState === "success"}
                        error={this.state.usernameState === "error"}
                        labelText={
                            <span>
                Username <small>(required)</small>
              </span>
                        }
                        id="username"
                        formControlProps={{
                            fullWidth: true
                        }}
                        inputProps={{
                            onChange: event => this.change(event, "username", "length", 5),
                            startAdornment: (
                                <InputAdornment
                                    position="start"
                                    className={classes.inputAdornment}
                                >
                                    <AccountCircle className={classes.inputAdornmentIcon}/>
                                </InputAdornment>
                            )
                        }}
                    />
                </GridItem>
                <GridItem xs={12} sm={12} md={12} lg={10}>
                    <CustomInput
                        success={this.state.passwordState === "success"}
                        error={this.state.passwordState === "error"}
                        labelText={
                            <span>
                Password <small>(required)</small>
              </span>
                        }
                        id="password"
                        helperText="Password must contain at least one Upper Case Char, Number and Symbol"
                        formControlProps={{
                            fullWidth: true
                        }}
                        inputProps={{
                            onChange: event => this.change(event, "password", "password", 8),
                            startAdornment: (
                                <InputAdornment
                                    position="start"

                                    className={classes.inputAdornment}
                                >
                                    <Lock className={classes.inputAdornmentIcon}/>
                                </InputAdornment>
                            )
                        }}
                    />
                </GridItem>
            </GridContainer>
        );
    }
}

Step1.propTypes = {
    classes: PropTypes.object
};

export default withStyles(style)(Step1);
