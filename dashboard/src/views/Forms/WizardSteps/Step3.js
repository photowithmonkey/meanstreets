import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
// core components
import CustomInput from "../../../components/CustomInput/CustomInput.js";
import GridContainer from "../../../components/Grid/GridContainer.js";
import GridItem from "../../../components/Grid/GridItem.js";

import customSelectStyle from "../../../assets/jss/material-dashboard-pro-react/customSelectStyle.js";

const style = {
    infoText: {
        fontWeight: "300",
        margin: "10px 0 30px",
        textAlign: "center"
    },
    ...customSelectStyle
};

class Step3 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            phone: "",
            streetaddr: "",
            city: "",
            state: "",
            zip: ""
        };
    }

    sendState() {
        return this.state;
    }

    handleSimple = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    isValidated() {
        return true;
    }

    render() {
        const {classes} = this.props;
        return (
            <GridContainer justify="center">
                <GridItem xs={12} sm={12}>
                    <h4 className={classes.infoText}>Enter Your Contact Information.</h4>
                </GridItem>
                <GridItem xs={12} sm={7}>
                    <CustomInput
                        inputProps={{
                            name: "phone",
                            onChange: event => {
                                this.handleSimple(event)
                            }
                        }}
                        labelText="Phone Number (required)"
                        helperText="Format: +11111111111"
                        id="phone"
                        formControlProps={{
                            fullWidth: true
                        }}
                    />
                </GridItem>
                <GridItem xs={12} sm={7}>
                    <CustomInput
                        inputProps={{
                            name: "streetaddr",
                            onChange: event => {
                                this.handleSimple(event)
                            }
                        }}
                        labelText="Street Address"
                        id="streetaddr"
                        formControlProps={{
                            fullWidth: true
                        }}
                    />
                </GridItem>
                <GridItem xs={12} sm={3}>
                    <CustomInput
                        inputProps={{
                            name: "city",
                            onChange: event => {
                                this.handleSimple(event)
                            }
                        }}
                        labelText="City"
                        id="city"
                        formControlProps={{
                            fullWidth: true
                        }}
                    />
                </GridItem>
                <GridItem xs={12} sm={3}>
                    <FormControl fullWidth className={classes.selectFormControl}>
                        <InputLabel htmlFor="simple-select" className={classes.selectLabel}>
                            State
                        </InputLabel>
                        <Select
                            MenuProps={{
                                className: classes.selectMenu
                            }}
                            classes={{
                                select: classes.select
                            }}
                            value={this.state.state}
                            onChange={this.handleSimple}
                            inputProps={{
                                name: "state",
                                id: "state"
                            }}
                        >
                            <MenuItem
                                disabled
                                classes={{
                                    root: classes.selectMenuItem
                                }}
                            >
                                State
                            </MenuItem>
                            <MenuItem
                                classes={{
                                    root: classes.selectMenuItem,
                                    selected: classes.selectMenuItemSelected
                                }}
                                value="md"
                            >
                                MD
                            </MenuItem>
                            <MenuItem
                                classes={{
                                    root: classes.selectMenuItem,
                                    selected: classes.selectMenuItemSelected
                                }}
                                value="pa"
                            >
                                PA
                            </MenuItem>
                            <MenuItem
                                classes={{
                                    root: classes.selectMenuItem,
                                    selected: classes.selectMenuItemSelected
                                }}
                                value="nj"
                            >
                                NJ
                            </MenuItem>
                        </Select>
                    </FormControl>
                </GridItem>
                <GridItem xs={12} sm={5}>
                    <CustomInput
                        inputProps={{
                            name: "zip",
                            onChange: event => {
                                this.handleSimple(event)
                            }
                        }}
                        labelText="Zip"
                        id="zip"
                        formControlProps={{
                            fullWidth: true
                        }}
                    />
                </GridItem>
            </GridContainer>
        );
    }
}

Step3.propTypes = {
    classes: PropTypes.object
};

export default withStyles(style)(Step3);
