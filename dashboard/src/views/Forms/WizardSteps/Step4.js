import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import customSelectStyle from "assets/jss/material-dashboard-pro-react/customSelectStyle.js";

const style = {
    infoText: {
        fontWeight: "300",
        margin: "10px 0 30px",
        textAlign: "center"
    },
    ...customSelectStyle
};

class Step4 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            code: ''
        };
    }

    sendState() {
        return this.state;
    }

    handleSimple = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    isValidated() {
        return true;
    }

    render() {
        console.log(">>Step4.render()");
        console.log(this.props.allStates);
        const {classes} = this.props;
        return (

            <GridContainer justify="center">
                <GridItem xs={12} sm={12}>
                    <h4 className={classes.infoText}>Please confirm your details</h4>
                </GridItem>
                <GridItem>
                    <h4>Firstname: </h4>
                </GridItem>
            </GridContainer>
        );
    }
}

Step4.propTypes = {
    classes: PropTypes.object
};

export default withStyles(style)(Step4);
