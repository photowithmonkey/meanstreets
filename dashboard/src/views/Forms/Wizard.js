import React, {useEffect, useRef} from "react";

// core components
import Wizard from "components/Wizard/Wizard.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import Step1 from "./WizardSteps/Step1.js";
import Step2 from "./WizardSteps/Step2.js";
import Step3 from "./WizardSteps/Step3.js";
import Step4 from "./WizardSteps/Step4.js";
import {Auth} from "aws-amplify";
import { useHistory } from "react-router-dom";
//import SimpleModal from "../../components/Modals/simple-modal";
//import SimpleModal from "../../components/Modals/simple-modal";
import AnimatedModal from "../../components/Modals/animated-modal";
import Button from "@material-ui/core/Button";
import Modal from "@material-ui/core/Modal";
import {makeStyles} from "@material-ui/core";
//import Modal from "@material-ui/core/Modal";
//import Button from "@material-ui/core/Button";
import Popup from "reactjs-popup";

export default function WizardView() {
  const [username , setUsername] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [firstname, setFirstname] = React.useState('');
  const [lastname, setLastname] = React.useState('');
  const [phoneNum, setPhoneNum] = React.useState('');
  //const [address, setAddress] = React.useState('');
  const [role, setRole] = React.useState('');
  const [street, setStreet] = React.useState('');
  const [city, setCity] = React.useState('');
  const [usState, setUsState] = React.useState('');
  const [zip, setZip] = React.useState('');
  //const [show, setShow] = React.useState(false);
  //const handleClose = () => setShow(false);
  //const handleOpen = () => setShow(true);
  const [error, setError] = React.useState(false);
  const [success, setSuccess] = React.useState(false);
  const [showModal, setShowModal] = React.useState(false);
  //const [modalMsg, setModalMsg] = React.useState('');
  const history = useHistory();

  function Register() {
    let address = (street+","+city+","+usState+","+zip);
    //setAddress(addr);
    let profile = '';

    if (role.police) {
      profile = 'Police'
    } else if (role.realestate) {
      profile = 'Real Estate'
    } else if (role.citizen) {
      profile = 'Citizen'
    }

    console.log(">>Register");
    Auth.signUp({
      username: username,
      password: password,
      attributes: {
        given_name: firstname,
        family_name: lastname,
        email: email,
        phone_number: phoneNum,
        address: address,
        profile: profile
      }
    })
      .then(res => {
        console.log('SignUp Success', res);
        //setModalMsg("Congratulations, you have successfully signed up. Please check your email for a verification code.");
        setSuccess(res)
        setShowModal(true);
        //history.push("/auth/verify-page");
      })
      .catch(err => {

        //setModalMsg(error.message);
        setError(err);
        setShowModal(true);
        //might erase
        //setShowModal(!showModal);
      })
  }


  const firstUpdate = useRef(true);
  const regTried = useRef(false);
  //const message = useRef('');

  useEffect(() => {
    if (error === false) {
      if (firstUpdate.current) {
        firstUpdate.current = false;
      } else if (regTried.current) {
      } else {
        regTried.current = true;
        Register();

        //setShowModal(!showModal);
        //setShowModal(true);
      }
    } else if (error) {
    console.log("Signup Error: ",error);
    //setShowModal(!showModal);

      //setShowModal(true);
    }}, [username, password, email, error, success,
              showModal, street, city, usState, zip]
  );

  //For the modal
  function rand() {
    return Math.round(Math.random() * 20) - 10;
  }
  //for the modal
  function getModalStyle() {
    const top = 50 + rand();
    const left = 50 + rand();
    return {
      top: `${top}%`,
      left: `${left}%`,
      transform: `translate(-${top}%, -${left}%)`,
    };
  }
  //For the modal
  const useStyles = makeStyles(theme => ({
    modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    paper: {
      position: 'absolute',
      width: 450,
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
    },
  }));

  function SimpleModal() {
    const classes = useStyles();
    const [modalStyle] = React.useState(getModalStyle);
    //const [open, setOpen] = React.useState(false);
    const [open, setOpen] = React.useState(true);

    const handleOpen = () => {
      setOpen(true);
    };

    const handleClose = () => {
      setOpen(false);
      if (success) {
        history.push("/auth/verify-page");
      }
    };

    let message = '';
    let buttonText = '';

    if (error) {
      message = error.message;
      buttonText = 'Fix it';
    } else if (success) {
      message = "Congratulations, you have successfully signed up. Please check your email for a verification code.";
      buttonText = 'Continue';

    }

    return (
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={open}
          onClose={handleClose}
        >
          <div style={modalStyle} className={classes.paper}>
            <h2>ACHTUNG!</h2>
            <hr/>
            <h5>{message}</h5>
            <hr/>

              <Button variant="contained" color="primary" onClick={handleClose}>
                {buttonText}
            </Button>

          </div>
        </Modal>
    );
  }


  return (
       <GridContainer justify="center">
      <GridItem xs={12} sm={8}>
        <Wizard
          validate
          steps={[
            { stepName: "About", stepComponent: Step1, stepId: "about" },
            { stepName: "Role", stepComponent: Step2, stepId: "role" },
            { stepName: "Contact Info", stepComponent: Step3, stepId: "contact" }
            //{ stepName: "Confirm", stepComponent: Step4, stepId: "confirm" }
          ]}
          title="Build Your Profile"
          subtitle="This information will let us know more about you."
          previousButtonClick={event =>{
            setShowModal(false);
          }}
          nextButtonClick={event =>{
            setShowModal(false);
          }}
          finishButtonClick={event => {
            console.log("event:",event);
            setUsername(event.about.username);
            setPassword(event.about.password);
            setFirstname(event.about.firstname);
            setLastname(event.about.lastname);
            setEmail(event.about.email);
            setPhoneNum(event.contact.phone);
            setStreet(event.contact.streetaddr);
            setCity(event.contact.city);
            setUsState(event.contact.state);
            setZip(event.contact.zip);
            setRole(event.role);
            setShowModal(false);
            regTried.current = false;
            setError(false);
          }}
        />
        {showModal && <SimpleModal/>}

      </GridItem>
    </GridContainer>


  );
}
