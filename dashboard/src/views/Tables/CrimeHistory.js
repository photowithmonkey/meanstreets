import React from "react";
// @material-ui/icons
// core components
import GridContainer from "../../components/Grid/GridContainer.js";
import GridItem from "../../components/Grid/GridItem.js";
import Card from "../../components/Card/Card.js";
import CardBody from "../../components/Card/CardBody.js";
import CardHeader from "../../components/Card/CardHeader.js";
import PagedTable from "../../components/Table/PagedTable"

import axios from "axios";
import IncidentQueryForm from "../Forms/IncidentQueryForm";
import {useSelector} from "react-redux";
import CardFooter from "../../components/Card/CardFooter";
import Button from "../../components/CustomButtons/Button";

const fixedHeaderColumns = {
    header: "DATE",
    accessor: "crimeDate",
};


const compare = (v1, v2) => v1 === v2 ? 0 : v1 > v2 ? 1 : -1;

function createRows(s, data, startKey) {
    //add an id to all the rows
    const newSlice = s.content.map((row, key) => {
        return {...row, id: startKey + key};
    });

    // determine if the new slice replaces, appends or prepends the existing data
    // we use the key to do that, however if supplied data parameters is null we assume that this
    // is a new query
    const side = data.length === 0 ? 0 : compare(data[1], newSlice[1]);
    return side === 0 ? newSlice : side > 0 ? [...data, ...newSlice] : [...newSlice, ...data];
}

const initialState = {
    pageSize: 300,
    page: 0,
    startIdx: 0,
    pages: -1,
    filters: "",
    data: []
};

export default function CrimeHistory() {

    const [state, setState] = React.useState(initialState);

    const attributes = useSelector(state => state.searchFiltersSlice.attributes);
    const dynamicHeadings = Object.keys(attributes).map(attr => ({header: attr.toUpperCase(), accessor: attr}));
    const header = [fixedHeaderColumns, ...dynamicHeadings];

    React.useEffect(() => {

        const {pageSize, filters, data} = state;

        if (!filters)
            return;

        let page = state.page;

        let p = "?page=" + page + "&size=" + pageSize;
        p += (filters ? "&" + filters : "");

        axios.get("/api/incidents" + p).then(res => setState({
                ...state,
                data: createRows(res.data, data, page * pageSize),
                pages: res.data.totalPages,
            })
        )
    }, [state.filters, state.page]);  // only request data when the filters or page number have changed


    // called by the filter editor to get a new query
    function returnSelectedFilters(filters) {
        if (filters !== state.filters) {
            setState({...state, filters: filters, page: 0, data: [], startIdx: 0});
        }
    }

    // called by paging table to get a new page (still same query)
    function getSliceFromServer(direction) {
        const {data, startIdx, pageSize} = state;
        const count = data.length;

        const page = direction === "forward" ? Math.floor((startIdx + count) / pageSize)
            : Math.max(0, startIdx - pageSize) / pageSize;

        if (page !== state.page) {
            setState({...state, page: page})
        }
    }

    const {data, filters} = state;
    return (
        <GridContainer>
            <GridItem xs={12}>
                <Card>
                    <CardHeader>
                        <IncidentQueryForm returnSelectedFilters={returnSelectedFilters}/>
                    </CardHeader>
                    <CardBody>
                        <PagedTable
                            tableHeaderColor="primary"
                            tableHead={header}
                            tableData={data}
                            coloredColls={[3]}
                            colorsColls={["primary"]}
                            sliceRequest={getSliceFromServer}
                        />
                    </CardBody>
                    <CardFooter>
                        <Button href={"http://meanstreets.io/api/download/incidents?" + filters}>Export</Button>
                    </CardFooter>
                </Card>
            </GridItem>
        </GridContainer>
    );
}

