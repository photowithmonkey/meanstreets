/*eslint-disable*/
// @material-ui/core components
import {makeStyles} from "@material-ui/core/styles";
// @material-ui/icons
import Timeline from "@material-ui/icons/Timeline";
import React from "react";

import styles from "../../assets/jss/material-dashboard-pro-react/views/chartsStyle.js";
import CrimeDistributionBars from "../../chartdata/CrimeDistributionBars";
import CrimeDistributionPie from "../../chartdata/CrimeDistributionPie";
import CrimePropertyValuesLine from "../../chartdata/CrimePropertyValuesLine";
import PredictedCrimes from "../../chartdata/PredictedCrimes";
import ScatterBubbleChart from "../../chartdata/ScatterBubbleChart";

import Card from "../../components/Card/Card.js";
import CardBody from "../../components/Card/CardBody.js";
import CardHeader from "../../components/Card/CardHeader.js";
import CardIcon from "../../components/Card/CardIcon.js";
import GridContainer from "../../components/Grid/GridContainer.js";
import GridItem from "../../components/Grid/GridItem.js";
// core components
import Heading from "../../components/Heading/Heading.js";
// react plugin for creating charts

const useStyles = makeStyles(styles);

export default function Charts() {
    const classes = useStyles();
    return (
        <div>
            <Heading
                textAlign="center"
                title="Crime Forecasts"
                category={"More crime is predicted for next year. Find all about it right here."}
            />
            <GridContainer>
                <GridItem xs={12} sm={12} md={12} xl={12}>
                    <PredictedCrimes/>
                </GridItem>
            </GridContainer>
            <GridContainer>
                <GridItem xs={12} sm={12} md={7}>
                    <CrimeDistributionBars/>
                </GridItem>
                <GridItem xs={12} sm={12} md={5}>
                    <CrimeDistributionPie/>
                </GridItem>
            </GridContainer>
            <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="info" icon>
                            <CardIcon color="info">
                                <Timeline/>
                            </CardIcon>
                            <h4 className={classes.cardIconTitle}>
                                Crime Rates by Days of the Month
                            </h4>
                        </CardHeader>
                        <CardBody>
                            <ScatterBubbleChart/>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
            <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                    <CrimePropertyValuesLine/>
                </GridItem>
            </GridContainer>
        </div>
    );
}
