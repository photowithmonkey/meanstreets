/*eslint-disable*/
import React from "react";
// @material-ui/core components
import {makeStyles} from "@material-ui/core/styles";
// @material-ui/icons
// core components
import Heading from "../../components/Heading/Heading.js";

import styles from "../../assets/jss/material-dashboard-pro-react/views/chartsStyle.js";
import {GoogleMap, LoadScript, Marker} from "@react-google-maps/api";
import Box from "@material-ui/core/Box";
import axios from "axios";
import BubbleChart from "../../chartdata/BubbleChart";
import {darkMapMode} from "../Maps/utils";
// react plugin for creating charts

const useStyles = makeStyles(styles);

const useStyles2 = makeStyles({
    mapContainer: {
        position: "relative"
    },
    bubbleContainer: {
        position: "absolute",
        top: 10,
        left: 10
    }
});

const initState = {
    center: {lat: 40.7128, lng: -74.0060},
    data: [],
    resolution: 4,
    chartOpen: false,
    selected: null,
    apiLoaded: false
};


function toDataURL(svg) {
    return "data:image/svg+xml;base64," + window.btoa(svg);
};

export default function CrimeTypeByLoc() {

    const [state, setState] = React.useState(initState);

    React.useEffect(() => {
        axios.get("/api/map/locations?level=2").then(json => {
            setState({...state, data: json.data});
        })
    }, []);

    const handleMapClick = p => e => {
        if (p === null) {
            setState({...state, chartOpen: false});
        } else {
            axios.get("/api/map/incidents?lat=" + p.latitude + "&lng=" + p.longitude + "&level=2")
                .then(json => {
                    setState({...state, selected: json.data, chartOpen: true})
                })
        }
    };

    const classes = useStyles2();
    const {center, data} = state;

    return (
        <div>
            <Heading
                textAlign="center"
                title="Crime Forecasts"
                category={<span>More crime is predicted for next year. Find all about it right here.</span>}
            />
            <Box className={classes.mapContainer}>
                <LoadScript
                    id="script-loader"
                    googleMapsApiKey="AIzaSyCDcYd-3wqyBCkb_iQPxKswOkt0GkJNV8s"
                    onLoad={() => {
                        setState({...state, apiLoaded: true})
                    }}
                >
                    <GoogleMap
                        id="selector-map"
                        zoom={12}
                        center={center}
                        mapContainerStyle={{
                            height: "calc(100vh - 50px)",
                        }}
                        onClick={handleMapClick(null)}
                        options={{
                            scrollwheel: true,
                            streetViewControl: false,
                            mapTypeControl: false,
                            fullscreenControl: false,
                            zoomControl: true,
                            styles: darkMapMode
                        }
                        }
                    >
                        {
                            data.map((p, idx) =>
                                <Marker key={"marker-" + idx}
                                        position={{lat: p.latitude, lng: p.longitude}}
                                        icon="http://static.meanstreets.io/img/yellow-circle.png"
                                        title={"Incidents: " + p.cnt.toString(10)}
                                        onClick={handleMapClick(p)}
                                />
                            )
                        }
                    </GoogleMap>
                </LoadScript>
                {state.chartOpen && <div className={classes.bubbleContainer}>
                    <BubbleChart chartOpen={state.chartOpen} selected={state.selected}
                                 handleClose={() => setState({...state, chartOpen: false})}/>
                </div>}
            </Box>
        </div>
    );
}
