import {Checkbox, DialogActions, DialogContent, FormControlLabel} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import FormControl from "@material-ui/core/FormControl";
import withStyles from "@material-ui/core/styles/withStyles";
import TextField from "@material-ui/core/TextField";
import React from "react";

const styles = {
    root: {
        padding: 10
    },
    form: {
        '& .MuiFormControl-root': {
            margin: 5,
            minWidth: 200
        }
    },
    titleField:{
        width:500
    },
    checkboxStyle: {
        padding: 20,
        paddingLeft: 50
    },
    errorStyle: {
        paddingLeft: 20,
        paddingRight: 20,
        color: "red"
    }
};

class EventForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isChecked: false,
            title: "",
            startDate: "",
            startTime: "",
            endDate: "",
            endTime: "",
            color: "",
            titleError: "",
            checkboxError: ""
        };

        this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
        this.printInfo = this.printInfo.bind(this);
        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handleStartDateChange = this.handleStartDateChange.bind(this);
        this.handleStartTimeChange = this.handleStartTimeChange.bind(this);
        this.handleEndDateChange = this.handleEndDateChange.bind(this);
        this.handleEndTimeChange = this.handleEndTimeChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.validateForm = this.validateForm.bind(this);
        this.createEvent = this.createEvent.bind(this);
        this.returnEvent = this.returnEvent.bind(this);
        this.clearStates = this.clearStates.bind(this);
    }

    clearStates() {
        this.setState({
            isChecked: false,
            title: "",
            startDate: "",
            startTime: "",
            endDate: "",
            endTime: "",
            color: "",
            titleError: "",
            checkboxError: ""
        });
    }

    createEvent() {
        let startDateArr = this.state.startDate.split("-");
        let startY = startDateArr[0];
        let startM = startDateArr[1];
        let startD = startDateArr[2];
        let startTimeArr = this.state.startTime.split(":");
        let startH = startTimeArr[0];
        let startMin = startTimeArr[1];

        let endDateArr = this.state.endDate.split("-");
        let endY = endDateArr[0];
        let endM = endDateArr[1];
        let endD = endDateArr[2];
        let endTimeArr = this.state.endTime.split(":");
        let endH = endTimeArr[0];
        let endMin = endTimeArr[1];

        let event = {
            title: this.state.title,
            allDay: this.state.isChecked,
            startYear: startY,
            startMonth: startM,
            startDay: startD,
            startHour: startH,
            startMinute: startMin,
            endYear: endY,
            endMonth: endM,
            endDay: endD,
            endHour: endH,
            endMinute: endMin,
            color: this.state.color
        };
        return event;
    }

    handleCheckboxChange() {
        console.log("Changed");
        if (this.state.isChecked == true) {
            this.setState({isChecked: false, checkboxError: ""});
        } else {
            this.setState({isChecked: true, checkboxError: ""});
        }
    }

    handleColorChange(val) {
        this.setState({
            color: val
        })
    }

    handleEndDateChange(val) {
        this.setState({
            endDate: val,
            titleError: ""
        })
    }

    handleEndTimeChange(val) {
        this.setState({
            endTime: val,
            checkboxError: ""
        })
    }

    handleStartDateChange(val) {
        this.setState({
            startDate: val,
            endDate: val,
            titleError: ""
        });
    }

    handleStartTimeChange(val) {
        this.setState({
            startTime: val,
            endTime: val,
            checkboxError: ""
        })
    }

    handleTitleChange(val) {
        this.setState({
            title: val,
            titleError: ""
        });
    }

    printInfo() {
        console.log("New Event Object",
            "\nTitle: " + this.state.title,
            "\nStart Date: " + this.state.startDate + "T" + this.state.startTime,
            "\nEnd Date: " + this.state.endDate + "T" + this.state.endTime,
            "\nColor: " + this.state.color);
    }

    returnEvent() {
        // handle event color
        if (this.state.color === "") {
            this.setState({color: "default"});
        }
        // print new event information
        this.printInfo();
        // validate info from form
        let validate = this.validateForm();
        if (validate) {
            console.log("Form is valid");
            // create event to return to parent
            let e = this.createEvent();
            this.props.sendEvent(e);
            this.clearStates();
            this.props.onClose();
        } else {
            console.log("Form is invalid");
        }
    }

    validateForm() {
        let val = true;
        let errorMsg = "";
        this.setState({error: ""});
        if (this.state.title === "" || this.state.startDate === "" ||
            this.state.endDate === "") {
            val = false;
            this.setState({titleError: "Error: required fields are empty"});
        }

        if (this.state.isChecked === false &&
            this.state.startTime === "" && this.state.endTime === "") {
            val = false;
            this.setState({checkboxError: "Error: checkbox not checked and time field empty"});
        }
        return val;
    }

    render() {
        const {classes} = this.props;

        return this.props.open ? <Dialog open className={classes.root} maxWidth="md">
            <DialogTitle id="form-dialog-title">Add New Event</DialogTitle>
            <p className={classes.errorStyle}>{this.state.titleError}</p>
            <p className={classes.errorStyle}>{this.state.checkboxError}</p>
            <DialogContent>
                <form className={classes.form}>
                    <div>
                        <TextField
                            className={classes.titleField}
                            autoFocus
                            required
                            id="outlined-required"
                            label="Event Title"
                            value={this.state.title}
                            onChange={(e) => this.handleTitleChange(e.target.value)}
                            variant="outlined"
                        />
                    </div>
                    <div>
                        <FormControl>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        label="All Day Event"
                                        value="All Day Event"
                                        onChange={(e) => this.handleCheckboxChange(e.target.value)}
                                        checked={this.state.isChecked}
                                        className={classes.checkboxStyle}
                                    />
                                }
                                label="All Day Event"
                            />
                        </FormControl>
                    </div>
                    <div>
                        <TextField
                            required
                            id="date"
                            label="Start Date"
                            type="date"
                            value={this.state.startDate}
                            onChange={(e) => this.handleStartDateChange(e.target.value)}
                            // className={classes.textField}
                            variant="outlined"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                        <TextField
                            id="time"
                            label="Start Time"
                            type="time"
                            value={this.state.startTime}
                            onChange={(e) => this.handleStartTimeChange(e.target.value)}
                            variant="outlined"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            inputProps={{
                                step: 300, //5 min
                            }}
                        />
                    </div>
                    <div>
                        <TextField
                            required
                            id="date"
                            label="End Date"
                            type="date"
                            value={this.state.endDate}
                            onChange={(e) => this.handleEndDateChange(e.target.value)}
                            variant="outlined"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                        <TextField
                            id="time"
                            label="End Time"
                            type="time"
                            value={this.state.endTime}
                            onChange={(e) => this.handleEndTimeChange(e.target.value)}
                            variant="outlined"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            inputProps={{
                                step: 300, //5 min
                            }}
                        />
                    </div>
                    <div>
                        <TextField
                            id="outlined-required"
                            label="Color"
                            value={this.state.color}
                            onChange={(e) => this.handleColorChange(e.target.value)}
                            variant="outlined"
                        />
                    </div>
                </form>
            </DialogContent>
            <DialogActions>
                <Button onClick={this.props.onClose} color="primary">Cancel</Button>
                <Button onClick={this.returnEvent} color="primary">Add Event</Button>
            </DialogActions>
        </Dialog> : null;
    }
}

export default withStyles(styles)(EventForm);