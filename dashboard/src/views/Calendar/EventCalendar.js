/*eslin-disable*/
import {Fab} from "@material-ui/core";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import {AddCircle} from "@material-ui/icons";

import axios from "axios";
// dependency plugin for react-big-calendar
import moment from "moment";
import React, {Fragment} from "react";
// react components used to create a calendar with events on it
import {Calendar as BigCalendar, momentLocalizer} from "react-big-calendar";
// react component used to create alerts
import SweetAlert from "react-bootstrap-sweetalert";
import Card from "../../components/Card/Card.js";
import CardBody from "../../components/Card/CardBody.js";
import GridContainer from "../../components/Grid/GridContainer.js";
import GridItem from "../../components/Grid/GridItem.js";
// core components
import Heading from "../../components/Heading/Heading.js";

import {events as defaultEvents} from "../../variables/general.js";
import EventForm from "./EventForm";

const localizer = momentLocalizer(moment);

const styles = {
    root: {
        position: "relative"
    },
    fabStyling: {
        margin: 0,
        top: 35,
        right: 20,
        position: "absolute"
    },
    extendedIcon: {
        marginRight: 2
    }
};

class EventCalendar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            events: [],
            eventsFetched: false,
            link: "/api/events",
            databaseFetch: true,
            alert: null,
            isOpen: false,
        };

        this.addNewEventAlert = this.addNewEventAlert.bind(this);
        this.convertEvent = this.convertEvent.bind(this);
        this.loadEvents = this.loadEvents.bind(this);
        this.openEvent = this.openEvent.bind(this);
        this.setColor = this.setColor.bind(this);
        this.hideAlert = this.hideAlert.bind(this);
        this.closeForm = this.closeForm.bind(this);
        this.addNewEvent = this.addNewEvent.bind(this);
        this.addNewFormEvent = this.addNewFormEvent.bind(this);
        this.postNewEvent = this.postNewEvent.bind(this);
    }

    addNewEventAlert(slotInfo) {
        const newAlert = (
            <SweetAlert
                input
                showCancel
                allowEscape
                style={{display: "block", marginTop: "-100px"}}
                title="Enter New Event Information"
                onConfirm={e => this.addNewEvent(e, slotInfo)}
                OnCancel={this.hideAlert}
                confirmBtnText="Ok"
                // confirmBtnCssClass={useStyles().button + " " + useStyles().success}
                cancelBtnText="Cancel"
                // cancelBtnCssClass={useStyles().button + " " + useStyles().danger}
            />);
        this.setState({alert: newAlert});
    }

    addNewFormEvent(event) {
        var newEvents = this.state.events;
        let newEvent = this.convertEvent(event);
        console.log(event.title);
        newEvents.push({
            title: newEvent.title,
            start: newEvent.start,
            end: newEvent.end,
            color: newEvent.color
        });
        this.setState({
            events: newEvents,
            isOpen: false
        });
        this.postNewEvent(event);
    }

    addNewEvent(e, slotInfo) {
        var newEvents = this.state.events;
        newEvents.push({
            title: e,
            start: slotInfo.start,
            end: slotInfo.end,
            color: "default"
        });
        this.setState({events: newEvents, alert: null});
    }

    closeForm() {
        this.setState({isOpen: false});
        this.forceUpdate();
    }

    componentDidMount() {
        this.loadEvents();
    }

    componentWillMount() {
    }

    convertEvent(e) {
        let fullDay;
        if (e.allDay === true) {
            fullDay = true;
        } else {
            fullDay = false;
        }

        let event = {
            title: e.title,
            allDay: fullDay,
            start: new Date(e.startYear, e.startMonth - 1, e.startDay, e.startHour,
                e.startMinute),
            end: new Date(e.endYear, e.endMonth - 1, e.endDay, e.endHour, e.endMinute),
            color: e.color
        }
        return event;
    }

    hideAlert() {
        console.log("Hiding alert....");
        this.setState({alert: null});
    }

    loadEvents() {
        // decide if we are requesting events or using default events
        if (this.state.databaseFetch) {
            // fetch events from database
            axios.get(this.state.link)
                .then(response => {
                    const eventList = [];
                    response.data.forEach(element => {
                        eventList.push(this.convertEvent(element));
                    });
                    this.setState({
                        events: eventList,
                        eventsFetched: true
                    });
                }).catch(error => {
                console.log("Whoopsie!!", error.message);
            })
        } else {
            // set the events variable to the default events
            this.setState({
                events: defaultEvents,
                eventsFetched: true
            });
        }
    }

    openEvent(ev) {

        if (ev.allDay === false) {
            window.alert(ev.title + "\n"
                + ev.start.getHours() + ":" + ev.start.getMinutes() + " - "
                + ev.end.getHours() + ":" + ev.end.getMinutes());
        } else {
            window.alert(ev.title + "\n" + "All Day Event");
        }
    }

    postNewEvent(e) {
        let res = axios.post(this.state.link, e);
        console.log(res.data);
    }

    setColor(ev) {
        let backgroundColor = "event-";
        ev.color ? (backgroundColor += ev.color)
            : (backgroundColor += "default");
        return {className: backgroundColor};
    }


    render() {

        const classes = this.props.classes;

        return (
                <div className={classes.root}>
                    <Heading
                        textAlign="center"
                        title="Mean Streets Calendar"
                    />
                    <Fab
                        color="primary"
                        className={classes.fabStyling}
                        size="large"
                        variant="extended"
                        onClick={() => {
                            console.log("Floating Button Clicked");
                            this.setState({isOpen: true});
                        }}>
                        <AddCircle color="inherit" className={classes.extendedIcon}/>
                        Add New Event
                    </Fab>
                    <EventForm
                        open={this.state.isOpen}
                        sendEvent={this.addNewFormEvent}
                        onClose={() => this.closeForm()}
                        OnCancel={() => this.closeForm()}
                    />
                    {this.state.alert}
                    <GridContainer justify="center">
                        <GridItem xs={12} sm={12} md={10}>
                            <Card>
                                <CardBody calendar>
                                    <BigCalendar
                                        selectable
                                        localizer={localizer}
                                        events={this.state.events}
                                        defaultView="month"
                                        scrollToTime={new Date(1970, 1, 1, 6)}
                                        defaultDate={new Date()}
                                        onSelectEvent={event => this.openEvent(event)}
                                        // onSelectSlot={slotInfo => this.addNewEventAlert(slotInfo)}
                                        eventPropGetter={this.setColor}
                                    />
                                </CardBody>
                            </Card>
                        </GridItem>
                    </GridContainer>
                </div>
        );
    }
}

export default withStyles(styles)(EventCalendar);