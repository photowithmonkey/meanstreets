import React from "react";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Icon from "@material-ui/core/Icon";

// @material-ui/icons
import Timeline from "@material-ui/icons/Timeline";
import Code from "@material-ui/icons/Code";
import Group from "@material-ui/icons/Group";
import Face from "@material-ui/icons/Face";
import Email from "@material-ui/icons/Email";
// import LockOutline from "@material-ui/icons/LockOutline";
import Check from "@material-ui/icons/Check";

import styles from "assets/jss/material-dashboard-pro-react/views/registerPageStyle";
import WizardView from "../Forms/Wizard";


const useStyles = makeStyles(styles);

export default function RegisterPage() {

  const classes = useStyles();

  return (
    <div className={classes.container}>
      <WizardView/>
    </div>
  );
}
