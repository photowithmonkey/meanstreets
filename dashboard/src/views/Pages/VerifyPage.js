import React from "react";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";

// @material-ui/icons
import Face from "@material-ui/icons/Face";
//import Email from "@material-ui/icons/Email";
// import LockOutline from "@material-ui/icons/LockOutline";

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import CardFooter from "components/Card/CardFooter.js";

import styles from "assets/jss/material-dashboard-pro-react/views/loginPageStyle.js";
import { useHistory } from "react-router-dom";
import {Auth} from "aws-amplify";


const useStyles = makeStyles(styles);

export default function VerifyPage() {
  const [cardAnimaton, setCardAnimation] = React.useState("cardHidden");
  const [username, setUsername] = React.useState('');
  const [vCode, setVCode] = React.useState('');
  const [showError, setShowError] = React.useState(false);
  const [error, setError] = React.useState(false);
  const history = useHistory();

  setTimeout(function () {
    setCardAnimation("");
  }, 700);
  const classes = useStyles();

  function Verify() {
    Auth.confirmSignUp(username, vCode)
      .then(res => {
        console.log("Confirmed", res)
        history.push("/auth")
      })
      .catch(err => {
        setError(err);
        setShowError(true);
        console.log("Error", err)
      })
  }

  function Error() {
    return <h1>{error}</h1>
  }

  return (
    <div className={classes.container}>
      <GridContainer justify="center">
        <GridItem xs={12} sm={6} md={4}>
          <form>
            <Card login className={classes[cardAnimaton]}>
              <CardHeader
                className={`${classes.cardHeader} ${classes.textCenter}`}
                color="rose"
              >
               <h4 className={classes.cardTitle}>Please enter your verification code below to finish signing up.</h4>

              </CardHeader>
              <CardBody>
                <CustomInput
                  labelText="Username"
                  id="username"
                  formControlProps={{
                    fullWidth: true
                  }}
                  inputProps={{
                    onChange: event => {
                      setUsername(event.target.value);
                    },
                    endAdornment: (
                      <InputAdornment position="end">
                        <Face className={classes.inputAdornmentIcon} />
                      </InputAdornment>
                    )
                  }}
                />
                <CustomInput
                  labelText="Verification Code"
                  id="vCode"
                  formControlProps={{
                    fullWidth: true
                  }}
                  inputProps={{
                    onChange: event => {
                      setVCode(event.target.value);
                    },
                    endAdornment: (
                      <InputAdornment position="end">
                        <Icon className={classes.inputAdornmentIcon}>
                          lock_outline
                        </Icon>
                      </InputAdornment>
                    ),
                    type: "vCode",
                    autoComplete: "off"
                  }}
                />
                <h5>{showError && "Error Verifying Account, Please try again."}</h5>
              </CardBody>
              <CardFooter className={classes.justifyContentCenter}>
                <Button onClick = {Verify} color="rose" simple size="lg" block>
                  Verify Account
                </Button>
              </CardFooter>
            </Card>
          </form>
        </GridItem>
      </GridContainer>
    </div>
  );
}
