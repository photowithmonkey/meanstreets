import React from "react";
// @material-ui/core components
import {makeStyles} from "@material-ui/core/styles";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";

import styles from "../../../assets/jss/material-dashboard-pro-react/views/devPageStyle.js";
import CardContent from "@material-ui/core/CardContent";
import Card from "../../../components/Card/Card";

const useStyles = makeStyles(styles);

export default function DenisComp() {
    const [cardAnimation, setCardAnimation] = React.useState("cardHidden");

    setTimeout(function () {
        setCardAnimation("");
    }, 700);
    const classes = useStyles();

    return (
        <div className={classes.container}>
            <GridContainer justify="center">
                <GridItem xs={12} sm={6} md={4}>
                    <Card>
                        <CardContent>
                            <p>
                                I have been programmed computers since I was little. My first program was about sharks
                                eating fish. It was where the sharks ate almost
                                all the fish and then almost died, so then the fish came back, which again led to more
                                sharks...
                            </p>
                        </CardContent>
                    </Card>
                    <Card>
                        <CardContent>
                            <p>
                                I grew up and matured mostly in progressive, friendly, environmental and peace loving
                                Ann Arbor,
                                Michigan. Now I am in Baltimore, which does not succumb to simple cliches.
                            </p>
                        </CardContent>
                    </Card>
                </GridItem>
            </GridContainer>
        </div>
    )
};