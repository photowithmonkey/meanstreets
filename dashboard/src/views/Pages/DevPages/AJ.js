import React from "react";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import CardFooter from "components/Card/CardFooter.js";
import arsenalPic from "assets/img/arsenal.jpg";
import lincolnPic from "assets/img/lincoln.jpg";

import styles from "assets/jss/material-dashboard-pro-react/views/devPageStyle.js";
import { CardContent, CardMedia, CardActionArea } from "@material-ui/core";
const useStyles = makeStyles(styles);

export default function AJComp() {
    const [cardAnimation, setCardAnimation] = React.useState("cardHidden");

    setTimeout(function () {
        setCardAnimation("");
    }, 700);
    const classes = useStyles();

    return (
        <div className={classes.container}>
            <GridContainer justify="center">
                <GridItem xs={12} sm={6} md={4}>
                    <Card >
                        <CardContent>
                            <CardHeader color="blue">
                                <h2>AJ Conklin</h2>
                            </CardHeader>
                            <CardBody>
                                <h5>
                                    I am originally from North Augusta, South Carolina. I grew up on the Georgia/Carolina border.
                                    I graduated from Virginia State University in Petersburg, VA. Petersburg is a
                                    town about 30 miles south of Richmond. I currently live in State College, PA.
                                </h5>
                            </CardBody>
                        </CardContent>
                    </Card>
                </GridItem>
                <GridItem xs={12} sm={6} md={4}>
                    <Card >
                        <CardHeader>
                            <h2>Fan of Arsenal!</h2>
                        </CardHeader>
                        <CardMedia
                            style={{height: 200}}
                            image={arsenalPic}
                            title="Arsenal FC"
                        />
                        <CardContent>
                        </CardContent>
                    </Card>
                </GridItem>
                <GridItem xs={12} sm={6} md={4}>
                    <Card >
                        <CardHeader>
                            <h2>Fun Fact!</h2>
                        </CardHeader>
                        <CardMedia
                            style={{height: 200}}
                            image={lincolnPic}
                            title="Lincoln"
                        />
                        <CardContent>
                            <CardBody>
                                <p>
                                    I was an extra in the Lincoln film. I got to
                                    see both David Oyelowo and Daniel Day Lewis in person.
                                    I also shook Steven Spielberg's hand.
                                </p>
                            </CardBody>
                        </CardContent>
                    </Card>
                </GridItem>
            </GridContainer>            
        </div>
    )
};