import React from "react";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import CardFooter from "components/Card/CardFooter.js";

import styles from "assets/jss/material-dashboard-pro-react/views/devPageStyle.js";
import { CardContent } from "@material-ui/core";
const useStyles = makeStyles(styles);

export default function MeanStreetsComp() {
    const [cardAnimation, setCardAnimation] = React.useState("cardHidden");

    setTimeout(function () {
        setCardAnimation("");
    }, 700);
    const classes = useStyles();

    return (
        <div className={classes.container}>
            <GridContainer justify="center">
                <GridItem xs={12} sm={6} md={4}>
                    <Card >
                        <CardContent>
                            <CardHeader color="blue">
                                <h2>Mean Streets</h2>
                            </CardHeader>
                            <CardBody>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    Proin nibh nisl condimentum id venenatis a condimentum vitae sapien. 
                                    Tellus rutrum tellus pellentesque eu tincidunt. 
                                </p>
                            </CardBody>
                        </CardContent>
                    </Card>
                </GridItem>
            </GridContainer>            
        </div>
    )
};