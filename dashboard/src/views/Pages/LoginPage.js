import React from "react";
// @material-ui/core components
import {makeStyles} from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Face from "@material-ui/icons/Face";
// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import CardFooter from "components/Card/CardFooter.js";

import styles from "../../assets/jss/material-dashboard-pro-react/views/loginPageStyle.js";
//import {auth} from "../../components/PrivateRoute";
import {useHistory} from "react-router-dom";
import {Auth} from "aws-amplify";
// import LockOutline from "@material-ui/icons/LockOutline";


const useStyles = makeStyles(styles);

export default function LoginPage() {
    const [cardAnimaton, setCardAnimation] = React.useState("cardHidden");
    const [username, setUsername] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [showMFA, setShowMFA] = React.useState(false);
    const [showLogin, setShowLogin] = React.useState(true);
    const [showError, setShowError] = React.useState(false);
    const [error, setError] = React.useState('');
    const [authCode, setAuthCode] = React.useState('');
    const [user, setUser] = React.useState('');
    const history = useHistory();

    setTimeout(function () {
        setCardAnimation("");
    }, 700);
    const classes = useStyles();


    /*  function handleSubmit(e) {
        console.log(username);
        console.log(password);
        auth.authenticate();
        history.push("/admin");
      }*/
    function handleLogin(e) {
        Auth.signIn(username, password)
            .then(user => {
                if (user.challengeName === "SMS_MFA") {
                    setShowLogin(false);
                    setShowMFA(true);
                    console.log("MFA Challenged: ", user);
                    setUser(user);
                } else {
                    history.push("/admin")
                }
            })
            .catch(err => console.log("Signin Error:", err))
    }

    function handleMfa(e) {
        console.log("User:", e);
        Auth.confirmSignIn(user, authCode)
            .then(r => {
                history.push("/admin");
            }).catch(
            err => {
                setError(err);
                setShowError(true);
                console.log("Signin Error:", err)
            })
    }


    return (
        <div className={classes.container}>
            <GridContainer justify="center">
                <GridItem xs={12} sm={6} md={4}>
                    <form>
                        {showLogin && <Card login className={classes[cardAnimaton]}>
                            <CardHeader
                                className={`${classes.cardHeader} ${classes.textCenter}`}
                                color="rose"
                            >
                                <h4 className={classes.cardTitle}>Log in</h4>
                                {//TODO Add social login
                                    /*<div className={classes.socialLine}>
                                    {[
                                      "fab fa-facebook-square",
                                      "fab fa-twitter",
                                      "fab fa-google-plus"
                                    ].map((prop, key) => {
                                      return (
                                        <Button
                                          color="transparent"
                                          justIcon
                                          key={key}
                                          className={classes.customButtonClass}
                                        >
                                          <i className={prop} />
                                        </Button>
                                      );
                                    })}
                                  </div>*/}
                            </CardHeader>
                            <CardBody>
                                <CustomInput
                                    labelText="Username"
                                    id="username"
                                    formControlProps={{
                                        fullWidth: true
                                    }}
                                    inputProps={{
                                        onChange: event => {
                                            setUsername(event.target.value);
                                        },
                                        endAdornment: (
                                            <InputAdornment position="end">
                                                <Face className={classes.inputAdornmentIcon}/>
                                            </InputAdornment>
                                        )
                                    }}
                                />
                                <CustomInput
                                    labelText="Password"
                                    id="password"
                                    formControlProps={{
                                        fullWidth: true
                                    }}
                                    inputProps={{
                                        onChange: event => {
                                            setPassword(event.target.value);
                                        },
                                        endAdornment: (
                                            <InputAdornment position="end">
                                                <Icon className={classes.inputAdornmentIcon}>
                                                    lock_outline
                                                </Icon>
                                            </InputAdornment>
                                        ),
                                        type: "password",
                                        autoComplete: "off"
                                    }}
                                />
                            </CardBody>
                            <CardFooter className={classes.justifyContentCenter}>
                                <Button onClick={handleLogin} color="rose" simple size="lg" block>
                                    Let{"'"}s Go
                                </Button>
                            </CardFooter>
                        </Card>}
                        {showMFA && <Card login className={classes[cardAnimaton]}>
                            <CardHeader
                                className={`${classes.cardHeader} ${classes.textCenter}`}
                                color="rose"
                            >
                                <h4 className={classes.cardTitle}>Enter Verification Code</h4>
                            </CardHeader>
                            <CardBody>
                                <CustomInput
                                    labelText="verification code"
                                    id="authCode"
                                    formControlProps={{
                                        fullWidth: true
                                    }}
                                    inputProps={{
                                        onChange: event => {
                                            setAuthCode(event.target.value);
                                        },
                                        endAdornment: (
                                            <InputAdornment position="end">
                                                <Icon className={classes.inputAdornmentIcon}>
                                                    lock_outline
                                                </Icon>
                                            </InputAdornment>
                                        ),
                                        type: "authCode",
                                        autoComplete: "off"
                                    }}
                                />
                                <h5>{showError && "Error Verifying Account, Please try again."}</h5>
                            </CardBody>

                            <CardFooter className={classes.justifyContentCenter}>
                                <Button onClick={handleMfa} color="rose" simple size="lg" block>
                                    Submit
                                </Button>
                            </CardFooter>
                        </Card>
                        }
                    </form>
                </GridItem>
            </GridContainer>
        </div>
    );
}
