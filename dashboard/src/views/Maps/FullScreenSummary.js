import Button from "@material-ui/core/Button";
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Slider from '@material-ui/core/Slider';
import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import {GoogleMap, HeatmapLayer, LoadScript,} from "@react-google-maps/api";
import axios from "axios";
import React, {Component} from "react";
import {darkMapMode, findCenter} from "./utils";

const libraries = ["visualization"];

const useStylesSlider = makeStyles(theme => ({
    root: {
        //width: 850,
        width: '100%',
    },
    margin: {
        height: theme.spacing(3),
    },
}));


const marksYears = [
    {
        value: 2012,
        label: '2012',
    },
    {
        value: 2013,
        label: '2013',
    },
    {
        value: 2014,
        label: '2014',
    },
    {
        value: 2015,
        label: '2015',
    },
    {
        value: 2016,
        label: '2016',
    },
    {
        value: 2017,
        label: '2017',
    },
    {
        value: 2018,
        label: '2018',
    },
];

const marksMonths = [
    {
        value: 1,
        label: '1',
    },
    {
        value: 2,
        label: '2',
    },
    {
        value: 3,
        label: '3',
    },
    {
        value: 4,
        label: '4',
    },
    {
        value: 5,
        label: '5',
    },
    {
        value: 6,
        label: '6',
    },
    {
        value: 7,
        label: '7',
    },
    {
        value: 8,
        label: '8',
    },
    {
        value: 9,
        label: '9',
    },
    {
        value: 10,
        label: '10',
    },
    {
        value: 11,
        label: '11',
    },
    {
        value: 12,
        label: '12',
    },
];

function valuetext(value) {
    return `${value}°C`;
}

const useStylesTable = makeStyles(theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing(3),
        overflowX: 'auto',
    },
    table: {
        minWidth: 650,
    },
}));

function createData(name, current_year_range1, current_year_range2, current_month_range1, current_month_range2, reported_crimes) {
    return {
        name,
        current_year_range1,
        current_year_range2,
        current_month_range1,
        current_month_range2,
        reported_crimes
    };
}

const useStyles = makeStyles({
    root: {
        width: '100%',
        flexGrow: 1,
    },
});

export function calcTop10zips(data, slicefrom, sliceto) {
    const myzipcodes = data.map(z => z.zipcode);
    //********************************************************************//
    // Find top 10 zipcodes with highest occurrences //
    let countszip = myzipcodes.reduce((map, zipcode) => {
        map[zipcode] = (map[zipcode] || 0) + 1;
        return map;
    }, {});

    let sortedzip = Object.keys(countszip).sort((a, b) => countszip[b] - countszip[a]);
    let top10zip = sortedzip.slice(slicefrom, sliceto);
    //********************************************************************//
    return top10zip;
}

export function calculateReported(data, zip) {
    let counter = 0;
    for (let i = 0; i < data.length; i++) {
        if (zip == data[i].zipcode) {
            counter++;
        }
    }
    return counter;
}


function calcNewZoom() {
    return 13;
}

function calcNewRadius() {
    return 55;
}

class FullScreenHeatMapPred extends Component {
    constructor(props) {
        super(props);
        this.state = {
            center: {lat: 40.7128, lng: -74.0060},
            zoom: [],
            raduis: [],
            opacity: [],
            gradient: null,
            apiLoaded: false,
            toggled: false,
            data: [],
            prevdata: [],
            incidents: [],
            heat_points: [],
            updated_data: [],
            zip_code: [],
            reported_crimes: [],
            current_year_range1: [],
            current_year_range2: [],
            current_month_range1: [],
            current_month_range2: [],
            curr_year1: [],
            curr_year2: [],
            curr_month1: [],
            curr_month2: []
        };
        this.sameYear = this.sameYear.bind(this);
        this.differentYear = this.differentYear.bind(this);
        this.toggleHeatmap = this.toggleHeatmap.bind(this);
        this.changeGradient = this.changeGradient.bind(this);
        this.changeRadius = this.changeRadius.bind(this);
        this.changeOpacity = this.changeOpacity.bind(this);
        this.centerMap = this.centerMap.bind(this);
        this.AccessibleTable = this.AccessibleTable.bind(this);
        this.mySliderYear = this.mySliderYear.bind(this);
        this.mySliderMonth = this.mySliderMonth.bind(this);
        this.year_1_setter = this.year_1_setter.bind(this);
        this.year_1_getter = this.year_1_getter.bind(this);
        this.year_2_setter = this.year_2_setter.bind(this);
        this.year_2_getter = this.year_2_getter.bind(this);
        this.month_1_setter = this.month_1_setter.bind(this);
        this.month_1_getter = this.month_1_getter.bind(this);
        this.month_2_setter = this.month_2_setter.bind(this);
        this.month_2_getter = this.month_2_getter.bind(this);
    }

    year_1_setter(year1) {
        const {current_year_range1, curr_year1} = this.state;
        this.setState({current_year_range1: year1, curr_year1: year1});
    }

    year_1_getter() {
        const {curr_year1} = this.state;
        return curr_year1;
    }

    year_2_setter(year2) {
        const {current_year_range2, curr_year2} = this.state;
        this.setState({current_year_range2: year2, curr_year2: year2});
    }

    year_2_getter() {
        const {curr_year2} = this.state;
        return curr_year2;
    }

    month_1_setter(month1) {
        const {current_month_range1, curr_month1} = this.state;
        this.setState({current_month_range1: month1, curr_month1: month1});
    }

    month_1_getter() {
        const {curr_month1} = this.state;
        return curr_month1;
    }

    month_2_setter(month2) {
        const {current_month_range2, curr_month2} = this.state;
        this.setState({current_month_range2: month2, curr_month2: month2});
    }

    month_2_getter() {
        const {curr_month2} = this.state;
        return curr_month2;
    }

    changeGradient() {
        const {apiLoaded, gradient} = this.state;
        var mygradient = [
            'rgba(0, 255, 255, 0)',
            'rgba(0, 255, 255, 1)',
            'rgba(0, 191, 255, 1)',
            'rgba(0, 127, 255, 1)',
            'rgba(0, 63, 255, 1)',
            'rgba(0, 0, 255, 1)',
            'rgba(0, 0, 223, 1)',
            'rgba(0, 0, 191, 1)',
            'rgba(0, 0, 159, 1)',
            'rgba(0, 0, 127, 1)',
            'rgba(63, 0, 91, 1)',
            'rgba(127, 0, 63, 1)',
            'rgba(191, 0, 31, 1)',
            'rgba(255, 0, 0, 1)'
        ];

        if (gradient == null) {
            this.setState({gradient: mygradient});
        } else {
            this.setState({gradient: null});
        }
    }

    centerMap() {
        const {apiLoaded, updated_data} = this.state;
        const mycenter = apiLoaded ? updated_data : [];
        const new_center = findCenter(mycenter);
        this.setState({center: new_center});
    }

    toggleHeatmap() {
        const {apiLoaded, heat_points, prevdata} = this.state;
        const points = apiLoaded ? heat_points : [];
        if (points.length > 0) {
            this.setState({heat_points: []});
        } else {
            this.setState({heat_points: prevdata});
        }
    }

    changeRadius() {
        const {apiLoaded, radius} = this.state;
        const myrad = apiLoaded ? radius : [];
        if (myrad == 5) {
            this.setState({radius: 10});
        } else if (myrad == 10) {
            this.setState({radius: 15});
        } else {
            this.setState({radius: 5});
        }
    }

    changeOpacity() {
        const {apiLoaded, opacity} = this.state;
        const myopacity = apiLoaded ? opacity : [];
        if (myopacity == 0.6) {
            this.setState({opacity: 0.9});
        } else if (myopacity == 0.9) {
            this.setState({opacity: 0.2});
        } else {
            this.setState({opacity: 0.6});
        }
    }

    sameYear() {
        const {data, center, heat_points, current_year_range1, current_year_range2, current_month_range1, current_month_range2, reported_crimes} = this.state;
        var current_month1 = this.month_1_getter();
        var current_month2 = this.month_2_getter();
        //console.log("Parsing data for same years");
        //console.log("Before - heat_points.length: "+heat_points.length);

        const points = [];
        for (let i = 0; i < data.length; i++) {
            for (var j = current_month1; j <= current_month2; j++) {
                if (data[i].month == j) {
                    if (data[i].year == current_year_range1) {
                        points.push(new window.google.maps.LatLng(data[i].latitude, data[i].longitude));
                    }
                }
            }
        }
        const reportedCrimes = points.length;
        this.setState({heat_points: points, reported_crimes: reportedCrimes})
        //console.log("After - points.length and new heat_points.length: "+points.length);
    }

    differentYear() {
        const {data, center, heat_points, current_year_range1, current_year_range2, current_month_range1, current_month_range2, reported_crimes} = this.state;

        var current_month1 = this.month_1_getter();
        var current_month2 = this.month_2_getter();
        var current_year1 = this.year_1_getter();
        var current_year2 = this.year_2_getter();
        //console.log("Before - heat_points.length: "+heat_points.length);

        const points = [];
        for (let i = 0; i < data.length; i++) {
            for (var j = current_year1; j <= current_year2; j++) {
                for (var k = current_month1; k <= current_month2; k++) {
                    if ((data[i].year == j) && (data[i].month == k)) {
                        points.push(new window.google.maps.LatLng(data[i].latitude, data[i].longitude));
                    }
                }
            }
        }
        const reportedCrimes = points.length;
        this.setState({heat_points: points, reported_crimes: reportedCrimes});
        //console.log("After - points.length and new heat_points.length: "+points.length);
    }

    componentDidMount() {
        //console.log("inside the componentDidMount function now");
        axios.get("/api/map/typeLocResults").then(json => {
                //console.log("(json.data).length: "+(json.data).length);
                const center = findCenter(json.data);
                const points = [];
                for (let i = 0; i < (json.data).length; i++) {
                    if ((json.data[i].year == 2016 && json.data[i].month == 1) || (json.data[i].year == 2016 && json.data[i].month == 2)
                        || (json.data[i].year == 2017 && json.data[i].month == 1) || (json.data[i].year == 2017 && json.data[i].month == 2)) {
                        points.push(new window.google.maps.LatLng(json.data[i].latitude, json.data[i].longitude));
                    }
                }
                const reportedCrimes = points.length;
                const myzoom = 11;
                const myradius = 5;
                const myopacity = 0.6;
                const year_range1 = 2016;
                const year_range2 = 2017;
                const month_range1 = 1;
                const month_range2 = 2;
                this.setState({
                    data: json.data,
                    curr_month1: month_range1,
                    curr_month2: month_range2,
                    curr_year1: year_range1,
                    curr_year2: year_range2,
                    current_year_range1: year_range1,
                    current_year_range2: year_range2,
                    current_month_range1: month_range1,
                    current_month_range2: month_range2,
                    heat_points: points,
                    center: center,
                    reported_crimes: reportedCrimes,
                    zoom: myzoom,
                    radius: myradius,
                    opacity: myopacity,
                    toggled: false
                });
            }
        )
    }

    AccessibleTable() {
        const {current_year_range1, current_year_range2, current_month_range1, current_month_range2, reported_crimes} = this.state;
        const self = this;
        const rows = [
            createData('Crime Data Range', current_year_range1, current_year_range2, current_month_range1, current_month_range2, reported_crimes),
        ];

        const classes = useStylesTable();
        return (
            <Paper className={classes.root}>
                <Table className={classes.table} aria-label="caption table">
                    <caption>Please view data for 12 months or less at a time for accurate heatmap</caption>
                    <TableHead>
                        <TableRow>
                            <TableCell>Ranges</TableCell>
                            <TableCell align="right">Year Range</TableCell>
                            <TableCell align="right">Month Range</TableCell>
                            <TableCell align="right">Reported Crime Number</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map(row => (
                            <TableRow key={row.name}>
                                <TableCell component="th" scope="row">
                                    {row.name}
                                </TableCell>
                                <TableCell
                                    align="right">{row.current_year_range1 + " - "}{row.current_year_range2}</TableCell>
                                <TableCell
                                    align="right">{row.current_month_range1 + " - "}{row.current_month_range2}</TableCell>
                                <TableCell align="right">{row.reported_crimes}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </Paper>
        );
    }

    mySliderYear() {
        const self = this;
        const {data, center, heat_points, current_year_range1, current_year_range2, current_month_range1, current_month_range2, zoom, radius, opacity, gradient} = this.state;
        const classes = useStylesSlider();
        const [value, setValue] = React.useState([2016, 2017]);

        const handleChange = (event, newValue) => {
            //console.log("value changed. newValue: "+newValue);
            setValue(newValue);

            if (newValue[0] == newValue[1]) {
                this.setState({current_year_range1: newValue[0], current_year_range2: newValue[1]});
                this.year_1_setter(newValue[0]);
                this.year_2_setter(newValue[1]);
                this.sameYear()
            } else if (newValue[0] != newValue[1]) {
                this.setState({current_year_range1: newValue[0], current_year_range2: newValue[1]});
                this.year_1_setter(newValue[0]);
                this.year_2_setter(newValue[1]);
                this.differentYear()
            } else {
                console.log("Invalid Year Selection")
            }
        };

        return (
            <div className={classes.root}>
                <Slider
                    value={value}
                    onChange={handleChange}
                    valueLabelDisplay="auto"
                    aria-labelledby="range-slider"
                    getAriaValueText={valuetext}
                    min={2012}
                    max={2018}
                    marks={marksYears}
                />
            </div>
        );
    }

    mySliderMonth() {
        const {current_year_range1, current_year_range2, current_month_range1, current_month_range2} = this.state;
        const classes = useStylesSlider();
        const [value, setValue] = React.useState([1, 2]);

        const handleChange = (event, newValue) => {
            //console.log("value changed. newValue[1]: "+newValue[1]);
            var current_year1 = this.year_1_getter();
            var current_year2 = this.year_2_getter();

            setValue(newValue);
            this.month_1_setter(newValue[0]);
            this.month_2_setter(newValue[1]);

            if (current_year1 == current_year2) {
                this.sameYear()
            } else if (current_year1 != current_year2) {
                this.differentYear()
            } else {
                console.log("Invalid Year Selection")
            }
        };

        return (
            <div className={classes.root}>
                <Slider
                    value={value}
                    onChange={handleChange}
                    valueLabelDisplay="auto"
                    aria-labelledby="range-slider"
                    getAriaValueText={valuetext}
                    min={1}
                    max={12}
                    marks={marksMonths}
                />
            </div>
        );
    }

    render() {
        const {center, heat_points, zoom, radius, opacity, gradient} = this.state;
        const self = this;
        //const heatPoints = apiLoaded ? data.map(v => new window.google.maps.LatLng(v.latitude, v.longitude)) : [];
        //console.log("heat_points.length: "+heat_points.length);

        return (
            <div>
                <this.mySliderYear/>
                <this.mySliderMonth/>
                <this.AccessibleTable/>
                <div>
                    <h5></h5>
                </div>
                <Grid container spacing={7}>
                    <Grid item xs={12} md={12}>
                        <ButtonGroup fullWidth aria-label="full width outlined button group">
                            <Button onClick={this.toggleHeatmap}>Toggle Heatmap</Button>
                            <Button onClick={this.changeGradient}>Change gradient</Button>
                            <Button onClick={this.changeRadius}>Change radius</Button>
                            <Button onClick={this.changeOpacity}>Change opacity</Button>
                            <Button onClick={this.centerMap}>Center Map</Button>
                        </ButtonGroup>
                    </Grid>
                </Grid>

                <LoadScript googleMapsApiKey="AIzaSyCDcYd-3wqyBCkb_iQPxKswOkt0GkJNV8s" libraries={libraries}
                            onLoad={() => {
                                // self.setState({heatPoints: getHeatPoints(incidents)})
                                self.setState({apiLoaded: true})
                            }}
                >
                    <GoogleMap zoom={parseInt(zoom)} center={center}
                               mapContainerStyle={{height: "100vh", width: "100%"}}
                               options={{
                                   scrollwheel: false,
                                   zoomControl: true,
                                   styles:darkMapMode,
                               }}>
                        <HeatmapLayer data={heat_points}
                                      options={{radius: radius,
                                          opacity: opacity,
                                          gradient: gradient
                                      }}/>
                    </GoogleMap>
                </LoadScript>

            </div>
        );
    }
}

export default FullScreenHeatMapPred;