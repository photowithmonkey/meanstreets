import React from "react";
import {GoogleMap, InfoWindow, LoadScript, Marker} from "@react-google-maps/api";
import axios from "axios";
import IncidentQueryForm from "../Forms/IncidentQueryForm";
import {darkMapMode, filterIncidents, findCenter} from "./utils";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import {useSelector} from "react-redux";


const iconMap =
    {
        iconUrls: [
            {
                key: ['AGG. ASSAULT'],
                url: "http://static.meanstreets.io/img/assault.png",
            },
            {
                key: ['ARSON'],
                url: "http://static.meanstreets.io/img/arson.png",
            },
            {
                key: ['COMMON ASSAULT', 'OFFENSES AGAINST THE PERSON', 'ASSAULT 3 & RELATED OFFENSES', 'FELONY ASSAULT',
                    'OFFENSES AGAINST PUBLIC SAFETY', 'KIDNAPPING & RELATED OFFENSES', 'KIDNAPPING', 'OFFENSES AGAINST THE ADMINI'],
                url: "http://static.meanstreets.io/img/battery.png",
            },
            {
                key: ['LARCENY', 'THEFT-FRAUD', 'POSSESSION OF STOLEN PROPERTY', 'THEFT OF SERVICES', 'OFFENSES INVOLVING FRAUD',
                    'GRAND LARCENY', "BURGLAR'S TOOLS", 'BURGLARY'],
                url: "http://static.meanstreets.io/img/breakin.png",
            },
            {
                key: ['LARCENY FROM AUTO', 'VEHICLE AND TRAFFIC LAWS', 'UNAUTHORIZED USE OF A VEHICLE', 'PETIT LARCENY OF MOTOR VEHICLE',
                    'PETIT LARCENY', 'GRAND LARCENY OF MOTOR VEHICLE', 'ROBBERY - CARJACKING', 'AUTO THEFT'],
                url: "http://static.meanstreets.io/img/carbreakin.png",
            },
            {
                key: ['RAPE', 'PROSTITUTION & RELATED OFFENSES', 'SEX CRIMES'],
                url: "http://static.meanstreets.io/img/wifebattery.png",
            },
            {
                key: ['ROBBERY - RESIDENCE', 'ROBBERY - COMMERCIAL', 'ROBBERY', 'ROBBERY - STREET'],
                url: "http://static.meanstreets.io/img/pursesnatching.png",
            },
            {
                key: ['SHOOTING', 'UNLAWFUL POSS. WEAP. ON SCHOOL', 'MURDER & NON_NEGL. MANSLAUGHTER', 'MISCELLANEOUS PENAL LAW',
                    'DANGEROUS WEAPONS', 'HOMICIDE'],
                url: "http://static.meanstreets.io/img/armedrobbery.png",
            },
            {
                regEx: /HARRASSMENT|MISCHIEF|SENSBLTY/,
                url: "http://static.meanstreets.io/img/stopthat.png"
            }
        ],
        default: {
            key: "default",
            url: "http://static.meanstreets.io/img/handcuffs.png",
        }
    };


const useStyle = makeStyles({
    card: {
        marginBottom: 0,
        marginTop: 0,
        boxShadow: 0
    }
});


function getIncidentIcon(incident) {
    const searchStr = incident.offense || incident.description;
    const item = iconMap.iconUrls
            .find(iconDef => {
                    let match = false;
                    // try searching directly for matching string
                    if (iconDef.key) {
                        match = iconDef.key.indexOf(searchStr) >= 0
                    }
                    // try a regex match if available
                    if (!match && iconDef.regEx) {
                        match = searchStr.match(iconDef.regEx) !== null;
                    }
                    return match;
                }
            )
        || iconMap.default;

    return {
        url: item.url,
        anchor: new window.google.maps.Point(16, 16),
        size: new window.google.maps.Size(32, 32),
        scaledSize: new window.google.maps.Size(32, 32),
    }
}

function CrimeMarker(props) {

    const {incident, setSelected} = props;

    const icon = getIncidentIcon(incident);

    return <Marker
        position={{lat: incident.latitude, lng: incident.longitude}}
        title={incident.offense || incident.description}
        icon={icon}
        onClick={e => {
            setSelected(e, incident);
        }}
    />
}

function CrimeInfoWindow(props) {

    const {incident, attributes, ...rest} = props;

    const classes = useStyle();

    return <InfoWindow {...rest}>
        <Card className={classes.card}>
            <CardHeader
                title={incident.description}
            />
            <CardContent>
                <List>
                    {
                        Object.keys(attributes).map(attr => <ListItem>
                            <ListItemText>{attr.toUpperCase() + ": " + incident[attr]}</ListItemText>
                        </ListItem>)
                    }
                </List>
            </CardContent>
        </Card>
    </InfoWindow>
};


function FullScreenMap() {

    const classes = useStyle();

    const attributes = useSelector(state => state.searchFiltersSlice.attributes);

    const [state, setState] = React.useState(
        {
            incidents: [],
            incident: null,
            center: {lat: 30, lng: 30},
            selected: false
        }
    )

    function fetchMapData(params) {
        let p = "/api/incidents?page=0&size=500";
        p += params ? "&" + params : "";
        axios.get(p).then(js => {
            const incidents = filterIncidents(js.data.content);
            const center = findCenter(incidents);
            setState({...state, incidents: incidents, center: center})
        });
    }

    function handleSelected(e, incident) {
        setState({
            ...state,
            selected: e,
            incident: incident
        })
    }


    const {incidents, selected, incident, center} = state;
    return (
        <div>
            <IncidentQueryForm returnSelectedFilters={fetchMapData}/>
            <LoadScript
                id="script-loader"
                googleMapsApiKey="AIzaSyCDcYd-3wqyBCkb_iQPxKswOkt0GkJNV8s">
                <GoogleMap
                    id="map-w-markers"
                    zoom={13}
                    center={center}
                    mapContainerStyle={{
                        height: "100vh",
                        width: "100%"
                    }}
                    options={{
                        scrollwheel: true,
                        zoomControl: true,
                        clickableIcons:false,
                        styles: darkMapMode
                    }
                    }
                >
                    {
                        incidents.map((i, key) => {
                            return <CrimeMarker
                                incident={i}
                                key={"incident-" + key}
                                setSelected={handleSelected}
                            />
                        })
                    }

                    {selected && <CrimeInfoWindow
                        incident={incident}
                        attributes={attributes}
                        onCloseClick={() => setState({...state, selected: false})}
                        position={selected.latLng}
                    />}
                </GoogleMap>
            </LoadScript>
        </div>
    );
}

export default FullScreenMap;
