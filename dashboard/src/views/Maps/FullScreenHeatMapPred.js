import Button from "@material-ui/core/Button";
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import {makeStyles} from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Tabs from '@material-ui/core/Tabs';
import Typography from '@material-ui/core/Typography';
import {GoogleMap, HeatmapLayer, LoadScript,} from "@react-google-maps/api";
import axios from "axios";
import React, {Component} from "react";
import {darkMapMode, findCenter} from "./utils";

const libraries = ["visualization"];
const useStylesTable = makeStyles(theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing(3),
        overflowX: 'auto',
    },
    table: {
        minWidth: 650,
    },
}));

function createData(name, zipcodes, boro_name, expected_crime) {
    return {name, zipcodes, boro_name, expected_crime};
}

const useStyles = makeStyles({
    root: {
        width: '100%',
        flexGrow: 1,
    },
});

export function calcTop10zips(data, slicefrom, sliceto) {
    const myzipcodes = data.map(z => z.zipcode);
    //********************************************************************//
    // Find top 10 zipcodes with highest occurrences //
    let countszip = myzipcodes.reduce((map, zipcode) => {
        map[zipcode] = (map[zipcode] || 0) + 1;
        return map;
    }, {});

    let sortedzip = Object.keys(countszip).sort((a, b) => countszip[b] - countszip[a]);
    let top10zip = sortedzip.slice(slicefrom, sliceto);
    //********************************************************************//
    return top10zip;
}

export function calculateExpected(data, zip) {
    let counter = 0;
    for (let i = 0; i < data.length; i++) {
        if (zip == data[i].zipcode) {
            counter++;
        }
    }
    return counter;
}

export function calcTop10boros(data, zip) {
    const myboro = [];
    for (let i = 0; i < data.length; i++) {
        if (zip == data[i].zipcode) {
            myboro.push(data[i].boro_name);
            break
        }
    }

    return myboro;
}


function calcTop10heatpoints(apiLoaded, data, top10zip) {
    const top10zip_heatpoints = [];
    for (let i = 0; i < data.length; i++) {
        for (let j = 0; j < top10zip.length; j++) {
            if (top10zip[j] == data[i].zipcode) {
                top10zip_heatpoints.push(new window.google.maps.LatLng(data[i].latitude, data[i].longitude));
            }
        }
    }
    return top10zip_heatpoints;
}

function calcNewData(apiLoaded, data, top10zip) {
    const newdata = [];
    for (let i = 0; i < data.length; i++) {
        for (let j = 0; j < top10zip.length; j++) {
            if (top10zip[j] == data[i].zipcode) {
                newdata.push(data[i]);
            }
        }
    }
    return newdata;
}

function calcNewZoom() {
    return 13;
}

function calcNewRadius() {
    return 55;
}

class FullScreenHeatMapPred extends Component {
    constructor(props) {
        super(props);
        this.state = {
            center: {lat: 40.7128, lng: -74.0060},
            zoom: [],
            raduis: [],
            opacity: [],
            gradient: null,
            apiLoaded: false,
            toggled: false,
            data: [],
            prevdata: [],
            incidents: [],
            heat_points: [],
            updated_data: [],
            zip_code: [],
            boro_name: [],
            expected_crimes: [],
        };
        this.top1 = this.top1.bind(this);
        this.top2 = this.top2.bind(this);
        this.top3 = this.top3.bind(this);
        this.top4 = this.top4.bind(this);
        this.top5 = this.top5.bind(this);
        this.top6 = this.top6.bind(this);
        this.top7 = this.top7.bind(this);
        this.top8 = this.top8.bind(this);
        this.top9 = this.top9.bind(this);
        this.top10 = this.top10.bind(this);
        this.allData = this.allData.bind(this);
        this.toggleHeatmap = this.toggleHeatmap.bind(this);
        this.changeGradient = this.changeGradient.bind(this);
        this.changeRadius = this.changeRadius.bind(this);
        this.changeOpacity = this.changeOpacity.bind(this);
        this.centerMap = this.centerMap.bind(this);
        this.CenteredTabsMenu = this.CenteredTabsMenu.bind(this);
        this.AcccessibleTable = this.AcccessibleTable.bind(this);
    }

    changeGradient() {
        const {apiLoaded, gradient} = this.state;
        var mygradient = [
            'rgba(0, 255, 255, 0)',
            'rgba(0, 255, 255, 1)',
            'rgba(0, 191, 255, 1)',
            'rgba(0, 127, 255, 1)',
            'rgba(0, 63, 255, 1)',
            'rgba(0, 0, 255, 1)',
            'rgba(0, 0, 223, 1)',
            'rgba(0, 0, 191, 1)',
            'rgba(0, 0, 159, 1)',
            'rgba(0, 0, 127, 1)',
            'rgba(63, 0, 91, 1)',
            'rgba(127, 0, 63, 1)',
            'rgba(191, 0, 31, 1)',
            'rgba(255, 0, 0, 1)'
        ];

        if (gradient == null) {
            this.setState({gradient: mygradient});
        } else {
            this.setState({gradient: null});
        }
    }

    centerMap() {
        const {apiLoaded, updated_data} = this.state;
        const mycenter = apiLoaded ? updated_data : [];
        const new_center = findCenter(mycenter);
        this.setState({center: new_center});
    }

    toggleHeatmap() {
        const {apiLoaded, heat_points, prevdata} = this.state;
        const points = apiLoaded ? heat_points : [];
        if (points.length > 0) {
            this.setState({heat_points: []});
        } else {
            this.setState({heat_points: prevdata});
        }
    }

    changeRadius() {
        const {apiLoaded, radius} = this.state;
        const myrad = apiLoaded ? radius : [];
        if (myrad == 25) {
            this.setState({radius: 40});
        } else if (myrad == 40) {
            this.setState({radius: 55});
        } else {
            this.setState({radius: 25});
        }
    }

    changeOpacity() {
        const {apiLoaded, opacity} = this.state;
        const myopacity = apiLoaded ? opacity : [];
        if (myopacity == 0.6) {
            this.setState({opacity: 0.9});
        } else if (myopacity == 0.9) {
            this.setState({opacity: 0.2});
        } else {
            this.setState({opacity: 0.6});
        }
    }

    top1() {
        const {apiLoaded, data} = this.state;
        const top10zips = calcTop10zips(data, 0, 1);
        const top10boros = calcTop10boros(data, top10zips);
        const expected = calculateExpected(data, top10zips);
        const top10heatpoints = calcTop10heatpoints(apiLoaded, data, top10zips);
        const new_data = calcNewData(apiLoaded, data, top10zips);
        const new_center = findCenter(new_data);
        const new_zoom = calcNewZoom();
        const new_radius = calcNewRadius();
        this.setState({
            heat_points: top10heatpoints,
            updated_data: new_data,
            prevdata: top10heatpoints,
            zip_code: top10zips,
            boro_name: top10boros,
            expected_crimes: expected,
            center: new_center,
            zoom: new_zoom,
            radius: new_radius
        });
    }

    top2() {
        const {apiLoaded, data} = this.state;
        const top10zips = calcTop10zips(data, 1, 2);
        const top10boros = calcTop10boros(data, top10zips);
        const expected = calculateExpected(data, top10zips);
        const top10heatpoints = calcTop10heatpoints(apiLoaded, data, top10zips);
        const new_data = calcNewData(apiLoaded, data, top10zips);
        const new_center = findCenter(new_data);
        const new_zoom = calcNewZoom();
        const new_radius = calcNewRadius();
        this.setState({
            heat_points: top10heatpoints,
            updated_data: new_data,
            prevdata: top10heatpoints,
            zip_code: top10zips,
            boro_name: top10boros,
            expected_crimes: expected,
            center: new_center,
            zoom: new_zoom,
            radius: new_radius
        });
    }

    top3() {
        const {apiLoaded, data} = this.state;
        const top10zips = calcTop10zips(data, 2, 3);
        const top10boros = calcTop10boros(data, top10zips);
        const expected = calculateExpected(data, top10zips);
        const top10heatpoints = calcTop10heatpoints(apiLoaded, data, top10zips);
        const new_data = calcNewData(apiLoaded, data, top10zips);
        const new_center = findCenter(new_data);
        const new_zoom = calcNewZoom();
        const new_radius = calcNewRadius();
        this.setState({
            heat_points: top10heatpoints,
            updated_data: new_data,
            prevdata: top10heatpoints,
            zip_code: top10zips,
            boro_name: top10boros,
            expected_crimes: expected,
            center: new_center,
            zoom: new_zoom,
            radius: new_radius
        });
    }

    top4() {
        const {apiLoaded, data} = this.state;
        const top10zips = calcTop10zips(data, 3, 4);
        const top10boros = calcTop10boros(data, top10zips);
        const expected = calculateExpected(data, top10zips);
        const top10heatpoints = calcTop10heatpoints(apiLoaded, data, top10zips);
        const new_data = calcNewData(apiLoaded, data, top10zips);
        const new_center = findCenter(new_data);
        const new_zoom = calcNewZoom();
        const new_radius = calcNewRadius();
        this.setState({
            heat_points: top10heatpoints,
            updated_data: new_data,
            prevdata: top10heatpoints,
            zip_code: top10zips,
            boro_name: top10boros,
            expected_crimes: expected,
            center: new_center,
            zoom: new_zoom,
            radius: new_radius
        });
    }

    top5() {
        const {apiLoaded, data} = this.state;
        const top10zips = calcTop10zips(data, 4, 5);
        const top10boros = calcTop10boros(data, top10zips);
        const expected = calculateExpected(data, top10zips);
        const top10heatpoints = calcTop10heatpoints(apiLoaded, data, top10zips);
        const new_data = calcNewData(apiLoaded, data, top10zips);
        const new_center = findCenter(new_data);
        const new_zoom = calcNewZoom();
        const new_radius = calcNewRadius();
        this.setState({
            heat_points: top10heatpoints,
            updated_data: new_data,
            prevdata: top10heatpoints,
            zip_code: top10zips,
            boro_name: top10boros,
            expected_crimes: expected,
            center: new_center,
            zoom: new_zoom,
            radius: new_radius
        });
    }

    top6() {
        const {apiLoaded, data} = this.state;
        const top10zips = calcTop10zips(data, 5, 6);
        const top10boros = calcTop10boros(data, top10zips);
        const expected = calculateExpected(data, top10zips);
        const top10heatpoints = calcTop10heatpoints(apiLoaded, data, top10zips);
        const new_data = calcNewData(apiLoaded, data, top10zips);
        const new_center = findCenter(new_data);
        const new_zoom = calcNewZoom();
        const new_radius = calcNewRadius();
        this.setState({
            heat_points: top10heatpoints,
            updated_data: new_data,
            prevdata: top10heatpoints,
            zip_code: top10zips,
            boro_name: top10boros,
            expected_crimes: expected,
            center: new_center,
            zoom: new_zoom,
            radius: new_radius
        });
    }

    top7() {
        const {apiLoaded, data} = this.state;
        const top10zips = calcTop10zips(data, 6, 7);
        const top10boros = calcTop10boros(data, top10zips);
        const expected = calculateExpected(data, top10zips);
        const top10heatpoints = calcTop10heatpoints(apiLoaded, data, top10zips);
        const new_data = calcNewData(apiLoaded, data, top10zips);
        const new_center = findCenter(new_data);
        const new_zoom = calcNewZoom();
        const new_radius = calcNewRadius();
        this.setState({
            heat_points: top10heatpoints,
            updated_data: new_data,
            prevdata: top10heatpoints,
            zip_code: top10zips,
            boro_name: top10boros,
            expected_crimes: expected,
            center: new_center,
            zoom: new_zoom,
            radius: new_radius
        });
    }

    top8() {
        const {apiLoaded, data, center, heat_points} = this.state;
        const top10zips = calcTop10zips(data, 7, 8);
        const top10boros = calcTop10boros(data, top10zips);
        const expected = calculateExpected(data, top10zips);
        const top10heatpoints = calcTop10heatpoints(apiLoaded, data, top10zips);
        const new_data = calcNewData(apiLoaded, data, top10zips);
        const new_center = findCenter(new_data);
        const new_zoom = calcNewZoom();
        const new_radius = calcNewRadius();
        this.setState({
            heat_points: top10heatpoints,
            updated_data: new_data,
            prevdata: top10heatpoints,
            zip_code: top10zips,
            boro_name: top10boros,
            expected_crimes: expected,
            center: new_center,
            zoom: new_zoom,
            radius: new_radius
        });
    }

    top9() {
        const {apiLoaded, data} = this.state;
        const top10zips = calcTop10zips(data, 8, 9);
        const top10boros = calcTop10boros(data, top10zips);
        const expected = calculateExpected(data, top10zips);
        const top10heatpoints = calcTop10heatpoints(apiLoaded, data, top10zips);
        const new_data = calcNewData(apiLoaded, data, top10zips);
        const new_center = findCenter(new_data);
        const new_zoom = calcNewZoom();
        const new_radius = calcNewRadius();
        this.setState({
            heat_points: top10heatpoints,
            updated_data: new_data,
            prevdata: top10heatpoints,
            zip_code: top10zips,
            boro_name: top10boros,
            expected_crimes: expected,
            center: new_center,
            zoom: new_zoom,
            radius: new_radius
        });
    }

    top10() {
        const {apiLoaded, data} = this.state;
        const top10zips = calcTop10zips(data, 9, 10);
        const top10boros = calcTop10boros(data, top10zips);
        const expected = calculateExpected(data, top10zips);
        const top10heatpoints = calcTop10heatpoints(apiLoaded, data, top10zips);
        const new_data = calcNewData(apiLoaded, data, top10zips);
        const new_center = findCenter(new_data);
        const new_zoom = calcNewZoom();
        const new_radius = calcNewRadius();
        this.setState({
            heat_points: top10heatpoints,
            updated_data: new_data,
            prevdata: top10heatpoints,
            zip_code: top10zips,
            boro_name: top10boros,
            expected_crimes: expected,
            center: new_center,
            zoom: new_zoom,
            radius: new_radius
        });
    }

    allData() {
        const {apiLoaded, data} = this.state;
        const top10zips = calcTop10zips(data, 0, 10);
        const points = [];
        const new_data = [];
        const center = findCenter(data);
        for (let i = 0; i < (data).length; i++) {
            points.push(new window.google.maps.LatLng(data[i].latitude, data[i].longitude));
            new_data.push(data[i]);
        }
        this.setState({
            heat_points: points,
            updated_data: new_data,
            prevdata: points,
            zip_code: "",
            boro_name: "",
            expected_crimes: "",
            zoom: 11,
            radius: 25,
            center: center
        });
    }

    componentDidMount() {
        axios.get("/api/map/typeLoc").then(json => {
                const center = findCenter(json.data);
                const points = [];
                for (let i = 0; i < (json.data).length; i++) {
                    points.push(new window.google.maps.LatLng(json.data[i].latitude, json.data[i].longitude));
                }
                const myzoom = 11;
                const myradius = 25;
                const myopacity = 0.6;
                //const zipcodes = calcTop10zips(json.data, 0, 10);
                this.setState({
                    data: json.data,
                    updated_data: json.data,
                    prevdata: points,
                    center: center,
                    heat_points: points,
                    zip_code: "",
                    boro_name: "",
                    expected_crimes: "",
                    zoom: myzoom,
                    radius: myradius,
                    opacity: myopacity,
                    toggled: false
                });
                //console.log(json.data);
            }
        )
    }

    AcccessibleTable() {
        const {data, center, heat_points, zip_code, boro_name, expected_crimes, zoom, radius, opacity, gradient} = this.state;
        const self = this;
        const rows = [
            createData('Crime Zone:', zip_code, boro_name, expected_crimes),
        ];

        const classes = useStylesTable();
        return (
            <Paper className={classes.root}>
                <Table className={classes.table} aria-label="caption table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Location</TableCell>
                            <TableCell align="right">Zip Codes</TableCell>
                            <TableCell align="right">Boro Name</TableCell>
                            <TableCell align="right">Expected Crime Number</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map(row => (
                            <TableRow key={row.name}>
                                <TableCell component="th" scope="row">
                                    {row.name}
                                </TableCell>
                                <TableCell align="right">{row.zipcodes}</TableCell>
                                <TableCell align="right">{row.boro_name}</TableCell>
                                <TableCell align="right">{row.expected_crime}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </Paper>
        );
    }

    CenteredTabsMenu() {
        const {data, center, heat_points, zip_code, boro_name, expected_crimes, zoom, radius, opacity, gradient} = this.state;
        const self = this;
        const classes = useStyles();
        const [value, setValue] = React.useState(0);

        const handleChange = (event, newValue) => {
            setValue(newValue);
            if (newValue == 0) {
                this.allData()
            } else if (newValue == 1) {
                this.top1()
            } else if (newValue == 2) {
                this.top2()
            } else if (newValue == 3) {
                this.top3()
            } else if (newValue == 4) {
                this.top4()
            } else if (newValue == 5) {
                this.top5()
            } else if (newValue == 6) {
                this.top6()
            } else if (newValue == 7) {
                this.top7()
            } else if (newValue == 8) {
                this.top8()
            } else if (newValue == 9) {
                this.top9()
            } else if (newValue == 10) {
                this.top10()
            } else {
                console.log("invalid value");
            }
        };

        return (
            <Paper className={classes.root}>
                <Typography variant="h5" display="block" align={'center'} gutterBottom>
                    2019 TOP 10 PREDICTED CRIME ZONES
                </Typography>
                <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="scrollable"
                    scrollButtons="auto"
                    aria-label="scrollable auto tabs example"
                >
                    <Tab label="All Data"/>
                    <Tab label="1"/>
                    <Tab label="2"/>
                    <Tab label="3"/>
                    <Tab label="4"/>
                    <Tab label="5"/>
                    <Tab label="6"/>
                    <Tab label="7"/>
                    <Tab label="8"/>
                    <Tab label="9"/>
                    <Tab label="10"/>
                </Tabs>
            </Paper>
        );
    }

    render() {
        const {data, center, heat_points, zip_code, boro_name, expected_crimes, zoom, radius, opacity, gradient} = this.state;
        const self = this;

        //<FullScreenHeatMapPredMenu returnSelected={this.fetchMapPredData}/>
        //<StickyHead data={data}/>
        //
        return (
            <div>
                <this.CenteredTabsMenu/>
                <this.AcccessibleTable/>
                <div>
                    <h5></h5>
                </div>

                <Grid container spacing={7}>
                    <Grid item xs={12} md={12}>
                        <ButtonGroup fullWidth aria-label="full width outlined button group">
                            <Button onClick={this.toggleHeatmap}>Toggle Heatmap</Button>
                            <Button onClick={this.changeGradient}>Change gradient</Button>
                            <Button onClick={this.changeRadius}>Change radius</Button>
                            <Button onClick={this.changeOpacity}>Change opacity</Button>
                            <Button onClick={this.centerMap}>Center Map</Button>
                        </ButtonGroup>
                    </Grid>
                </Grid>

                <LoadScript googleMapsApiKey="AIzaSyCDcYd-3wqyBCkb_iQPxKswOkt0GkJNV8s" libraries={libraries}
                            onLoad={() => {
                                // self.setState({heatPoints: getHeatPoints(incidents)})
                                self.setState({apiLoaded: true})
                            }}
                >
                    <GoogleMap zoom={parseInt(zoom)} center={center}
                               mapContainerStyle={{height: "100vh", width: "100%"}}
                               options={{
                                   scrollwheel: false,
                                   zoomControl: true,
                                   styles:darkMapMode,
                               }}>
                        <HeatmapLayer data={heat_points}
                                      options={{radius: radius, opacity: opacity, gradient: gradient}}/>
                    </GoogleMap>
                </LoadScript>

            </div>
        );
    }
}

export default FullScreenHeatMapPred;