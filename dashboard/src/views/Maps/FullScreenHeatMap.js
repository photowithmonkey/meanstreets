import React, {Fragment} from "react";
import {GoogleMap, HeatmapLayer, LoadScript,} from "@react-google-maps/api";
import axios from "axios";
import IncidentQueryForm from "../Forms/IncidentQueryForm";
import {withLastLocation} from "react-router-last-location";
import {darkMapMode, filterIncidents, findCenter} from "./utils";

const libraries = ["visualization"];

class FullScreenHeatMap extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            incidents: [],
            heatPoints: [],
            apiLoaded: false,
            center: {lat: 30, lng: 30}
        };
        this.fetchMapData = this.fetchMapData.bind(this);
    }

    fetchMapData(params) {
        let p = "/api/incidents?page=0&size=500";
        p += params ? "&" + params : "";
        axios.get(p).then(js => {
            const incidents = filterIncidents(js.data.content);
            const center = findCenter(incidents);
            this.setState({incidents: incidents, center: center})
        });
    }


    render() {
        const {center, incidents, apiLoaded} = this.state;
        const heatPoints = apiLoaded ? incidents.map(v => new window.google.maps.LatLng(v.latitude, v.longitude)) : [];
        const self = this;
        return (
            <Fragment>
                <IncidentQueryForm returnSelectedFilters={this.fetchMapData}/>
                <LoadScript googleMapsApiKey="AIzaSyCDcYd-3wqyBCkb_iQPxKswOkt0GkJNV8s"
                            libraries={libraries}
                            onLoad={() => {
                                // self.setState({heatPoints: getHeatPoints(incidents)})
                                self.setState({apiLoaded: true})
                            }}
                >
                    <GoogleMap
                        zoom={13}
                        center={center}
                        mapContainerStyle={{
                            height: "100vh",
                            width: "100%"
                        }}
                        options={{
                            scrollwheel: false,
                            zoomControl: true,
                            styles:darkMapMode
                        }}>

                        <HeatmapLayer
                            data={heatPoints}
                            options={{radius: 60, opacity: 0.5}}
                        />
                    </GoogleMap>
                </LoadScript>
            </Fragment>
        );
    }
}

export default withLastLocation(FullScreenHeatMap);