import L from "leaflet";
import React from 'react';
import {GridLayer, withLeaflet} from 'react-leaflet';
import CustomStyles from "./CustomStyles";
import './leaflet.bing';


class BingLayer extends GridLayer {
    // static propTypes = {
    //   bingkey: PropTypes.string.isRequired
    // };

    createLeafletElement(props) {

        const propsN = props.customStyle ? {...props, style: CustomStyles(props.customStyle)} : props;

        return L.bingLayer(props.bingkey, this.getOptions(propsN));
    }
}

export default withLeaflet(BingLayer);