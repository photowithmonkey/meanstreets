import replace from "lodash/replace";

const urlParams = name => ({
    adminDistrict: "ad",
    adminDistrictCapital: "adc",
    airport: "ap",
    area: "ar",
    arterialRoad: "ard",
    building: "bld",
    business: "bs",
    capital: "cp",
    cemetery: "cm",
    continent: "ct",
    controlledAccessHighway: "cah",
    countryRegion: "cr",
    countryRegionCapital: "crc",
    district: "ds",
    education: "ed",
    educationBuilding: "eb",
    foodPoint: "fp",
    forest: "fr",
    golfCourse: "gc",
    highSpeedRamp: "hsrp",
    highway: "hg",
    indigenousPeoplesReserve: "ipr",
    island: "is",
    majorRoad: "mr",
    mapElement: "me",
    medical: "md",
    medicalBuilding: "mb",
    military: "ima",
    naturalPoint: "np",
    nautical: "nt",
    neighborhood: "nh",
    park: "pr",
    peak: "pk",
    playingField: "pf",
    point: "pt",
    pointOfInterest: "poi",
    political: "pl",
    populatedPlace: "pp",
    railway: "rl",
    ramp: "rm",
    reserve: "rsv",
    river: "rv",
    road: "rd",
    roadExit: "re",
    runway: "rw",
    sand: "sn",
    shoppingCenter: "sct",
    stadium: "st",
    street: "st",
    structure: "str",
    tollRoad: "tr",
    trail: "tr",
    transit: "tr",
    transitBuilding: "tb",
    transportation: "trs",
    unpavedStreet: "us",
    vegetation: "vg",
    volcanicPeak: "vp",
    water: "wt",
    waterPoint: "wp",
    waterRoute: "wr",

    fillColor: "fc",
    labelColor: "lbc",
    labelOutlineColor: "loc",
    labelVisible: "lv",
    strokeColor: "sc",
    visible: "v",
    borderOutlineColor: "boc",
    borderStrokeColor: "bsc",
    borderVisible: "bv",
    landColor: "lc"
}[name] || name);

function convertStyle(styleJson) {

    function filterFields(field) {
        return replace(field, "#", '');
    }

    function buildLine(fields, key) {
        const line = Object.keys(fields).map(key2 =>
            urlParams(key2) + ":" + filterFields(fields[key2]));

        return urlParams(key) + "|" + line.join(";")
    }

    const {elements, settings} = styleJson;
    const el = Object.keys(styleJson.elements).map(key => {
        const l = buildLine(elements[key], key);
        return l;
    });

    const st = buildLine(settings, "global");
    el.push(st);
    return el.join("_");
}


export default function CustomStyles(style) {
    return convertStyle(style);
}