import {makeStyles} from "@material-ui/core";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Slider from "@material-ui/core/Slider";
import withStyles from "@material-ui/core/styles/withStyles";
import axios from "axios";
import debounce from "lodash/debounce";
import moment from "moment";
import React, {useEffect, useState} from "react";
import {LayersControl, Map} from "react-leaflet";
import HeatmapLayer from "react-leaflet-heatmap-layer/lib/HeatmapLayer";
import Card from "../../components/Card/Card";
import CardBody from "../../components/Card/CardBody";
import CustomDropdown from "../../components/CustomDropdown/CustomDropdown";
import BingLayer from "./bing/BingLayer";
import BroodingDark from "./bing/styles/BroodingDark";

const {BaseLayer, Overlay} = LayersControl;

const useStyles = makeStyles(theme => ({
    leafletContainer: {
        height: "calc(100vh - 325px)",
        width: "auto"
    },
    dateRangeSlider: {
        paddingTop: theme.spacing(4),
        paddingLeft: 15,
        paddingRight: 15,
    },
    formLabel: {
        marginLeft: 0
    }
}));

const initState = {
    crimeType: "felony",
    monthIdx: 1,
    maxMonth: 153,
    incidents: [],
    heatPoints: [],
    apiLoaded: false,
    viewport: {
        center: [40.503, -74.243],
        zoom: 11
    }
};

const BING_KEY = 'AjyM0Q0OU808HYdk0pgSYg7mpnOsteT8YQ2dNoq_QNXVe9XQYJH6BjvlQJNO-ZN_';

const iOSBoxShadow =
    '0 3px 1px rgba(0,0,0,0.1),0 4px 8px rgba(0,0,0,0.13),0 0 0 1px rgba(0,0,0,0.02)';

const DateRangeSlider = withStyles({
    root: {
        color: '#3880ff',
        height: 2,
        padding: '15px 0',
    },
    thumb: {
        height: 28,
        width: 28,
        backgroundColor: '#fff',
        boxShadow: iOSBoxShadow,
        marginTop: -14,
        marginLeft: -14,
        '&:focus,&:hover,&$active': {
            boxShadow: '0 3px 1px rgba(0,0,0,0.1),0 4px 8px rgba(0,0,0,0.3),0 0 0 1px rgba(0,0,0,0.02)',
            // Reset on touch devices, it doesn't add specificity
            '@media (hover: none)': {
                boxShadow: iOSBoxShadow,
            },
        },
    },
    active: {},
    valueLabel: {
        left: 'calc(-50% + 11px)',
        top: -22,
        '& *': {
            background: 'transparent',
            color: '#000',
            textAlign: "center"
        },
    },
    track: {
        height: 2,
    },
    rail: {
        height: 2,
        opacity: 0.5,
        backgroundColor: '#bfbfbf',
    },
    mark: {
        backgroundColor: '#bfbfbf',
        height: 8,
        width: 1,
        marginTop: -3,
    },
    markActive: {
        opacity: 1,
        backgroundColor: 'currentColor',
    },
})(Slider);


export default function CrimeOverTimeLeaflet() {

    const [state, setState] = useState(initState);

    useEffect(() => {
        let url = "/api/map/timeline/" + state.crimeType + "/" + state.monthIdx;
        axios.get(url).then(({data}) => {
            setState({...state, incidents: data})
        });
    }, [state.crimeType, state.monthIdx]);

    const handleDateChange = debounce(_handleDateChange, 1000);

    function _handleDateChange(e, monthIdx) {
        setState({...state, monthIdx: monthIdx})
    }

    function valueText(mIdx) {
        const d = moment("2006-01-01").add(mIdx, 'M');
        return d.format("MMM YYYY").toUpperCase();
    }

    function handleViewportChanged(viewport) {
        setState({...state, viewport: viewport})
    }

    const {crimeType, viewport, monthIdx, incidents} = state;
    const classes = useStyles();

    return <div>
        <Card>
            <CardBody>
                <FormControlLabel
                    className={classes.formLabel}
                    control={
                        <CustomDropdown
                            buttonText={crimeType}
                            buttonProps={{
                                color: "info"
                            }}
                            dropdownList={["Felony", "Misdemeanor", "Violation"]}
                            onClick={s => setState({...state, crimeType: s.toLowerCase()})}
                        />
                    }
                    labelPlacement="start"
                    label="Crime Type&nbsp;"
                />
                <div className={classes.dateRangeSlider}>
                    <DateRangeSlider
                        min={1} max={state.maxMonth}
                        value={monthIdx}
                        valueLabelFormat={valueText}
                        valueLabelDisplay="on"
                        onChange={handleDateChange}/>

                </div>
            </CardBody>
        </Card>
        <div>
            <Map className={classes.leafletContainer}
                 onViewportChanged={handleViewportChanged}
                 viewport={viewport}
            >
                <LayersControl position="topright">
                    <BaseLayer checked name="Street Map">
                        <BingLayer
                            bingkey={BING_KEY}
                            type="CanvasDark"
                            customStyle={BroodingDark}
                        />
                    </BaseLayer>
                    <BaseLayer name="Arial View">
                        <BingLayer
                            type="AerialWithLabels"
                            bingkey={BING_KEY}
                        />
                    </BaseLayer>
                    <Overlay name="Heatmap" checked>
                        <HeatmapLayer
                            // fitBoundsOnLoad
                            // fitBoundsOnUpdate

                            maxZoom={7}
                            minOpacity={0.01}
                            points={incidents}
                            latitudeExtractor={m => m[0]}
                            longitudeExtractor={m => m[1]}
                            intensityExtractor={m => m[2]}/>
                    </Overlay>

                </LayersControl>
            </Map>
        </div>
    </div>
}

