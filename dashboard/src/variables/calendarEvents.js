import axios from "axios";

function convertEventObject(e) {

    let fullDay;
    if(e.allDay === true) {
        fullDay = true;
    } else {
        fullDay = false;
    }

    let event = {
        title: e.title,
        allDay: fullDay,
        start: new Date(e.startYear, e.startMonth, e.startDay, e.startHour, 
            e.startMinute),
        end: new Date(e.endYear, e.endMonth, e.endDay, e.endHour, e.endMinute),
        color: "default"
    }
    
    return event;
}

function getEvents() {
    let eventLink = "/api/events";
    return axios.get(eventLink);
}

function fetchCalendarEvents() {
    let response = getEvents();
    console.log(response.data);
    const rawEvents = [];
    response.data.forEach(elem => {
        rawEvents.push(convertEventObject(elem));
    });
    console.log(rawEvents);
    console.log("here goes");
    return rawEvents;
        
    }).catch(response => {
        console.log(response);
    })
    console.log("made it");
}

export const events = () => {
    fetchCalendarEvents();
}