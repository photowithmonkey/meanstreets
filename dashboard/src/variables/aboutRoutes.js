import AJComp from "views/Pages/DevPages/AJ.js";
import DenisComp from "views/Pages/DevPages/Denis.js";
import EminComp from "views/Pages/DevPages/Emin.js";
import JustinComp from "views/Pages/DevPages/Justin.js";
import PaulComp from "views/Pages/DevPages/Paul.js";
import MeanStreetsComp from "views/Pages/DevPages/MeanStreets";

var aboutRoutes = [
    {
        path: "/crimeapp",
        name: "Mean Streets",
        mini: "MS",
        component: MeanStreetsComp,
        layout: "/about"
    },
    {
        path: "/aj",
        name: "AJ",
        mini: "AJ",
        component: AJComp,
        layout: "/about"
    },
    {
        path: "/denis",
        name: "Denis",
        mini: "DD",
        component: DenisComp,
        layout: "/about"
    },
    {
        path: "/emin",
        name: "Emin",
        mini: "EO",
        component: EminComp,
        layout: "/about"
    },
    {
        path: "/justin",
        name: "Justin",
        mini: "JS",
        component: JustinComp,
        layout: "/about"
    },
    {
        path: "/paul",
        name: "Paul",
        mini: "PS",
        component: PaulComp,
        layout: "/about"
    }
];
export default aboutRoutes;