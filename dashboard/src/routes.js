import {AccountBalance, CalendarToday, Casino, PieChart, TableChart} from "@material-ui/icons";
// @material-ui/icons
import DashboardIcon from "@material-ui/icons/Dashboard";
import MapIcon from "@material-ui/icons/Map";
import EventCalendar from "views/Calendar/EventCalendar";
import Charts from "views/Charts/Charts";
import Dashboard from "views/Dashboard/Dashboard.js";
import FullScreenHeatMap from "views/Maps/FullScreenHeatMap.js";
import FullScreenHeatMapPred from "views/Maps/FullScreenHeatMapPred.js";
import FullScreenMap from "views/Maps/FullScreenMap.js";
import FullScreenSummary from "views/Maps/FullScreenSummary.js";
import CrimeHistoryTable from "views/Tables/CrimeHistory";
import CrimeTypeByLoc from "./views/Charts/CrimeTypeByLoc";
import CrimeOverTimeLeaflet from "./views/Maps/CrimeOverTimeLeaflet";

var dashRoutes = [
    {
        path: "/dashboard",
        name: "Dashboard",
        icon: DashboardIcon,
        component: Dashboard,
        layout: "/admin"
    },
    {
        collapse: true,
        name: "Data",
        icon: TableChart,
        state: "dataCollapse",
        views: [
            {
                path: "/table-extended",
                name: "Crime History",
                mini: "VC",
                component: CrimeHistoryTable,
                layout: "/admin"
            },
        ]
    },
    {
        path: "/calendar",
        name: "Event Calendar",
        mini: "EC",
        icon: CalendarToday,
        component: EventCalendar,
        layout: "/admin"
    },
    {
        collapse: true,
        name: "Graphs & Charts",
        icon: PieChart,
        state: "chartsCollapse",
        views: [
            {
                path: "/charts-crime-summary",
                name: "Crime Summary",
                mini: "CS",
                component: Charts,
                layout: "/admin"
            },
            {
                path: "/charts-crime-type",
                name: "Crime By Type",
                mini: "CT",
                component: CrimeTypeByLoc,
                layout: "/admin"
            },
        ]
    },
    {
        collapse: true,
        name: "Maps",
        icon: MapIcon,
        state: "mapsCollapse",
        views: [
            {
                collapse: true,
                name: "Historical Data",
                icon: AccountBalance,
                mini: "HD", // state: "multiCollapse",
                state: "historicalCollapse",
                views: [
                    {
                        path: "/full-screen-map",
                        name: "Crime History Map",
                        mini: "CFM",
                        component: FullScreenMap,
                        layout: "/admin"
                    },
                    {
                        path: "/full-screen-heatmap",
                        name: "Crime History Heatmap",
                        mini: "CFH",
                        component: FullScreenHeatMap,
                        layout: "/admin"
                    },
                    {
                        path: "/crime-over-time",
                        name: "Crime Over Time",
                        mini: "COT",
                        component: CrimeOverTimeLeaflet,
                        layout: "/admin"
                    },
                    // {
                    //     path: "/full-screen-summary",
                    //     name: "Crime Over Time Summary",
                    //     mini: "CTS",
                    //     component: FullScreenSummary,
                    //     layout: "/admin"
                    // }
                ]
            },
            {
                collapse: true,
                name: "Predicted Data",
                mini: "PD",
                icon: Casino,
                // state: "multiCollapse",
                state: "predictedCollapse",
                views: [
                    {
                        path: "/full-screen-pred-heatmap",
                        name: "Crime Predicted Heatmap",
                        mini: "CPH",
                        component: FullScreenHeatMapPred,
                        layout: "/admin"
                    }
                ]
            }
        ]
    }
];
export default dashRoutes;
