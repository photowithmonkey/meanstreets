/*!

=========================================================
* Material Dashboard PRO React - v1.8.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import ReactDOM from "react-dom";
import {BrowserRouter, Redirect, Route, Switch, withRouter} from "react-router-dom";

import AdminLayout from "layouts/Admin.js";
import AboutLayout from "layouts/About.js";

import "assets/scss/material-dashboard-pro-react.scss?v=1.8.0";
import {LastLocationProvider} from "react-router-last-location";

ReactDOM.render(
    <BrowserRouter forceRefresh={false}>
        <LastLocationProvider>
            <Switch>
                <Route path={"/admin"} component={AdminLayout}/>*
                <Route path="/about" component={AboutLayout}/>
                <Redirect from="/" to="/admin"/>
                <Redirect from="/about" to="/about/meanstreets/"/>
            </Switch>
        </LastLocationProvider>
    </BrowserRouter>,
    document.getElementById("root")
);
