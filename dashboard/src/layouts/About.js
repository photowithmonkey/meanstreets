import React from "react";
import {Switch, Route} from "react-router-dom";
import Footer from "../components/Footer/Footer.js";
// @material-ui
import {makeStyles} from "@material-ui/styles";

import AboutNavbar from "../components/Navbars/AboutNavbar.js";
import aboutRoutes from "variables/aboutRoutes";
import AJComp from "../views/Pages/DevPages/AJ";
import DenisComp from "../views/Pages/DevPages/Denis";
import EminComp from "../views/Pages/DevPages/Emin";
import JustinComp from "../views/Pages/DevPages/Justin";
import PaulComp from "../views/Pages/DevPages/Paul";
import MeanStreetsComp from "../views/Pages/DevPages/MeanStreets";

import styles from "../assets/jss/material-dashboard-pro-react/layouts/aboutStyle.js";
// background images
// Photo by Leo Cardelli from Pexels
import defaultPic from "../assets/img/skyline.jpg";
import city2 from "../assets/img/city2.jpg";
import baltCop from "../assets/img/backgrounds/balt_cop.jpg";

const useStyles = makeStyles(styles);

export default function AboutLayout(props) {
    const {...rest} = props;
    const wrapper = React.createRef();
    // styles
    const classes = useStyles();

    React.useEffect(() => {
        document.body.style.overflow = "unset";
        return function cleanup() {
        };
    });

    const getRoutes = aboutRoutes => {
        return aboutRoutes.map((prop, key) => {
            if (prop.layout === "/about") {
                return (
                    <Route
                        path={prop.layout + prop.path}
                        component={prop.component}
                        key={key}
                    />
                );
            } else {
                return null;
            }
        });
    };

    const getBgImage = () => {
        if (window.location.pathname.indexOf("/about/meanstreets") !== -1) {
            return city2;
        } else if (window.location.pathname.indexOf("/about/aj") !== -1) {
            return defaultPic;
        } else if (window.location.pathname.indexOf("/about/denis") !== -1) {
            return baltCop;
        } else if (window.location.pathname.indexOf("/about/emin") !== -1) {
            return defaultPic;
        } else if (window.location.pathname.indexOf("/about/justin") !== -1) {
            return city2;
        } else if (window.location.pathname.indexOf("/about/paul") !== -1) {
            return defaultPic;
        }
    };

    const getActiveRoute = routes => {
        let activeRoute = "Mean Streets";
        for (let i = 0; i < aboutRoutes.length; i++) {
            if (
                window.location.href.indexOf(aboutRoutes[i].layout + aboutRoutes[i].path)
                !== -1
            ) {
                return aboutRoutes[i].name;
            }
        }
        return activeRoute;
    };

    return (
        <div>
            <AboutNavbar color="#FFFFFF" pageName={getActiveRoute(aboutRoutes)}/>
            <div>
                <div
                    className={classes.fullPage}
                    style={{backgroundImage: "url(" + getBgImage() + ")"}}
                >
                    <Switch>
                        <Route path={"/about/meanstreets"} component={MeanStreetsComp}/>
                        <Route path={"/about_us/aj"} component={AJComp}/>
                        <Route path={"/about_us/emin"} component={EminComp}/>
                        <Route path={"/about_us/denis"} component={DenisComp}/>
                        <Route path={"/about_us/justin"} component={JustinComp}/>
                        <Route path={"/about_us/paul"} component={PaulComp}/>

                        {getRoutes(aboutRoutes)}
                    </Switch>
                    <Footer black/>
                </div>
            </div>
        </div>
    );
}