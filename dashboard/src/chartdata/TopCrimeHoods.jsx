import Icon from "@material-ui/core/Icon";
import makeStyles from "@material-ui/core/styles/makeStyles";
import axios from "axios";
import React, {useEffect, useState} from "react";
import dashboardStyles from "../assets/jss/material-dashboard-pro-react/views/dashboardStyle.js";
import Card from "../components/Card/Card";
import CardBody from "../components/Card/CardBody";
import CardHeader from "../components/Card/CardHeader";
import CardIcon from "../components/Card/CardIcon";
import GridContainer from "../components/Grid/GridContainer";
import GridItem from "../components/Grid/GridItem";


const useStyles = makeStyles(theme => ({
    ...dashboardStyles
}));

const colors = ["primary", "warning", "success", "danger", "info", ""]

export default function TopCrimeHoods() {

    const [hoods, setHoods] = useState([]);
    const classes = useStyles();

    useEffect(() => {axios.get("/api/abstracts/crimehood/2020").then(({data}) => setHoods(data))}, []);

    return (
        <GridContainer>
            {
                hoods.map((hood, idx) =>
                    <GridItem key={"hd-" + idx} xs={12} sm={6} md={6} lg={4}>
                        <Card>
                            <CardHeader color={colors[idx]} stats icon>
                                <CardIcon color={colors[idx]}>
                                    <Icon>speed</Icon>
                                </CardIcon>
                                <p className={classes.cardCategory}>{hood.hood}</p>
                            </CardHeader>
                            <CardBody>
                                <h3 className={classes.cardTitle}>{hood.totalCrime}<small>&nbsp;incidents</small></h3>
                            </CardBody>
                        </Card>
                    </GridItem>
                )
            }
        </GridContainer>
    )
}