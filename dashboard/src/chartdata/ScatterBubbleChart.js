import React, {PureComponent} from 'react';
import {
    ScatterChart, Scatter, XAxis, YAxis, ZAxis, Tooltip,
    Legend,
} from 'recharts';
import axios from "axios";
import _ from "lodash";


const parseDomain = (data) => {

    const weekMaxies = _.keys(data).map(week => Math.max.apply(null, data[week].value));
    const weekMinies = _.keys(data).map(week => Math.min.apply(null, data[week].value));

    return [
        Math.min.apply(null, weekMinies),
        Math.max.apply(null, weekMaxies)
    ];
};

export default function ScatterBubbleChart() {


    const [data, setData] = React.useState([]);

    React.useEffect(() => {

        const reducer = (accumulator, currentValue) => {
            accumulator[currentValue.week] = currentValue.days;
            return accumulator;
        };

        axios.get("/api/abstracts/weekly").then(res => {
            const d = res.data;
            const grouped = _.groupBy(d, row => row.week);
            const data = _.keys(grouped).map(week => {
                const group = grouped[week]; //array
                const days = group.map(row => {
                    return {day: row.day, dayName: row.dayName, index: 1, value: row.countIncidents}
                })

                return {week: week, days: days};
            }).reduce(reducer, {});

            setData(data);
        })
    }, []);


    const renderTooltip = (props) => {
        const {active, payload} = props;

        if (active && payload && payload.length) {
            const data = payload[0] && payload[0].payload;

            return (
                <div style={{
                    backgroundColor: '#fff', border: '1px solid #999', margin: 0, padding: 10,
                }}
                >
                    <p>{data.hour}</p>
                    <p>
                        <span>value: </span>
                        {data.value}
                    </p>
                </div>
            );
        }

        return null;
    };

    const domain = parseDomain(data);
    const range = [15, 300];

    return (
        <div>
            {
                _.keys(data).map((week, idx) =>
                    <ScatterChart
                        key={idx}
                        width={800}
                        height={60}
                        margin={{
                            top: 10, right: 0, bottom: 0, left: 0,
                        }}
                    >
                        <XAxis type="category" dataKey="dayName" interval={0}
                               tick={{fontSize: _.keys(data).length - 1 === idx ? 12 : 0}}
                               tickLine={{transform: 'translate(0, -6)'}}/>
                        <YAxis type="number" dataKey="index" name={week} height={10} width={80}
                               tick={false}
                               tickLine={false} axisLine={false}
                               label={{value: 'Week ' + week, position: 'insideRight'}}/>
                        <ZAxis type="number" dataKey="value" domain={domain} range={range}/>
                        <Tooltip cursor={{strokeDasharray: '3 3'}} wrapperStyle={{zIndex: 100}}
                                 content={renderTooltip}/>
                        <Scatter data={data[week]} fill="#8884d8"/>
                    </ScatterChart>
                )
            }
        </div>
    );
}
