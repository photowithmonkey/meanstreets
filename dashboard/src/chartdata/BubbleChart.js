import React from "react";
import {ResponsiveBubble} from "@nivo/circle-packing";

const theme = {
    labels: {
        text: {
            fontSize: 11
        }
    }
};

// make sure parent container have a defined height when using
// responsive component, otherwise height will be 0 and
// no chart will be rendered.
// website examples showcase many properties,
// you'll often use just a few of them.
const MyResponsiveBubble = ({root}) => (
    <ResponsiveBubble
        root={root}
        margin={{top: 20, right: 20, bottom: 20, left: 20}}
        identity="name"
        value="incidents"
        colors={{scheme: 'yellow_green'}}
        colorBy="name"
        padding={6}
        theme={theme}
        // labelTextColor={{from: 'color', modifiers: [['darker', 1.8]]}}
        labelTextColor="black"
        borderWidth={2}
        borderColor={{from: 'color'}}
        labelSkipRadius={32}
        animate={true}
        motionStiffness={45}
        motionDamping={12}
        defs={[
            {
                id: 'gradientT',
                type: 'linearGradient',
                colors: [
                    {offset: 0, color: 'inherit', opacity: 5},
                    {offset: 90, color: 'inherit', opacity: 0}
                ]
            }
        ]}
        fill={[
            {match: d => d.value > 1, id: 'gradientT'}
            // {match: {depth:1}, id: 'gradientT'},
            // {match: {depth:0}, id: 'gradientT'}
        ]}
    />
);

export default function BubbleChart(props) {

    const {chartOpen, selected, handleClose} = props;

    return (
        <div style={{width: 600, height: 600}} onClick={handleClose}>
            {
                chartOpen ? <MyResponsiveBubble root={selected}/> : null
            }
        </div>
    )
}