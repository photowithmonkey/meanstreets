import Button from "../components/CustomButtons/Button";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Tooltip from "@material-ui/core/Tooltip";
import LaunchIcon from "@material-ui/icons/Launch";
import Timeline from "@material-ui/icons/Timeline";
import axios from "axios";
import takeRight from "lodash/takeRight";
import moment from "moment";
import React, {useEffect, useState} from "react";
import ChartistGraph from "react-chartist";
import chartStyle from "../assets/jss/material-dashboard-pro-react/views/chartsStyle";
import dashboardStyles from "../assets/jss/material-dashboard-pro-react/views/dashboardStyle.js";
import Card from "../components/Card/Card";
import CardBody from "../components/Card/CardBody";
import CardFooter from "../components/Card/CardFooter";
import CardHeader from "../components/Card/CardHeader";
import CardIcon from "../components/Card/CardIcon";
import YearSlider from "../components/Input/YearSlider";

const styles = {
    ...dashboardStyles,
    ...chartStyle,
    cardFooter: {
        ...chartStyle.cardFooter,
        margin: 25
    }
};

const useStyles = makeStyles(styles);

const delays2 = 80,
    durations2 = 500;

const multipleBarsChart = {
    colors: ["#00bcd4", "#f05b4f", "#f4c63d"],
    options: {
        seriesBarDistance: 10,
        axisX: {
            showGrid: false
        },
        height: "300px"
    },
    responsiveOptions: [
        [
            "screen and (max-width: 640px)",
            {
                seriesBarDistance: 5,
                axisX: {
                    labelInterpolationFnc: function (value) {
                        return value[0];
                    }
                }
            }
        ]
    ],
    animation: {
        draw: function (data) {
            if (data.type === "bar") {
                data.element.animate({
                    opacity: {
                        begin: (data.index + 1) * delays2,
                        dur: durations2,
                        from: 0,
                        to: 1,
                        easing: "ease"
                    }
                });
            }
        }
    }
};


function formatLabel(d) {
    const month = new moment(d, 'MM');
    return month.format('MMM');
}


function PreviewChart({barData}) {

    const classes = useStyles();

    function shortenSeries(data) {
        const labels = takeRight(data.labels, 5);
        const series = data.series.map(s => takeRight(s, 5));
        return {labels: labels, series: series};
    }

    function handleLaunchClick() {
        window.location.href = "/admin/charts-crime-summary";
    }


    return (
        <Card chart className={classes.cardHover}>
            <CardHeader color="warning" className={classes.cardHeaderHover}>
                <ChartistGraph
                    className="ct-chart-white-colors"
                    data={shortenSeries(barData)}
                    type="Bar"
                    // options={dailySalesChart.options}
                    // listener={dailySalesChart.animation}
                />
            </CardHeader>
            <CardBody>
                <div className={classes.cardHoverUnder}>
                    <Tooltip
                        id="tooltip-top"
                        title="Full Size"
                        placement="bottom"
                        classes={{tooltip: classes.tooltip}}
                    >
                        <Button simple color="warning" justIcon onClick={handleLaunchClick}>
                            <LaunchIcon className={classes.underChartIcons}/>
                        </Button>
                    </Tooltip>
                </div>
                <h4 className={classes.cardTitle}>Crime by Season</h4>
            </CardBody>
        </Card>
    )
}


function MainChart({barData, handleYearChange}) {

    const classes = useStyles();

    return <Card>
        <CardHeader color="rose" icon>
            <CardIcon color="rose">
                <Timeline/>
            </CardIcon>
            <h4 className={classes.cardIconTitle}>
                Crime Distribution by Month <small>- Bar Chart</small>
            </h4>
        </CardHeader>
        <CardBody>
            <ChartistGraph
                data={barData}
                type="Bar"
                options={multipleBarsChart.options}
                listener={multipleBarsChart.animation}
            />
        </CardBody>
        <CardFooter stats className={classes.cardFooter}>
            <YearSlider defaultYear={2018} handleSliderChange={handleYearChange}/>
            <h6 className={classes.legendTitle}>Legend</h6>
            {barData.categories.map((l, idx) =>
                <span key={idx}><i className={"fas fa-circle"}
                                   style={{color: multipleBarsChart.colors[idx]}}/>{l}&nbsp;</span>
            )}
        </CardFooter>
    </Card>
}


export default function CrimeDistributionBars(props) {


    const [barData, setBarData] = useState({labels: [], series: [], categories: [], year: 2018});


    useEffect(() => {
        axios.get("/api/abstracts/monthly_by_type/" + barData.year).then(({data}) => {
            formatAllData(data);
        })
    }, [barData.year]);

    function formatAllData(data) {
        const labels = data.map((l) => formatLabel(l.month));

        let result = data.reduce((collector, v) => {
            Object.keys(v)
                .filter(k => k !== 'month')
                .forEach(key => collector[key] ? collector[key].push(v[key]) : collector[key] = [v[key]]);

            return collector;
        }, {});
        const categories = Object.keys(result).sort();
        const series = categories.map(key => result[key]);

        setBarData({...barData, labels: labels, series: series, categories: categories})
    }


    function handleYearChange(year) {
        setBarData({...barData, year: year})
    }

    return (
        props.type === "full" || !props.type ? <MainChart barData={barData} handleYearChange={handleYearChange}/>
            : <PreviewChart barData={barData}/>
    )

}