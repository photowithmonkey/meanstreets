import Button from "../components/CustomButtons/Button";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Tooltip from "@material-ui/core/Tooltip";
import LaunchIcon from "@material-ui/icons/Launch";
import Timeline from "@material-ui/icons/Timeline";
import axios from "axios";
import React, {useEffect, useState} from "react";
import ChartistGraph from "react-chartist";
import chartStyle from "../assets/jss/material-dashboard-pro-react/views/chartsStyle.js";
import dashboardStyles from "../assets/jss/material-dashboard-pro-react/views/dashboardStyle.js";
import Card from "../components/Card/Card";
import CardBody from "../components/Card/CardBody";
import CardFooter from "../components/Card/CardFooter";
import CardHeader from "../components/Card/CardHeader";
import CardIcon from "../components/Card/CardIcon";
import YearSlider from "../components/Input/YearSlider";

const styles = {
    ...dashboardStyles,
    ...chartStyle,
    cardFooter: {
        ...chartStyle.cardFooter,
        margin: 25
    }
};

const useStyles = makeStyles(styles);

const sum = (a, b) => a + b;

function PreviewChart({chartData}) {

    const classes = useStyles();

    function handleLaunchClick() {
        window.location.href = "/admin/charts-crime-summary";
    }

    return (
        <Card chart className={classes.cardHover}>
            <CardHeader color="success" className={classes.cardHeaderHover}>
                <ChartistGraph
                    className="ct-chart-white-colors"
                    data={chartData}
                    type="Pie"
                    // options={dailySalesChart.options}
                    // listener={dailySalesChart.animation}
                />
            </CardHeader>
            <CardBody>
                <div className={classes.cardHoverUnder}>
                    <Tooltip
                        id="tooltip-top"
                        title="Full Size"
                        placement="bottom"
                        classes={{tooltip: classes.tooltip}}
                    >
                        <Button color="success" simple justIcon onClick={handleLaunchClick}>
                            <LaunchIcon className={classes.underChartIcons}/>
                        </Button>
                    </Tooltip>
                </div>
                <h4 className={classes.cardTitle}>Crime Types</h4>
            </CardBody>
        </Card>
    )
}

function MainChart({chartData,handleYearChange}) {

    const f1 = function (value) {
        return Math.round(value / chartData.series.reduce(sum) * 100) + "%";
    };

    const pieChart = {
        options: {
            height: "330px",
            labelInterpolationFnc: f1
        },
        colors: ["#00bcd4", "#f05b4f", "#f4c63d"]
    };
    const classes = useStyles();

    return (
        <Card>
            <CardHeader color="danger" icon>
                <CardIcon color="danger">
                    <Timeline/>
                </CardIcon>
                <h4 className={classes.cardIconTitle}>Crime Distribution</h4>
            </CardHeader>
            <CardBody>
                <ChartistGraph
                    data={chartData}
                    type="Pie"
                    options={pieChart.options}
                />
            </CardBody>
            <CardFooter stats className={classes.cardFooter}>
                <YearSlider defaultYear={2019} handleSliderChange={handleYearChange}/>
                <h6 className={classes.legendTitle}>Legend</h6>
                {chartData.legend.map((l, idx) =>
                    <span key={idx}>
                        <i className={"fas fa-circle"} style={{color: pieChart.colors[idx]}}/>
                        {l}&nbsp;</span>
                )}
            </CardFooter>
        </Card>
    )
}

export default function CrimeDistributionPie(props) {

    const [chartData, setChartData] = useState({
        legend: [],
        series: [],
        year: 2019
    });

    useEffect(() => {
        axios.get('/api/abstracts/annual_by_type/' + chartData.year).then(({data}) => {
            fetchAllData(data);
        })
    }, [chartData.year]);

    function fetchAllData(data) {

        let totals = data.reduce((collector, val) => {
            collector[val.crimeDescription] = val.totalCrimes;
            return collector;
        }, {});

        const legend = Object.keys(totals).sort();
        const series = legend.map(key => totals[key]);

        setChartData({
            ...chartData, legend: legend, series: series
        })
    }

    function handleYearChange(year) {
        setChartData({...chartData, year: year})
    }


    return (props.type === "full" || !props.type ? <MainChart chartData={chartData} handleYearChange={handleYearChange}/>
    : <PreviewChart chartData={chartData}/>
    )
}