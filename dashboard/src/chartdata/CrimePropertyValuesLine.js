import makeStyles from "@material-ui/core/styles/makeStyles";
import Timeline from "@material-ui/icons/Timeline";
import axios from "axios";
import Chartist from "chartist";
import moment from "moment";
import React, {useEffect, useState} from "react";
import ChartistGraph from "react-chartist";
import chartStyle from "../assets/jss/material-dashboard-pro-react/views/chartsStyle";
import Card from "../components/Card/Card";
import CardBody from "../components/Card/CardBody";
import CardFooter from "../components/Card/CardFooter";
import CardHeader from "../components/Card/CardHeader";
import CardIcon from "../components/Card/CardIcon";
import CustomDropdown from "../components/CustomDropdown/CustomDropdown";

const styles = {
    ...chartStyle,
    cardFooter: {
        ...chartStyle.cardFooter,
        margin: 25
    }
};

const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

const useStyles = makeStyles(styles);

const delays2 = 80,
    durations2 = 500;

const lineChartOptions = {
    colors: ["#00bcd4", "#f05b4f"],
    options: {
        axisX: {
            type: Chartist.FixedScaleAxis,
            divisor: 25,
            showGrid: true,
            labelInterpolationFnc: function (value) {
                return moment(value).format('MMM YYYY');
            }
        },
        height: "300px"
    },
    responsiveOptions: [
        [
            "screen and (max-width: 640px)",
            {
                seriesBarDistance: 5,
                axisX: {
                    labelInterpolationFnc: function (value) {
                        return value[0];
                    }
                }
            }
        ]
    ],
    animation: {
        draw: function (data) {
            if (data.type === "line") {
                data.element.animate({
                    opacity: {
                        begin: (data.index + 1) * delays2,
                        dur: durations2,
                        from: 0,
                        to: 1,
                        easing: "ease"
                    }
                });
            }
        }
    }
};


export default function CrimePropertyValuesLine() {

    const baseUrl = (p) => "/api/abstracts/properties" + (p || "");

    const [chartData, setChartData] = useState({series: []});
    const [selected, setSelected] = useState("Combined");
    const [zipCodes, setZipCodes] = useState(["Combined"]);

    useEffect(() => {
        axios.get(baseUrl("/zips")).then(({data}) => {
            setZipCodes(["Combined", {divider: true}, ...data]);
        })
    }, []);


    useEffect(() => {
        axios.get(baseUrl(selected !== "Combined" ? "/" + selected.toLowerCase() : ""))
            .then(({data}) => {
                let collector = {};

                let series1 = [];
                let series2 = [];
                data.forEach(pt => {
                    series1.push({x: moment(pt.clusterDate), y: pt.price});
                    series2.push({x: moment(pt.clusterDate), y: pt.totalCrime});
                });

                const chartData = {
                    series: [
                        {
                            name: "House Prices",
                            data: series1
                        },
                        {
                            name: "Crime Incidents",
                            data: series2
                        }
                    ]
                };

                setChartData(chartData);

            })
    }, [selected]);


    function handleCrimeSelector(slct) {
        setSelected(slct);
    }

    const classes = useStyles();
    return (
        <Card>
            <CardHeader color="rose" icon>
                <CardIcon color="rose">
                    <Timeline/>
                </CardIcon>
                <h4 className={classes.cardIconTitle}>
                    Crime Rates vs Property Values <small>- Line Chart</small>
                </h4>
            </CardHeader>
            <CardBody>
                <ChartistGraph
                    data={chartData}
                    type="Line"
                    options={lineChartOptions.options}
                    listener={lineChartOptions.animation}
                />
            </CardBody>
            <CardFooter stats className={classes.cardFooter}>
                {/*<Typography component="span">Zip Code:</Typography>*/}
                <CustomDropdown
                    buttonText={selected}
                    buttonProps={{
                        color: "info"
                    }}
                    dropdownList={zipCodes}
                    onClick={handleCrimeSelector}
                />
                <h6 className={classes.legendTitle}>Legend</h6>
                {chartData.series.map((series, idx) =>
                    <span key={idx}><i className={"fas fa-circle"}
                                       style={{color: lineChartOptions.colors[idx]}}/>{series.name}&nbsp;</span>
                )}
            </CardFooter>
        </Card>
    )

}
