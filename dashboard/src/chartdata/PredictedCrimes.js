import Button from "../components/CustomButtons/Button";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Tooltip from "@material-ui/core/Tooltip";
import LaunchIcon from "@material-ui/icons/Launch"
import Timeline from "@material-ui/icons/Timeline";
import axios from "axios";
import takeRight from "lodash/takeRight";
import moment from "moment";
import React, {Fragment, useEffect, useState} from "react";
import ChartistGraph from "react-chartist";
import chartStyle from "../assets/jss/material-dashboard-pro-react/views/chartsStyle";
import dashboardStyles from "../assets/jss/material-dashboard-pro-react/views/dashboardStyle.js";
import Card from "../components/Card/Card";
import CardBody from "../components/Card/CardBody";
import CardFooter from "../components/Card/CardFooter";
import CardHeader from "../components/Card/CardHeader";
import CardIcon from "../components/Card/CardIcon";

const Chartist = require("chartist");

// ############################## // // // variables used to create animation on charts
// #############################

const delays = 80,
    durations = 500;
/* unused var delays2 = 80,
    durations2 = 500;*/

// ##############################
// // // Coloured Lines Chart
// #############################

const useStyles = makeStyles(theme => ({
    ...dashboardStyles,
    ...chartStyle,
    cardFooter: {
        ...chartStyle.cardFooter,
        margin: 25
    }
}));

const colouredLinesChart = {
    colors: ["#00bcd4", "#f05b4f", "#f4c63d"],
    data: {
        labels: [
            "'06",
            "'07",
            "'08",
            "'09",
            "'10",
            "'11",
            "'12",
            "'13",
            "'14",
            "'15"
        ],
        series: [
            [287, 385, 490, 554, 586, 698, 695, 752, 788, 846, 944],
            [67, 152, 143, 287, 335, 435, 437, 539, 542, 544, 647],
            [23, 113, 67, 190, 239, 307, 308, 439, 410, 410, 509]
        ]
    },
    options: {
        lineSmooth: Chartist.Interpolation.cardinal({
            tension: 10
        }),
        axisY: {
            showGrid: true,
            offset: 40
        },
        axisX: {
            showGrid: false
        },
        low: 0,
        high: 10000,
        showPoint: true,
        height: "300px"
    },
    animation: {
        draw: function (data) {
            if (data.type === "line" || data.type === "area") {
                data.element.animate({
                    d: {
                        begin: 600,
                        dur: 700,
                        from: data.path
                            .clone()
                            .scale(1, 0)
                            .translate(0, data.chartRect.height())
                            .stringify(),
                        to: data.path.clone().stringify(),
                        easing: Chartist.Svg.Easing.easeOutQuint
                    }
                });
            } else if (data.type === "point") {
                data.element.animate({
                    opacity: {
                        begin: (data.index + 1) * delays,
                        dur: durations,
                        from: 0,
                        to: 1,
                        easing: "ease"
                    }
                });
            }
        }
    }
};

function formatLabel(d) {
    const year = new moment(d, 'YYYY');
    return year.format('YYYY');
}


function PreviewChart({chartData}) {

    const classes = useStyles();

    function shortenSeries(data) {
        const labels = takeRight(data.labels, 5);
        const series = data.series.map(s => takeRight(s, 5));
        return {labels: labels, series: series};
    }

    function handleLaunchClick(){
        window.location.href="/admin/charts-crime-summary";
    }


    return (
        <Card chart className={classes.cardHover}>
            <CardHeader color="info" className={classes.cardHeaderHover}>
                <ChartistGraph
                    className="ct-chart-white-colors"
                    data={shortenSeries(chartData)}
                    type="Line"
                    // options={dailySalesChart.options}
                    // listener={dailySalesChart.animation}
                />
            </CardHeader>
            <CardBody>
                <div className={classes.cardHoverUnder}>
                    <Tooltip
                        id="tooltip-top"
                        title="Full Size"
                        placement="bottom"
                        classes={{tooltip: classes.tooltip}}
                    >
                        <Button color="info" justIcon simple onClick={handleLaunchClick}>
                            <LaunchIcon className={classes.underChartIcons}/>
                        </Button>
                    </Tooltip>
                </div>
                <h4 className={classes.cardTitle}>Annual Totals</h4>
            </CardBody>
        </Card>
    )
}

function MainChart({chartData}) {

    const classes = useStyles();

    return (
        <Card>
            <CardHeader color="warning" icon>
                <CardIcon color="warning">
                    <Timeline/>
                </CardIcon>
                <h4 className={classes.cardIconTitle}>
                    Crimes Variation By Year
                </h4>
            </CardHeader>
            <CardBody>
                <ChartistGraph
                    data={chartData}
                    type="Line"
                    options={colouredLinesChart.options}
                    listener={colouredLinesChart.animation}
                />
            </CardBody>
            <CardFooter stats className={classes.cardFooter}>
                <h6 className={classes.legendTitle}>Legend</h6>
                {chartData.legend.map((l, idx) =>
                    <span key={"legend-cvy-" + idx}><i className={"fas fa-circle"}
                                                       style={{color: colouredLinesChart.colors[idx]}}/>{l}&nbsp;</span>
                )}
            </CardFooter>
        </Card>
    )
}


export default function PredictedCrimes(props) {

    const [chartData, setChartData] = useState({labels: [], legend: [], series: [[], [], []]});
    const classes = useStyles();

    useEffect(() => {
        axios.get("/api/abstracts/annual").then((d) => {
                formatAllData(d.data)
            }
        )

    }, []);

    function formatAllData(data) {
        const labels = data.map((l) => formatLabel(l.year));

        let result = data.reduce((collector, v) => {
            Object.keys(v)
                .filter(k => k !== 'year')
                .forEach(key => collector[key] ? collector[key].push(v[key]) : collector[key] = [v[key]]);

            return collector;
        }, {});
        const legend = Object.keys(result).sort();
        const series = legend.map(key => result[key]);

        setChartData({labels: labels, legend: legend, series: series})
    }


    return <Fragment>
        {props.type === "main" || !props.type
            ? <MainChart chartData={chartData}/> : <PreviewChart chartData={chartData}/>}
    </Fragment>
}

