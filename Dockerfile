FROM openjdk:8-jdk-alpine
VOLUME /tmp
RUN mkdir h2
COPY h2/testdb.mv.db h2/testdb.mv.db
COPY crime-predictor/target/*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]